/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : disinfect

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-07-25 22:25:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `t_auth_config`
-- ----------------------------
DROP TABLE IF EXISTS `t_auth_config`;
CREATE TABLE `t_auth_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL COMMENT '名称',
  `url` varchar(60) NOT NULL COMMENT '设定url',
  `status` int(1) NOT NULL COMMENT '1正常 4删除',
  `pid` bigint(20) NOT NULL COMMENT '父ID，根的父ID为0.',
  `level` varchar(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_auth_config
-- ----------------------------
INSERT INTO `t_auth_config` VALUES ('1', '我的工作台', '', '1', '0', '0');
INSERT INTO `t_auth_config` VALUES ('2', '物资管理', '', '1', '1', '1');
INSERT INTO `t_auth_config` VALUES ('3', '物品入库登记', '/goods/inlist', '1', '2', '2');
INSERT INTO `t_auth_config` VALUES ('4', '物品出库登记', '/goods/stocklistForOut', '1', '2', '2');
INSERT INTO `t_auth_config` VALUES ('5', '物品库存查询', '/goods/stocklist', '1', '2', '1');
INSERT INTO `t_auth_config` VALUES ('6', '物品出库统计', '/goods/outlist', '1', '2', '2');
INSERT INTO `t_auth_config` VALUES ('10', '物品申领', '', '1', '1', '1');
INSERT INTO `t_auth_config` VALUES ('11', '一次性物品申请登记', '/oneTime/list', '1', '10', '2');
INSERT INTO `t_auth_config` VALUES ('12', '非一次性物品申请登记', '/nonDisposableApply/list', '1', '10', '2');
INSERT INTO `t_auth_config` VALUES ('13', '非一次性物品借用登记', '/nonDisposableBorrow/list', '1', '10', '1');
INSERT INTO `t_auth_config` VALUES ('14', '非一次性物品归还登记', '/nonDisposableRevert/list', '1', '10', '2');
INSERT INTO `t_auth_config` VALUES ('15', '特殊物品申请登记', '/special/list', '1', '10', '2');
INSERT INTO `t_auth_config` VALUES ('16', '物品申领记录统计', '/apply/list', '1', '10', '2');
INSERT INTO `t_auth_config` VALUES ('20', '消毒灭菌', '', '1', '1', '1');
INSERT INTO `t_auth_config` VALUES ('21', '回收登记', '/retrieve/list', '1', '20', '2');
INSERT INTO `t_auth_config` VALUES ('22', '清洗登记', '/clean/list', '1', '20', '2');
INSERT INTO `t_auth_config` VALUES ('23', '打包登记', '/pack/list', '1', '20', '1');
INSERT INTO `t_auth_config` VALUES ('24', '灭菌登记', '/disinfect/list', '1', '20', '2');
INSERT INTO `t_auth_config` VALUES ('25', '发放登记', '/giveout/list', '1', '20', '2');
INSERT INTO `t_auth_config` VALUES ('26', '追溯', '/goodTrace/list', '1', '20', '2');
INSERT INTO `t_auth_config` VALUES ('30', '人员管理', '', '1', '1', '1');
INSERT INTO `t_auth_config` VALUES ('31', '人员信息列表', '/person/list', '1', '30', '2');
INSERT INTO `t_auth_config` VALUES ('32', '科室信息列表', '/department/list', '1', '30', '2');
INSERT INTO `t_auth_config` VALUES ('36', '系统管理', '', '1', '1', '1');
INSERT INTO `t_auth_config` VALUES ('37', '账号信息', '/admin/list', '1', '36', '2');
INSERT INTO `t_auth_config` VALUES ('38', '管理员角色信息', '/role/list', '1', '36', '2');
INSERT INTO `t_auth_config` VALUES ('39', '数据库备份', '/database/dataBaseList', '1', '36', '2');
INSERT INTO `t_auth_config` VALUES ('40', '人员角色', '/personRole/list', '1', '36', '2');
INSERT INTO `t_auth_config` VALUES ('41', '数据导入', '/goodsType/inexcel', '1', '36', '2');

-- ----------------------------
-- Table structure for `t_database`
-- ----------------------------
DROP TABLE IF EXISTS `t_database`;
CREATE TABLE `t_database` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  `path` varchar(256) DEFAULT NULL,
  `createTime` varchar(19) DEFAULT NULL,
  `createor` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_database
-- ----------------------------
INSERT INTO `t_database` VALUES ('6', '2018-05-05', 'D:/logs/20180505172959.sql', '2018-05-05 17:30:00', 'admin');

-- ----------------------------
-- Table structure for `t_department`
-- ----------------------------
DROP TABLE IF EXISTS `t_department`;
CREATE TABLE `t_department` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `departmentName` varchar(32) NOT NULL,
  `departmentDirector` varchar(32) NOT NULL,
  `telephoneNum` varchar(20) DEFAULT NULL,
  `address` varchar(128) DEFAULT NULL,
  `createTime` varchar(19) DEFAULT NULL,
  `updateTime` varchar(19) DEFAULT NULL,
  `createor` varchar(24) DEFAULT NULL,
  `updateor` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_department
-- ----------------------------
INSERT INTO `t_department` VALUES ('5', '内科', '张三', '18888888888', '一楼左三', '2017-09-20 12:00:00', '2017-09-20 12:00:00', null, null);
INSERT INTO `t_department` VALUES ('7', '外科', '外科', '15889720261', '外科', '2018-07-10 22:20:04', '2018-07-10 22:36:16', 'admin', null);
INSERT INTO `t_department` VALUES ('8', '外科1', '外科1', '15889720260', '外科', '2018-07-11 22:14:16', '2018-07-11 22:14:16', 'admin', null);

-- ----------------------------
-- Table structure for `t_goods`
-- ----------------------------
DROP TABLE IF EXISTS `t_goods`;
CREATE TABLE `t_goods` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `wpglid` varchar(10) DEFAULT NULL COMMENT '物品管理ID',
  `wzuuid` varchar(10) DEFAULT NULL COMMENT '物资编码UUID（物品编号）',
  `ckid` varchar(32) DEFAULT NULL COMMENT '仓库ID',
  `wzzt` varchar(32) DEFAULT NULL COMMENT '物资状态 在库/出库/删除/退回/报损/残缺',
  `wpyxq` varchar(32) DEFAULT NULL COMMENT '有效期/按月',
  `scph` varchar(32) DEFAULT NULL COMMENT '生产批号',
  `scrq` varchar(32) DEFAULT NULL COMMENT '生产日期',
  `wzsl` varchar(32) DEFAULT NULL COMMENT '物资数量',
  `rksj` varchar(19) DEFAULT NULL COMMENT '入库时间',
  `cksj` varchar(19) DEFAULT NULL COMMENT '出库时间',
  `czyuuid` varchar(5) DEFAULT NULL COMMENT '操作员UUID',
  `createTime` varchar(19) DEFAULT NULL COMMENT '创建时间',
  `updateTime` varchar(19) DEFAULT NULL,
  `createor` varchar(20) DEFAULT NULL,
  `updateor` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_goods
-- ----------------------------

-- ----------------------------
-- Table structure for `t_goods_class`
-- ----------------------------
DROP TABLE IF EXISTS `t_goods_class`;
CREATE TABLE `t_goods_class` (
  `id` int(10) NOT NULL,
  `goodsClass` varchar(10) DEFAULT NULL,
  `goodsClassName` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_goods_class
-- ----------------------------
INSERT INTO `t_goods_class` VALUES ('1', '1', '一次性');
INSERT INTO `t_goods_class` VALUES ('2', '2', '非一次性');
INSERT INTO `t_goods_class` VALUES ('3', '3', '可回收');
INSERT INTO `t_goods_class` VALUES ('4', '4', '不可回收');

-- ----------------------------
-- Table structure for `t_goods_in`
-- ----------------------------
DROP TABLE IF EXISTS `t_goods_in`;
CREATE TABLE `t_goods_in` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `wpglid` varchar(64) DEFAULT NULL COMMENT '物品管理ID',
  `sccs` varchar(32) DEFAULT NULL COMMENT '生成厂商',
  `wzuuid` varchar(20) DEFAULT NULL COMMENT '物资编码UUID（物品编号）',
  `ckid` varchar(32) DEFAULT NULL COMMENT '仓库ID',
  `wzzt` varchar(32) DEFAULT NULL COMMENT '物资状态 在库/出库/删除/退回/报损/残缺',
  `wpyxq` varchar(32) DEFAULT NULL COMMENT '有效期/按月',
  `scph` varchar(32) DEFAULT NULL COMMENT '生产批号',
  `scrq` varchar(32) DEFAULT NULL COMMENT '生产日期',
  `wzsl` varchar(32) DEFAULT NULL COMMENT '物资数量',
  `rksj` varchar(19) DEFAULT NULL COMMENT '入库时间',
  `disinfectTime` varchar(19) DEFAULT NULL COMMENT '灭菌时间',
  `cksj` varchar(19) DEFAULT NULL COMMENT '出库时间',
  `czyuuid` varchar(5) DEFAULT NULL COMMENT '操作员UUID',
  `createor` varchar(20) DEFAULT NULL,
  `createTime` varchar(19) DEFAULT NULL COMMENT '创建时间',
  `updateTime` varchar(19) DEFAULT NULL,
  `updateor` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_goods_in
-- ----------------------------
INSERT INTO `t_goods_in` VALUES ('5', '5d953275861e45bcadc9760e138a1c6b', '生产厂商12', '物资编码UUID1', '1', '0', '5个月', '0003', '2018-07-16 21:25:47', '100', '2018-07-16 21:25:56', '2018-07-16', null, 'admin', 'admin', '2018-07-16 21:25:56', null, null);

-- ----------------------------
-- Table structure for `t_goods_out`
-- ----------------------------
DROP TABLE IF EXISTS `t_goods_out`;
CREATE TABLE `t_goods_out` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inId` bigint(20) DEFAULT NULL COMMENT '入库表 对应的外键',
  `wpglid` varchar(64) DEFAULT NULL COMMENT '物品管理ID',
  `sccs` varchar(32) DEFAULT NULL COMMENT '生成厂商',
  `wzuuid` varchar(20) DEFAULT NULL COMMENT '物资编码UUID（物品编号）',
  `ckid` varchar(32) DEFAULT NULL COMMENT '仓库ID',
  `wzzt` varchar(32) DEFAULT NULL COMMENT '物资状态 在库/出库/删除/退回/报损/残缺',
  `wpyxq` varchar(32) DEFAULT NULL COMMENT '有效期/按月',
  `scph` varchar(32) DEFAULT NULL COMMENT '生产批号',
  `scrq` varchar(32) DEFAULT NULL COMMENT '生产日期',
  `wzsl` varchar(32) DEFAULT NULL COMMENT '物资数量',
  `rksj` varchar(19) DEFAULT NULL COMMENT '入库时间',
  `disinfectTime` varchar(19) DEFAULT NULL COMMENT '灭菌时间',
  `cksj` varchar(19) DEFAULT NULL COMMENT '出库时间',
  `czyuuid` varchar(5) DEFAULT NULL COMMENT '操作员UUID',
  `createor` varchar(20) DEFAULT NULL,
  `createTime` varchar(19) DEFAULT NULL COMMENT '创建时间',
  `updateTime` varchar(19) DEFAULT NULL,
  `updateor` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_goods_out
-- ----------------------------
INSERT INTO `t_goods_out` VALUES ('6', '5', '502d537543714e05a076767ca7c81647', '生产厂商12', '物资编码UUID1', '1', '1', '5个月', '0003', '2018-07-16 21:25:47', '30', '2018-07-16 21:25:56', '2018-07-15', '2018-07-16 21:54:47', 'admin', 'admin', '2018-07-16 21:54:47', '2018-07-16 23:41:52', null);

-- ----------------------------
-- Table structure for `t_goods_type`
-- ----------------------------
DROP TABLE IF EXISTS `t_goods_type`;
CREATE TABLE `t_goods_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `wzuuid` varchar(10) DEFAULT NULL COMMENT '物资编码UUID（物品编号）',
  `pydm` varchar(32) DEFAULT NULL COMMENT '拼音代码',
  `wpmc` varchar(32) NOT NULL COMMENT '物品名称',
  `wpgg` varchar(32) DEFAULT NULL COMMENT '物品规格',
  `wpdw` varchar(32) DEFAULT NULL COMMENT '物品单位',
  `zxgg` varchar(32) DEFAULT NULL COMMENT '装箱规格',
  `sccs` varchar(32) DEFAULT NULL COMMENT '生产厂商',
  `mrdj` varchar(32) DEFAULT NULL COMMENT '默认单价',
  `wpfl` varchar(32) DEFAULT NULL COMMENT '物品分类 一次性，非一次性，可回收，不可回收',
  `createTime` varchar(19) DEFAULT NULL COMMENT '创建时间',
  `updateTime` varchar(19) DEFAULT NULL,
  `createor` varchar(20) DEFAULT NULL,
  `updateor` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_goods_type
-- ----------------------------
INSERT INTO `t_goods_type` VALUES ('1', '物资编码UUID1', '拼音代码1', '物品名称12', '物品规格12', '物品单位12', '装箱规格12', '生产厂商12', '默认单价12', '一次性', '2018-07-15 11:10:22', '2018-07-15 11:10:22', 'admin', 'admin');

-- ----------------------------
-- Table structure for `t_manager`
-- ----------------------------
DROP TABLE IF EXISTS `t_manager`;
CREATE TABLE `t_manager` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account` varchar(32) NOT NULL COMMENT '帐号',
  `password` varchar(64) NOT NULL COMMENT '密码',
  `createTime` varchar(19) DEFAULT NULL,
  `updateTime` varchar(19) DEFAULT NULL,
  `remark` text,
  `departmentId` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_UNIQUE` (`account`),
  KEY `MANAGER_ACCOUNT_INDEX` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_manager
-- ----------------------------
INSERT INTO `t_manager` VALUES ('61', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '2012-10-10 00:11:11', '2016-08-28 22:58:49', '超级管理员', '5');
INSERT INTO `t_manager` VALUES ('62', 'test11', 'c20ad4d76fe97759aa27a0c99bff6710', '2018-02-27 22:33:49', '2018-07-12 22:15:28', 'test', '7');
INSERT INTO `t_manager` VALUES ('63', 'just', '8134b84030cca5285ed0e0b31ba06f10', '2018-05-02 21:57:14', '2018-07-10 21:30:12', 'just', '5');

-- ----------------------------
-- Table structure for `t_manager_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_manager_role`;
CREATE TABLE `t_manager_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `managerid` int(20) NOT NULL,
  `roleid` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_manager_role
-- ----------------------------
INSERT INTO `t_manager_role` VALUES ('38', '61', '8');
INSERT INTO `t_manager_role` VALUES ('41', '63', '8');
INSERT INTO `t_manager_role` VALUES ('55', '62', '8');
INSERT INTO `t_manager_role` VALUES ('56', '62', '9');

-- ----------------------------
-- Table structure for `t_nondisposable_apply`
-- ----------------------------
DROP TABLE IF EXISTS `t_nondisposable_apply`;
CREATE TABLE `t_nondisposable_apply` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` varchar(3) DEFAULT '0' COMMENT '0 申请 1借用 2 归还',
  `wzuuid` varchar(20) DEFAULT NULL,
  `pydm` varchar(10) DEFAULT NULL COMMENT '拼音代码',
  `applyPersonId` varchar(32) DEFAULT NULL COMMENT '申请人 ID',
  `sccs` varchar(32) DEFAULT NULL COMMENT '生产厂商',
  `scph` varchar(20) DEFAULT NULL COMMENT '生产批号',
  `wzsl` varchar(32) DEFAULT NULL COMMENT '物资数量',
  `tid` varchar(32) DEFAULT NULL COMMENT '追溯ID',
  `applyTime` varchar(19) DEFAULT NULL COMMENT '申请 时间',
  `createTime` varchar(19) DEFAULT NULL COMMENT '创建时间',
  `updateTime` varchar(19) DEFAULT NULL,
  `createor` varchar(20) DEFAULT NULL,
  `updateor` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_nondisposable_apply
-- ----------------------------
INSERT INTO `t_nondisposable_apply` VALUES ('4', '2', '物资编码UUID1', '拼音代码1', '21', '生产厂商12', '3', '31', '9df8b97bd2b24b0f93f9a96572b3b6e7', '2018-07-24 21:25:45', '2018-07-24 21:25:45', '2018-07-24 22:02:37', 'admin', 'admin');

-- ----------------------------
-- Table structure for `t_one_time_apply`
-- ----------------------------
DROP TABLE IF EXISTS `t_one_time_apply`;
CREATE TABLE `t_one_time_apply` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `wzuuid` varchar(20) DEFAULT NULL,
  `pydm` varchar(10) DEFAULT NULL COMMENT '拼音代码',
  `applyPersonId` varchar(32) DEFAULT NULL COMMENT '申请人 ID',
  `sccs` varchar(32) DEFAULT NULL COMMENT '生产厂商',
  `scph` varchar(20) DEFAULT NULL COMMENT '生产批号',
  `wzsl` varchar(32) DEFAULT NULL COMMENT '物资数量',
  `tid` varchar(32) DEFAULT NULL COMMENT '追溯ID',
  `applyTime` varchar(19) DEFAULT NULL COMMENT '申请 时间',
  `createTime` varchar(19) DEFAULT NULL COMMENT '创建时间',
  `updateTime` varchar(19) DEFAULT NULL,
  `createor` varchar(20) DEFAULT NULL,
  `updateor` varchar(20) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_one_time_apply
-- ----------------------------
INSERT INTO `t_one_time_apply` VALUES ('3', '物资编码UUID1', '拼音代码1', '0001', '生产厂商12', '01', '1', '58e47fc652af4ef1b92aac5704effd5d', '2018-07-24 21:08:04', '2018-07-24 21:08:04', '2018-07-24 21:29:57', 'admin', 'admin', null);

-- ----------------------------
-- Table structure for `t_person`
-- ----------------------------
DROP TABLE IF EXISTS `t_person`;
CREATE TABLE `t_person` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `personId` varchar(20) NOT NULL,
  `name` varchar(32) NOT NULL,
  `sex` varchar(8) NOT NULL,
  `telephoneNum` varchar(20) DEFAULT NULL,
  `departmentId` int(11) DEFAULT NULL,
  `createTime` varchar(19) DEFAULT NULL,
  `updateTime` varchar(19) DEFAULT NULL,
  `remark` varchar(256) DEFAULT NULL,
  `createor` varchar(24) DEFAULT NULL,
  `updateor` varchar(24) DEFAULT NULL,
  `personRoleIds` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_person
-- ----------------------------
INSERT INTO `t_person` VALUES ('5', '0001', '孙思邈', '男', '18602554094', '5', '2017-09-20 00:32:25', '2017-10-22 14:45:51', '谜', null, 'admin', '5,');
INSERT INTO `t_person` VALUES ('8', '21', '21', '女', '15989898711', '5', '2017-11-26 09:07:57', '2017-11-26 09:13:48', '1234561', 'admin', 'admin', '7,');
INSERT INTO `t_person` VALUES ('13', '213', '21', '1', '15989898718', '7', '2018-07-11 22:39:31', '2018-07-11 22:47:23', '1234569', null, 'admin', '5,7,');
INSERT INTO `t_person` VALUES ('14', 'just', 'just', '0', '15889720260', '5', '2018-07-12 22:09:43', '2018-07-12 22:14:00', 'just', null, 'admin', '5,7');

-- ----------------------------
-- Table structure for `t_person_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_person_role`;
CREATE TABLE `t_person_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `personRoleName` varchar(32) NOT NULL,
  `createTime` varchar(19) DEFAULT NULL,
  `updateTime` varchar(19) DEFAULT NULL,
  `createor` varchar(24) DEFAULT NULL,
  `updateor` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_person_role
-- ----------------------------
INSERT INTO `t_person_role` VALUES ('5', '医生', '2017-09-20 12:00:00', '2018-07-11 23:57:28', null, 'admin');
INSERT INTO `t_person_role` VALUES ('7', '情绪', '2018-07-10 22:20:04', '2018-07-11 23:58:15', 'admin', 'admin');

-- ----------------------------
-- Table structure for `t_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(32) NOT NULL,
  `roleDes` varchar(64) NOT NULL,
  `createtime` varchar(19) DEFAULT NULL,
  `updatetime` varchar(19) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('8', '超级管理员', '超级管理员拥有所以权限', '0', '2018-07-14 20:05:01');
INSERT INTO `t_role` VALUES ('9', 'test1', 'test des', '2016-08-28 16:44:50', '2018-03-03 23:47:32');

-- ----------------------------
-- Table structure for `t_role_auth`
-- ----------------------------
DROP TABLE IF EXISTS `t_role_auth`;
CREATE TABLE `t_role_auth` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roleId` bigint(20) NOT NULL COMMENT '管理员表id',
  `authId` bigint(20) NOT NULL COMMENT '权限表id',
  PRIMARY KEY (`id`),
  KEY `MANAGER_AUTH_MANAGERID_INDEX` (`roleId`),
  KEY `MANAGER_AUTH_AUTHID_INDEX` (`authId`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role_auth
-- ----------------------------
INSERT INTO `t_role_auth` VALUES ('139', '8', '1');
INSERT INTO `t_role_auth` VALUES ('140', '8', '10');
INSERT INTO `t_role_auth` VALUES ('141', '8', '11');
INSERT INTO `t_role_auth` VALUES ('142', '8', '12');
INSERT INTO `t_role_auth` VALUES ('143', '8', '13');
INSERT INTO `t_role_auth` VALUES ('144', '8', '14');
INSERT INTO `t_role_auth` VALUES ('145', '8', '15');
INSERT INTO `t_role_auth` VALUES ('146', '8', '16');
INSERT INTO `t_role_auth` VALUES ('147', '8', '2');
INSERT INTO `t_role_auth` VALUES ('148', '8', '20');
INSERT INTO `t_role_auth` VALUES ('149', '8', '21');
INSERT INTO `t_role_auth` VALUES ('150', '8', '22');
INSERT INTO `t_role_auth` VALUES ('151', '8', '23');
INSERT INTO `t_role_auth` VALUES ('152', '8', '24');
INSERT INTO `t_role_auth` VALUES ('153', '8', '25');
INSERT INTO `t_role_auth` VALUES ('154', '8', '26');
INSERT INTO `t_role_auth` VALUES ('155', '8', '3');
INSERT INTO `t_role_auth` VALUES ('156', '8', '30');
INSERT INTO `t_role_auth` VALUES ('157', '8', '31');
INSERT INTO `t_role_auth` VALUES ('158', '8', '32');
INSERT INTO `t_role_auth` VALUES ('159', '8', '36');
INSERT INTO `t_role_auth` VALUES ('160', '8', '37');
INSERT INTO `t_role_auth` VALUES ('161', '8', '38');
INSERT INTO `t_role_auth` VALUES ('162', '8', '39');
INSERT INTO `t_role_auth` VALUES ('163', '8', '4');
INSERT INTO `t_role_auth` VALUES ('164', '8', '40');
INSERT INTO `t_role_auth` VALUES ('165', '8', '41');
INSERT INTO `t_role_auth` VALUES ('166', '8', '5');
INSERT INTO `t_role_auth` VALUES ('167', '8', '6');

-- ----------------------------
-- Table structure for `t_special_apply`
-- ----------------------------
DROP TABLE IF EXISTS `t_special_apply`;
CREATE TABLE `t_special_apply` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `wzuuid` varchar(20) DEFAULT NULL,
  `pydm` varchar(10) DEFAULT NULL COMMENT '拼音代码',
  `applyPersonId` varchar(32) DEFAULT NULL COMMENT '申请人 ID',
  `sccs` varchar(32) DEFAULT NULL COMMENT '生产厂商',
  `scph` varchar(20) DEFAULT NULL COMMENT '生产批号',
  `wzsl` varchar(32) DEFAULT NULL COMMENT '物资数量',
  `tid` varchar(32) DEFAULT NULL COMMENT '追溯ID',
  `applyTime` varchar(19) DEFAULT NULL COMMENT '申请 时间',
  `createTime` varchar(19) DEFAULT NULL COMMENT '创建时间',
  `updateTime` varchar(19) DEFAULT NULL,
  `createor` varchar(20) DEFAULT NULL,
  `updateor` varchar(20) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_special_apply
-- ----------------------------
INSERT INTO `t_special_apply` VALUES ('5', '物资编码UUID1', '拼音代码1', '0001', '生产厂商12', '1', '1', '03eead0c0b7c4257b828804a2fc56b15', '2018-07-24 21:18:32', '2018-07-24 21:18:32', '2018-07-24 21:30:10', 'admin', null, null);

-- ----------------------------
-- Table structure for `t_trace_clean`
-- ----------------------------
DROP TABLE IF EXISTS `t_trace_clean`;
CREATE TABLE `t_trace_clean` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `personId` varchar(20) NOT NULL,
  `tid` varchar(32) NOT NULL,
  `remark` varchar(256) DEFAULT NULL,
  `createTime` varchar(19) DEFAULT NULL,
  `updateTime` varchar(19) DEFAULT NULL,
  `createor` varchar(24) DEFAULT NULL,
  `updateor` varchar(24) DEFAULT NULL,
  `wzuuid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_trace_clean
-- ----------------------------
INSERT INTO `t_trace_clean` VALUES ('16', '0001', '58e47fc652af4ef1b92aac5704effd5d', '清洗人员:孙思邈', '2018-07-25 21:39:19', null, 'admin', null, '物资编码UUID1');

-- ----------------------------
-- Table structure for `t_trace_disinfect`
-- ----------------------------
DROP TABLE IF EXISTS `t_trace_disinfect`;
CREATE TABLE `t_trace_disinfect` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `personId` varchar(20) NOT NULL,
  `tid` varchar(32) NOT NULL,
  `remark` varchar(256) DEFAULT NULL,
  `createTime` varchar(19) DEFAULT NULL,
  `updateTime` varchar(19) DEFAULT NULL,
  `createor` varchar(24) DEFAULT NULL,
  `updateor` varchar(24) DEFAULT NULL,
  `wzuuid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_trace_disinfect
-- ----------------------------
INSERT INTO `t_trace_disinfect` VALUES ('16', '0001', '58e47fc652af4ef1b92aac5704effd5d', '灭菌人员:孙思邈', '2018-07-25 22:23:35', null, 'admin', null, '物资编码UUID1');

-- ----------------------------
-- Table structure for `t_trace_giveout`
-- ----------------------------
DROP TABLE IF EXISTS `t_trace_giveout`;
CREATE TABLE `t_trace_giveout` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `personId` varchar(20) NOT NULL,
  `tid` varchar(32) NOT NULL,
  `remark` varchar(256) DEFAULT NULL,
  `createTime` varchar(19) DEFAULT NULL,
  `updateTime` varchar(19) DEFAULT NULL,
  `createor` varchar(24) DEFAULT NULL,
  `updateor` varchar(24) DEFAULT NULL,
  `wzuuid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_trace_giveout
-- ----------------------------
INSERT INTO `t_trace_giveout` VALUES ('15', '0001', '58e47fc652af4ef1b92aac5704effd5d', '发放人员:孙思邈', '2018-07-25 21:45:56', null, 'admin', null, '物资编码UUID1');

-- ----------------------------
-- Table structure for `t_trace_pack`
-- ----------------------------
DROP TABLE IF EXISTS `t_trace_pack`;
CREATE TABLE `t_trace_pack` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `personId` varchar(20) NOT NULL,
  `tid` varchar(32) NOT NULL,
  `remark` varchar(256) DEFAULT NULL,
  `createTime` varchar(19) DEFAULT NULL,
  `updateTime` varchar(19) DEFAULT NULL,
  `createor` varchar(24) DEFAULT NULL,
  `updateor` varchar(24) DEFAULT NULL,
  `wzuuid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_trace_pack
-- ----------------------------
INSERT INTO `t_trace_pack` VALUES ('17', '0001', '58e47fc652af4ef1b92aac5704effd5d', '打包人员:孙思邈', '2018-07-25 21:39:36', null, 'admin', null, '物资编码UUID1');

-- ----------------------------
-- Table structure for `t_trace_retrieve`
-- ----------------------------
DROP TABLE IF EXISTS `t_trace_retrieve`;
CREATE TABLE `t_trace_retrieve` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `personId` varchar(20) NOT NULL,
  `tid` varchar(32) NOT NULL,
  `remark` varchar(256) DEFAULT NULL,
  `createTime` varchar(19) DEFAULT NULL,
  `updateTime` varchar(19) DEFAULT NULL,
  `createor` varchar(24) DEFAULT NULL,
  `updateor` varchar(24) DEFAULT NULL,
  `wzuuid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_trace_retrieve
-- ----------------------------
INSERT INTO `t_trace_retrieve` VALUES ('21', '0001', '58e47fc652af4ef1b92aac5704effd5d', '回收人员:孙思邈', '2018-07-25 21:38:54', null, 'admin', null, '物资编码UUID1');

-- ----------------------------
-- Table structure for `t_warehouse`
-- ----------------------------
DROP TABLE IF EXISTS `t_warehouse`;
CREATE TABLE `t_warehouse` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `wzuuids` varchar(1024) DEFAULT NULL COMMENT '物资编码UUID（物品编号）',
  `warehouseName` varchar(32) DEFAULT NULL COMMENT '物品规格',
  `createTime` varchar(19) DEFAULT NULL COMMENT '创建时间',
  `updateTime` varchar(19) DEFAULT NULL,
  `createor` varchar(20) DEFAULT NULL,
  `updateor` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_warehouse
-- ----------------------------
INSERT INTO `t_warehouse` VALUES ('1', '物资编码UUID1', '仓库名称', '2018-07-13 22:41:45', '2018-07-13 22:41:45', 'admin', 'admin');
