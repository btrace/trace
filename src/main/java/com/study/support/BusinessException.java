package com.study.support;

public class BusinessException extends Exception{

	private static final long serialVersionUID = 8825702188062528195L;
	
	private String message;
	
	private Exception e;
	
	public BusinessException(String message) {
		super(message);
		this.message = message;
	}
	
	public BusinessException(String message,Exception e) {
		super(message);
		this.message = message;
		this.e = e;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Exception getE() {
		return e;
	}

	public void setE(Exception e) {
		this.e = e;
	}
}
