package com.study.controller.apply;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.study.common.JsonDTo;
import com.study.common.constant.DisinfectStatus;
import com.study.common.constant.ResultCode;
import com.study.model.Goods;
import com.study.service.GoodsService;
import com.study.util.Page;
import com.study.util.StringUtil;

@Controller
public class GoodsController {

	@Autowired
	private GoodsService goodsService;

	private final static Logger logger = Logger.getLogger(GoodsController.class);

	// 入库业务逻辑操作 begin
	@RequestMapping(value = "/goods/inlist")
	public String inlist(HttpServletRequest request, HttpServletResponse response) {
		return "goods/goodsInList.jsp";
	}

	@RequestMapping(value = "/goods/getInPaginate")
	public @ResponseBody Page getPaginate(HttpServletRequest request, HttpServletResponse response) {
		Page results = new Page();
		String pageNo = request.getParameter("pageNo");
		results.setPageNo(Integer.parseInt(pageNo));
		try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", (results.getPageNo() - 1) * results.getPageSize());
			paramMap.put("pageSize", results.getPageSize());

			String pydm = null;
			if (!StringUtil.isBlank(request.getParameter("pydm"))) {
				pydm = "%" + request.getParameter("pydm") + "%";
			}
			paramMap.put("pydm", pydm);
			paramMap.put("beginCreateTime", request.getParameter("beginCreateTime"));
			paramMap.put("endCreateTime", request.getParameter("endCreateTime"));

			List<Goods> objects = goodsService.getInPaginate(paramMap);
			int total = goodsService.getInPaginateTotal(paramMap);
			results.setTotalPages(results.getPages(total));
			results.getResult().addAll(objects);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/goods/getPaginate: " + e.getMessage());
		}
		return results;
	}

	@RequestMapping(value = "/goods/inAdd")
	public String inAdd(HttpServletRequest request, HttpServletResponse response) {
		return "goods/goodsInAdd.jsp";
	}

	@RequestMapping(value = "/goods/inSave")
	public @ResponseBody JsonDTo save(HttpServletRequest request, HttpServletResponse response, Goods goods) {
		try {
			goods.setWzzt(DisinfectStatus.in.getStatus());
			goods.setWpglid(StringUtil.getUUID());
			goodsService.inSave(goods);
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/goods/inSave" + e.getMessage());
			return JsonDTo.getFail();
		}
	}

	@RequestMapping(value = "/goods/inRemoveList")
	public @ResponseBody JsonDTo removeList(HttpServletRequest request, HttpServletResponse response) {
		try {
			goodsService.inRemove(request.getParameter("ids"));
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return JsonDTo.getFail();
		}
	}

	@RequestMapping(value = "/goods/inEdit")
	public String inEdit(HttpServletRequest request, HttpServletResponse response, String id) {
		request.setAttribute("id", id);
		return "goods/goodsInEdit.jsp";
	}

	@RequestMapping(value = "/goods/inGet")
	public @ResponseBody JsonDTo inGet(HttpServletRequest request, HttpServletResponse response) {
		Goods goods = null;
		try {
			goods = goodsService.inGet(request.getParameter("id"));
			return new JsonDTo(ResultCode.success.value(), ResultCode.success.getDisplay(), goods);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/goods/get:" + e.getMessage());
			return JsonDTo.getFail(e.getMessage());
		}
	}

	@RequestMapping(value = "/goods/inUpdate")
	public @ResponseBody JsonDTo update(HttpServletRequest request, HttpServletResponse response, Goods goods) {
		try {
			goodsService.inUpdate(goods);
			goods.setUpdateor(request.getSession().getAttribute("account").toString());
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/goods/update" + e.getMessage());
			return JsonDTo.getFail();
		}
	}
	// 入库业务逻辑操作 end

	// 库存统计列表 --》 为了 添加出库记录  begin
	@RequestMapping(value = "/goods/stocklistForOut")
	public String stocklistForOut(HttpServletRequest request, HttpServletResponse response) {
		return "goods/goodsStockListForOut.jsp";
	}

	@RequestMapping(value = "/goods/getStockPaginateForOut")
	public @ResponseBody Page getStockPaginateForOut(HttpServletRequest request, HttpServletResponse response) {
		Page results = new Page();
		String pageNo = request.getParameter("pageNo");
		results.setPageNo(Integer.parseInt(pageNo));
		try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", (results.getPageNo() - 1) * results.getPageSize());
			paramMap.put("pageSize", results.getPageSize());

			String pydm = null;
			if (!StringUtil.isBlank(request.getParameter("pydm"))) {
				pydm = "%" + request.getParameter("pydm") + "%";
			}
			paramMap.put("pydm", pydm);
			paramMap.put("beginCreateTime", request.getParameter("beginCreateTime"));
			paramMap.put("endCreateTime", request.getParameter("endCreateTime"));

			List<Goods> objects = goodsService.getStockPaginateForOut(paramMap);
			int total = goodsService.getStockPaginateForOutTotal(paramMap);
			results.setTotalPages(results.getPages(total));
			results.getResult().addAll(objects);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/goods/getStockPaginate: " + e.getMessage());
		}
		return results;
	}

	/**
	 * 这个其实就是  就是copy 一份数据 从in 到 out
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/goods/stockEditForOut")
	public String outAdd(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("id", request.getParameter("id"));
		return "goods/goodsStockEditForOut.jsp";
	}
	
	
	@RequestMapping(value = "/goods/stockGetForOut")
	public @ResponseBody JsonDTo stockGetForOut(HttpServletRequest request, HttpServletResponse response) {
		Goods goods = null;
		try {
			goods = goodsService.stockGetForOut(request.getParameter("id"));
			return new JsonDTo(ResultCode.success.value(), ResultCode.success.getDisplay(), goods);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/goods/get:" + e.getMessage());
			return JsonDTo.getFail(e.getMessage());
		}
	}
	/**
	 * 出库数据 其实就是 入库的数据 copy 一份到 out表，in-》out
	 */
	@RequestMapping(value = "/goods/outSave")
	public @ResponseBody JsonDTo outSave(HttpServletRequest request, HttpServletResponse response) {
		try {
			Goods goods = goodsService.inGet(request.getParameter("id"));
			goods.setInId(request.getParameter("id"));
			goods.setWzsl(request.getParameter("wzsl"));
			goods.setWzzt(DisinfectStatus.out.getStatus());
			goods.setWpglid(StringUtil.getUUID());
			goods.setCzyuuid(request.getSession().getAttribute("account").toString());
			goodsService.outSave(goods);
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/goods/outSave" + e.getMessage());
			return JsonDTo.getFail();
		}
	}
	// 库存统计列表 --》 为了 添加出库记录  end
	
	
	// 库存统计查询begin
	@RequestMapping(value = "/goods/stocklist")
	public String stocklist(HttpServletRequest request, HttpServletResponse response) {
		return "goods/goodsStockList.jsp";
	}

	@RequestMapping(value = "/goods/getStockPaginate")
	public @ResponseBody Page getStockPaginate(HttpServletRequest request, HttpServletResponse response) {
		Page results = new Page();
		String pageNo = request.getParameter("pageNo");
		results.setPageNo(Integer.parseInt(pageNo));
		try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", (results.getPageNo() - 1) * results.getPageSize());
			paramMap.put("pageSize", results.getPageSize());

			String pydm = null;
			String wpmc = null;
			String scph = null;
			String sccs = null;
			if (!StringUtil.isBlank(request.getParameter("pydm"))) {
				pydm = "%" + request.getParameter("pydm") + "%";
			}
			if (!StringUtil.isBlank(request.getParameter("wpmc"))) {
				wpmc = "%" + request.getParameter("wpmc") + "%";
			}
			if (!StringUtil.isBlank(request.getParameter("scph"))) {
				scph = "%" + request.getParameter("scph") + "%";
			}
			if (!StringUtil.isBlank(request.getParameter("sccs"))) {
				sccs = "%" + request.getParameter("sccs") + "%";
			}
			paramMap.put("pydm", pydm);
			paramMap.put("wpmc", wpmc);
			paramMap.put("scph", scph);
			paramMap.put("sccs", sccs);
			paramMap.put("beginCreateTime", request.getParameter("beginCreateTime"));
			paramMap.put("endCreateTime", request.getParameter("endCreateTime"));

			List<Goods> objects = goodsService.getStockPaginate(paramMap);
			int total = goodsService.getStockPaginateTotal(paramMap);
			results.setTotalPages(results.getPages(total));
			results.getResult().addAll(objects);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/goods/getStockPaginate: " + e.getMessage());
		}
		return results;
	}

	/**
	 * 库存统计详情页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/goods/stackDetail")
	public String stackDetail(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("id", request.getParameter("id"));
		return "goods/goodsStockDetail.jsp";
	}
	// 库存统计查询end

	
	// 出库业务逻辑操作 begin
	@RequestMapping(value = "/goods/outlist")
	public String outlist(HttpServletRequest request, HttpServletResponse response) {
		return "goods/goodsOutList.jsp";
	}

	@RequestMapping(value = "/goods/getOutPaginate")
	public @ResponseBody Page getOutPaginate(HttpServletRequest request, HttpServletResponse response) {
		Page results = new Page();
		String pageNo = request.getParameter("pageNo");
		results.setPageNo(Integer.parseInt(pageNo));
		try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", (results.getPageNo() - 1) * results.getPageSize());
			paramMap.put("pageSize", results.getPageSize());

			String pydm = null;
			if (!StringUtil.isBlank(request.getParameter("pydm"))) {
				pydm = "%" + request.getParameter("pydm") + "%";
			}
			paramMap.put("pydm", pydm);
			paramMap.put("beginCreateTime", request.getParameter("beginCreateTime"));
			paramMap.put("endCreateTime", request.getParameter("endCreateTime"));

			List<Goods> objects = goodsService.getOutPaginate(paramMap);
			int total = goodsService.getOutPaginateTotal(paramMap);
			results.setTotalPages(results.getPages(total));
			results.getResult().addAll(objects);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/goods/getPaginate: " + e.getMessage());
		}
		return results;
	}

	@RequestMapping(value = "/goods/outRemoveList")
	public @ResponseBody JsonDTo outRemoveList(HttpServletRequest request, HttpServletResponse response) {
		try {
			goodsService.outRemove(request.getParameter("ids"));
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return JsonDTo.getFail();
		}
	}

	@RequestMapping(value = "/goods/outEdit")
	public String outEdit(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("id", request.getParameter("id"));
		return "goods/goodsOutEdit.jsp";
	}
	
	@RequestMapping(value = "/goods/outDetail")
	public String outDetail(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("id", request.getParameter("id"));
		return "goods/goodsOutDetail.jsp";
	}

	@RequestMapping(value = "/goods/outGet")
	public @ResponseBody JsonDTo outGet(HttpServletRequest request, HttpServletResponse response) {
		Goods goods = null;
		try {
			goods = goodsService.outGet(request.getParameter("id"));
			return new JsonDTo(ResultCode.success.value(), ResultCode.success.getDisplay(), goods);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/goods/outGet:" + e.getMessage());
			return JsonDTo.getFail(e.getMessage());
		}
	}

	@RequestMapping(value = "/goods/outUpdate")
	public @ResponseBody JsonDTo outUpdate(HttpServletRequest request, HttpServletResponse response, Goods goods) {
		try {
			goodsService.outUpdate(goods);
			goods.setUpdateor(request.getSession().getAttribute("account").toString());
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/goods/outUpdate" + e.getMessage());
			return JsonDTo.getFail();
		}
	}
	// 出库业务逻辑操作 end
}