package com.study.controller.apply;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.study.common.JsonDTo;
import com.study.common.constant.ResultCode;
import com.study.model.NonDisposable;
import com.study.service.NonDisposableService;
import com.study.util.Page;
import com.study.util.StringUtil;

@Controller
public class NonDisposableController {

	@Autowired
	private NonDisposableService nonDisposableService;

	private final static Logger logger = Logger.getLogger(NonDisposableController.class);

	//申请
	@RequestMapping(value = "/nonDisposableApply/list")
	public String applyList(HttpServletRequest request, HttpServletResponse response) {
		return "nonDisposable/nonDisposableApplyList.jsp";
	}

	@RequestMapping(value = "/nonDisposableApply/toAdd")
	public String applytoAdd(HttpServletRequest request, HttpServletResponse response) {
		return "nonDisposable/nonDisposableApplyAdd.jsp";
	}

	@RequestMapping(value = "/nonDisposableApply/toEdit")
	public String applytoEdit(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("id", request.getParameter("id"));
		return "nonDisposable/nonDisposableApplyEdit.jsp";
	}
	//借用
	@RequestMapping(value = "/nonDisposableBorrow/list")
	public String borrowList(HttpServletRequest request, HttpServletResponse response) {
		return "nonDisposable/nonDisposableBorrowList.jsp";
	}
	@RequestMapping(value = "/nonDisposableBorrow/toEdit")
	public String borrowtoEdit(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("id", request.getParameter("id"));
		return "nonDisposable/nonDisposableBorrowEdit.jsp";
	}
	
	//归还
	@RequestMapping(value = "/nonDisposableRevert/list")
	public String revertList(HttpServletRequest request, HttpServletResponse response) {
		return "nonDisposable/nonDisposableRevertList.jsp";
	}
	@RequestMapping(value = "/nonDisposableRevert/toEdit")
	public String reverttoEdit(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("id", request.getParameter("id"));
		return "nonDisposable/nonDisposableRevertEdit.jsp";
	}

	@RequestMapping(value = "/nonDisposable/getPaginate")
	public @ResponseBody Page getPaginate(HttpServletRequest request, HttpServletResponse response) {
		Page result = new Page();
		String pageNo = request.getParameter("pageNo");
		result.setPageNo(Integer.parseInt(pageNo));
		try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", (result.getPageNo() - 1) * result.getPageSize());
			paramMap.put("pageSize", result.getPageSize());

			String pydm = null;
			if (!StringUtil.isBlank(request.getParameter("pydm"))) {
				pydm = "%" + request.getParameter("pydm") + "%";
			}
			paramMap.put("pydm", pydm);
			paramMap.put("status", request.getParameter("status"));
			paramMap.put("beginCreateTime", request.getParameter("beginCreateTime"));
			paramMap.put("endCreateTime", request.getParameter("endCreateTime"));

			List<NonDisposable> objs = nonDisposableService.getPaginate(paramMap);
			int total = nonDisposableService.getTotal(paramMap);

			result.setTotalPages(result.getPages(total));
			result.getResult().addAll(objs);
		} catch (Exception e) {
			logger.error("/nonDisposable/getPaginate: " + e.getMessage());
		}
		return result;
	}

	@RequestMapping(value = "/nonDisposable/removeList")
	public @ResponseBody JsonDTo removeList(HttpServletRequest request, HttpServletResponse response) {
		try {
			nonDisposableService.remove(request.getParameter("ids"));
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return JsonDTo.getFail();
		}
	}

	@RequestMapping(value = "/nonDisposable/save")
	public @ResponseBody JsonDTo save(HttpServletRequest request, HttpServletResponse response, NonDisposable obj) {
		try {
			obj.setCreateor(request.getSession().getAttribute("account").toString());// 如果是保存的话，更新人和创建人是一致的
			obj.setTid(StringUtil.getUUID());
			nonDisposableService.save(obj);
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			return JsonDTo.getFail();
		}
	}

	@RequestMapping(value = "/nonDisposable/update")
	public @ResponseBody JsonDTo update(HttpServletRequest request, HttpServletResponse response, NonDisposable obj) {
		try {
			obj.setUpdateor(request.getSession().getAttribute("account").toString());// 如果是保存的话，更新人和创建人是一致的
			nonDisposableService.update(obj);
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			return JsonDTo.getFail();
		}
	}

	@RequestMapping(value = "/nonDisposable/get")
	public @ResponseBody JsonDTo get(HttpServletRequest request, HttpServletResponse response) {
		try {
			Map<String,Object>  paramMap = new HashMap<String,Object>();
			paramMap.put("id", request.getParameter("id"));
			return new JsonDTo(ResultCode.success.value(), ResultCode.success.getDisplay(), nonDisposableService.get(paramMap));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/nonDisposable/get: " + e.getMessage());
			return JsonDTo.getFail();
		}
	}
	

	@RequestMapping(value = "/apply/list")
	public String applylist(HttpServletRequest request, HttpServletResponse response) {
		return "apply/applyList.jsp";
	}

	@RequestMapping(value = "/apply/getPaginate")
	public @ResponseBody Page applyGetPaginate(HttpServletRequest request, HttpServletResponse response) {
		Page result = new Page();
		String pageNo = request.getParameter("pageNo");
		result.setPageNo(Integer.parseInt(pageNo));
		try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", (result.getPageNo() - 1) * result.getPageSize());
			paramMap.put("pageSize", result.getPageSize());

			String pydm = null;
			if (!StringUtil.isBlank(request.getParameter("pydm"))) {
				pydm = "%" + request.getParameter("pydm") + "%";
			}
			paramMap.put("pydm", pydm);
			paramMap.put("beginCreateTime", request.getParameter("beginCreateTime"));
			paramMap.put("endCreateTime", request.getParameter("endCreateTime"));

			List<NonDisposable> newsLists = nonDisposableService.getAllApplyPaginate(paramMap);
			int total = nonDisposableService.getAllApplyTotal(paramMap);

			result.setTotalPages(result.getPages(total));
			result.getResult().addAll(newsLists);
		} catch (Exception e) {
			logger.error("/apply/getPaginate: " + e.getMessage());
		}
		return result;
	}
}