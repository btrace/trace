package com.study.controller.apply;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.study.common.JsonDTo;
import com.study.common.constant.ResultCode;
import com.study.model.Special;
import com.study.service.SpecialService;
import com.study.util.Page;
import com.study.util.StringUtil;

@Controller
public class SpecialController {

	@Autowired
	private SpecialService specialService;

	private final static Logger logger = Logger.getLogger(SpecialController.class);

	@RequestMapping(value = "/special/list")
	public String list(HttpServletRequest request, HttpServletResponse response) {
		return "special/specialList.jsp";
	}

	@RequestMapping(value = "/special/toAdd")
	public String toAdd(HttpServletRequest request, HttpServletResponse response) {
		return "special/specialAdd.jsp";
	}

	@RequestMapping(value = "/special/toEdit")
	public String toEdit(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("id", request.getParameter("id"));
		return "special/specialEdit.jsp";
	}

	@RequestMapping(value = "/special/getPaginate")
	public @ResponseBody Page getPaginate(HttpServletRequest request, HttpServletResponse response) {
		Page result = new Page();
		String pageNo = request.getParameter("pageNo");
		result.setPageNo(Integer.parseInt(pageNo));
		try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", (result.getPageNo() - 1) * result.getPageSize());
			paramMap.put("pageSize", result.getPageSize());

			String pydm = null;
			if (!StringUtil.isBlank(request.getParameter("pydm"))) {
				pydm = "%" + request.getParameter("pydm") + "%";
			}
			paramMap.put("pydm", pydm);
			paramMap.put("beginCreateTime", request.getParameter("beginCreateTime"));
			paramMap.put("endCreateTime", request.getParameter("endCreateTime"));

			List<Special> newsLists = specialService.getPaginate(paramMap);
			int total = specialService.getTotal(paramMap);

			result.setTotalPages(result.getPages(total));
			result.getResult().addAll(newsLists);
		} catch (Exception e) {
			logger.error("/special/getPaginate: " + e.getMessage());
		}
		return result;
	}

	@RequestMapping(value = "/special/removeList")
	public @ResponseBody JsonDTo removeList(HttpServletRequest request, HttpServletResponse response) {
		try {
			specialService.remove(request.getParameter("ids"));
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return JsonDTo.getFail();
		}
	}

	@RequestMapping(value = "/special/save")
	public @ResponseBody JsonDTo save(HttpServletRequest request, HttpServletResponse response, Special obj) {
		try {
			obj.setCreateor(request.getSession().getAttribute("account").toString());// 如果是保存的话，更新人和创建人是一致的
			obj.setTid(StringUtil.getUUID());
			specialService.save(obj);
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			return JsonDTo.getFail();
		}
	}

	@RequestMapping(value = "/special/update")
	public @ResponseBody JsonDTo update(HttpServletRequest request, HttpServletResponse response, Special obj) {
		try {
			obj.setUpdateor(request.getSession().getAttribute("account").toString());// 如果是保存的话，更新人和创建人是一致的
			specialService.update(obj);
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			return JsonDTo.getFail();
		}
	}

	@RequestMapping(value = "/special/get")
	public @ResponseBody JsonDTo get(HttpServletRequest request, HttpServletResponse response) {
		try {
			Map<String,Object>  paramMap = new HashMap<String,Object>();
			paramMap.put("id", request.getParameter("id"));
			return new JsonDTo(ResultCode.success.value(), ResultCode.success.getDisplay(), specialService.get(paramMap));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/special/get: " + e.getMessage());
			return JsonDTo.getFail();
		}
	}
}