package com.study.controller.apply;

import com.study.common.JsonDTo;
import com.study.common.constant.ResultCode;
import com.study.model.GoodsType;
import com.study.model.Warehouse;
import com.study.service.GoodsTypeService;
import com.study.util.ImportExcelUtil;
import com.study.util.Page;
import com.study.util.StringUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class GoodsTypeController {

    @Autowired
    private GoodsTypeService goodsTypeService;

    private final static Logger logger = Logger.getLogger(GoodsTypeController.class);

    @RequestMapping(value = "/goodsType/list")
    public String list(HttpServletRequest request, HttpServletResponse response) {
        return "goodsType/goodsTypeList.jsp";
    }

    @RequestMapping(value = "/goodsType/getPaginate")
    public @ResponseBody
    Page getPaginate(HttpServletRequest request, HttpServletResponse response) {
        Page goods = new Page();
        String pageNo = request.getParameter("pageNo");
        String goodsType = request.getParameter("goodsType");
        goods.setPageNo(Integer.parseInt(pageNo));
        try {
            String searchValue = "%" + StringUtil.getStringObj(request.getParameter("searchValue")) + "%";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("page", (goods.getPageNo() - 1) * goods.getPageSize());
            paramMap.put("pageSize", goods.getPageSize());
            paramMap.put("searchValue", searchValue);
            paramMap.put("goodsType", goodsType);
            List<GoodsType> newsLists = goodsTypeService.getPaginate(paramMap);
            int total = goodsTypeService.getTotal(paramMap);
            goods.setTotalPages(goods.getPages(total));
            goods.getResult().addAll(newsLists);
        } catch (Exception e) {
            logger.error("/goodsType/getDispPaginate.do: " + e.getMessage());
            e.printStackTrace();
        }
        return goods;
    }

    @RequestMapping(value = "/goodsType/removeList.do")
    public @ResponseBody
    JsonDTo batchRemove(HttpServletRequest request, HttpServletResponse response) {
        try {
            goodsTypeService.remove(request.getParameter("ids"));
            return JsonDTo.getFail();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return JsonDTo.getSuccess();
        }
    }

    @RequestMapping(value = "/goodsType/getWarehouseByWzuuid")
    public @ResponseBody
    JsonDTo getWarehouseByWzuuid(HttpServletRequest request, HttpServletResponse response, GoodsType goods) {
    	try {
    		return JsonDTo.getSuccess(goodsTypeService.getWarehouseByWzuuid(goods));
    	} catch (Exception e) {
    		e.printStackTrace();
    		logger.error("/goodsType/getWarehouseByWzuuid: " + e.getMessage());
    		return JsonDTo.getFail();
    	}
    }
    
    @RequestMapping(value = "/goodsType/getAllGoodsType")
    public @ResponseBody
    JsonDTo getAllGoodsType(HttpServletRequest request, HttpServletResponse response, GoodsType goods) {
        try {
            return JsonDTo.getSuccess(goodsTypeService.getAllGoodsType(goods));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("/goodsType/getAllGoodsType: " + e.getMessage());
            return JsonDTo.getFail();
        }
    }
    
    @RequestMapping(value = "/goodsType/getAllSccssByPydm")
    public @ResponseBody
    JsonDTo getAllSccssByPydm(HttpServletRequest request, HttpServletResponse response, GoodsType goods) {
    	try {
    		return JsonDTo.getSuccess(goodsTypeService.getAllSccssByPydm(goods));
    	} catch (Exception e) {
    		e.printStackTrace();
    		logger.error("/goodsType/getAllSccssByPydm: " + e.getMessage());
    		return JsonDTo.getFail();
    	}
    }

    @RequestMapping(value = "/goodsType/get")
    public @ResponseBody
    JsonDTo get(HttpServletRequest request, HttpServletResponse response,GoodsType obj) {
        GoodsType goods = null;
        try {
            goods = goodsTypeService.get(obj);
            return new JsonDTo(ResultCode.success.value(), ResultCode.success.getDisplay(), goods);
        } catch (Exception e) {
            logger.error("/goodsType/get: " + e.getMessage());
            return JsonDTo.getFail();
        }
    }


    @RequestMapping(value = "/goodsType/inexcel")
    public String inexcel(HttpServletRequest request, HttpServletResponse response) {
        return "database/inexcel.jsp";
    }

    @RequestMapping(value = "/goodsType/addGoodsType", method = RequestMethod.POST)
    public @ResponseBody
    JsonDTo addGoodsUpload(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            InputStream in = null;
            List<List<Object>> listob = null;
            MultipartFile file = multipartRequest.getFile("addGoods");
            in = file.getInputStream();
            listob = new ImportExcelUtil().getBankListByExcelForGoods(in, file.getOriginalFilename());

            GoodsType goodsType = null;
            List<GoodsType> insertList = new ArrayList<GoodsType>();
            for (int i = 0; i < listob.size(); i++) {
                List<Object> lo = listob.get(i);
                goodsType = new GoodsType();
//				String new_model = StringUtil.getStringObj(lo.get(0));
//				if (!StringUtil.isBlank(new_model) && new_model.endsWith(".0")) {
//					new_model = new_model.substring(0, new_model.length() - 2);
//				}
                goodsType.setWzuuid(StringUtil.getStringObj(lo.get(0)));
                goodsType.setPydm(StringUtil.getStringObj(lo.get(1)));
                goodsType.setWpmc(StringUtil.getStringObj(lo.get(2)));
                goodsType.setWpgg(StringUtil.getStringObj(lo.get(3)));
                goodsType.setWpdw(StringUtil.getStringObj(lo.get(4)));
                goodsType.setZxgg(StringUtil.getStringObj(lo.get(5)));
                goodsType.setSccs(StringUtil.getStringObj(lo.get(6)));
                goodsType.setMrdj(StringUtil.getStringObj(lo.get(7)));
                goodsType.setWpfl(StringUtil.getStringObj(lo.get(8)));
                goodsType.setCreateor(request.getSession().getAttribute("account").toString());
                insertList.add(goodsType);
            }
//            List<String> errorList = goodsService.checkGoods(insertList);
            goodsTypeService.saveList(insertList);
            return JsonDTo.getSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            return JsonDTo.getFail();
        }
    }

    @RequestMapping(value = "/goodsType/addWarehouse", method = RequestMethod.POST)
    public @ResponseBody
    JsonDTo addWarehouse(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            InputStream in = null;
            List<List<Object>> listob = null;
            MultipartFile file = multipartRequest.getFile("addWarehouse");
            in = file.getInputStream();
            listob = new ImportExcelUtil().getBankListByExcelForGoods(in, file.getOriginalFilename());

            Warehouse warehouse = null;
            List<Warehouse> insertList = new ArrayList<Warehouse>();
            for (int i = 0; i < listob.size(); i++) {
                List<Object> lo = listob.get(i);
                warehouse = new Warehouse();
//				String new_model = StringUtil.getStringObj(lo.get(0));
//				if (!StringUtil.isBlank(new_model) && new_model.endsWith(".0")) {
//					new_model = new_model.substring(0, new_model.length() - 2);
//				}
                warehouse.setWzuuids(StringUtil.getStringObj(lo.get(0)));
                warehouse.setWarehouseName(StringUtil.getStringObj(lo.get(1)));
                warehouse.setCreateor(request.getSession().getAttribute("account").toString());
            }
//            List<String> errorList = goodsService.checkGoods(insertList);
            goodsTypeService.saveWarehouseList(insertList);
            return JsonDTo.getSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            return JsonDTo.getFail();
        }
    }
}