package com.study.controller.trace;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.study.common.JsonDTo;
import com.study.model.Clean;
import com.study.model.Person;
import com.study.service.CleanService;
import com.study.service.PersonService;
import com.study.util.Page;
import com.study.util.StringUtil;

@Controller
public class CleanController {

	@Autowired
	private PersonService personService;

	@Autowired
	private CleanService cleanService;

	private final static Logger logger = Logger.getLogger(CleanController.class);

	@RequestMapping(value = "/clean/list")
	public String retrieveList(HttpServletRequest request, HttpServletResponse response) {
		return "clean/cleanList.jsp";
	}

	@RequestMapping(value = "/clean/toAdd")
	public String toAdd(HttpServletRequest request, HttpServletResponse response) {
		return "clean/cleanAdd.jsp";
	}

	@RequestMapping(value = "/clean/getPaginate")
	public @ResponseBody Page getPaginate(HttpServletRequest request, HttpServletResponse response) {
		Page objs = new Page();
		String pageNo = request.getParameter("pageNo");
		objs.setPageNo(Integer.parseInt(pageNo));
		try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", (objs.getPageNo() - 1) * objs.getPageSize());
			paramMap.put("pageSize", objs.getPageSize());
			String pydm = null;
			if (!StringUtil.isBlank(request.getParameter("pydm"))) {
				pydm = "%" + request.getParameter("pydm") + "%";
			}
			paramMap.put("pydm", pydm);
			paramMap.put("beginCreateTime", request.getParameter("beginCreateTime"));
			paramMap.put("endCreateTime", request.getParameter("endCreateTime"));

			List<Clean> results = cleanService.getPaginate(paramMap);
			int total = cleanService.getTotal(paramMap);
			objs.setTotalPages(objs.getPages(total));
			objs.getResult().addAll(results);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/clean/getPaginate: " + e.getMessage());
		}
		return objs;
	}

	@RequestMapping(value = "/clean/save")
	public @ResponseBody JsonDTo save(HttpServletRequest request, HttpServletResponse response, Clean obj) {
		try {
			obj.setCreateor(request.getSession().getAttribute("account").toString());
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("personId", obj.getPersonId());
			Person person = personService.get(paramMap);
			obj.setRemark("清洗人员:"+person.getName());
			cleanService.save(obj);
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/clean/save," + e.getMessage());
			return JsonDTo.getFail();
		}
	}
}