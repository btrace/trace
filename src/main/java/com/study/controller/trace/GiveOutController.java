package com.study.controller.trace;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.study.common.JsonDTo;
import com.study.model.GiveOut;
import com.study.model.Person;
import com.study.service.GiveOutService;
import com.study.service.PersonService;
import com.study.util.Page;
import com.study.util.StringUtil;

@Controller
public class GiveOutController {
	
	@Autowired
	private PersonService personService;

	@Autowired
	private GiveOutService giveoutService;

	private final static Logger logger = Logger.getLogger(GiveOutController.class);

	@RequestMapping(value = "/giveout/list")
	public String list(HttpServletRequest request, HttpServletResponse response) {
		return "giveout/giveoutList.jsp";
	}

	@RequestMapping(value = "/giveout/toAdd")
	public String toAdd(HttpServletRequest request, HttpServletResponse response) {
		return "giveout/giveoutAdd.jsp";
	}

	@RequestMapping(value = "/giveout/getPaginate")
	public @ResponseBody Page getPaginate(HttpServletRequest request, HttpServletResponse response) {
		Page objs = new Page();
		String pageNo = request.getParameter("pageNo");
		objs.setPageNo(Integer.parseInt(pageNo));
		try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", (objs.getPageNo() - 1) * objs.getPageSize());
			paramMap.put("pageSize", objs.getPageSize());
			String pydm = null;
			if (!StringUtil.isBlank(request.getParameter("pydm"))) {
				pydm = "%" + request.getParameter("pydm") + "%";
			}
			paramMap.put("pydm", pydm);
			paramMap.put("beginCreateTime", request.getParameter("beginCreateTime"));
			paramMap.put("endCreateTime", request.getParameter("endCreateTime"));

			List<GiveOut> results = giveoutService.getPaginate(paramMap);
			int total = giveoutService.getTotal(paramMap);
			objs.setTotalPages(objs.getPages(total));
			objs.getResult().addAll(results);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/giveout/getPaginate: " + e.getMessage());
		}
		return objs;
	}

	@RequestMapping(value = "/giveout/save")
	public @ResponseBody JsonDTo save(HttpServletRequest request, HttpServletResponse response, GiveOut obj) {
		try {
			obj.setCreateor(request.getSession().getAttribute("account").toString());
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("personId", obj.getPersonId());
			Person person = personService.get(paramMap);
			obj.setRemark("发放人员:"+person.getName());
			giveoutService.save(obj);
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/giveout/save," + e.getMessage());
			return JsonDTo.getFail();
		}
	}

	@RequestMapping(value = "/goodTrace/list")
	public String goodTraceList(HttpServletRequest request, HttpServletResponse response) {
		return "goodTrace/goodTraceList.jsp";
	}

	@RequestMapping(value = "/goodTrace/getPaginate")
	public @ResponseBody Page getTracePaginate(HttpServletRequest request, HttpServletResponse response) {
		Page objs = new Page();
		String pageNo = request.getParameter("pageNo");
		objs.setPageNo(Integer.parseInt(pageNo));
		try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", (objs.getPageNo() - 1) * objs.getPageSize());
			paramMap.put("pageSize", objs.getPageSize());
			String pydm = null;
			if (!StringUtil.isBlank(request.getParameter("pydm"))) {
				pydm = "%" + request.getParameter("pydm") + "%";
			}
			paramMap.put("pydm", pydm);
			paramMap.put("beginCreateTime", request.getParameter("beginCreateTime"));
			paramMap.put("endCreateTime", request.getParameter("endCreateTime"));

			List<Map<String,Object>> results = giveoutService.getTracePaginate(paramMap);
			int total = giveoutService.getTraceTotal(paramMap);
			objs.setTotalPages(objs.getPages(total));
			objs.getResult().addAll(results);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/goodTrace/getPaginate: " + e.getMessage());
		}
		return objs;
	}

	
}