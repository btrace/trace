package com.study.controller.trace;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.study.common.JsonDTo;
import com.study.model.Disinfect;
import com.study.model.Person;
import com.study.service.DisinfectService;
import com.study.service.PersonService;
import com.study.util.Page;

@Controller
public class DisinfectController {
	
	@Autowired
	private PersonService personService;

	@Autowired
	private DisinfectService disinfectService;

	private final static Logger logger = Logger.getLogger(DisinfectController.class);

	@RequestMapping(value = "/disinfect/list")
	public String retrieveList(HttpServletRequest request, HttpServletResponse response) {
		return "disinfect/disinfectList.jsp";
	}

	@RequestMapping(value = "/disinfect/toAdd")
	public String toAdd(HttpServletRequest request, HttpServletResponse response) {
		return "disinfect/disinfectAdd.jsp";
	}

	@RequestMapping(value = "/disinfect/getPaginate")
	public @ResponseBody Page getPaginate(HttpServletRequest request, HttpServletResponse response) {
		Page objs = new Page();
		String pageNo = request.getParameter("pageNo");
		objs.setPageNo(Integer.parseInt(pageNo));
		try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", (objs.getPageNo() - 1) * objs.getPageSize());
			paramMap.put("pageSize", objs.getPageSize());
			paramMap.put("beginCreateTime", request.getParameter("beginCreateTime"));
			paramMap.put("endCreateTime", request.getParameter("endCreateTime"));
			List<Disinfect> results = disinfectService.getPaginate(paramMap);
			int total = disinfectService.getTotal(paramMap);
			objs.setTotalPages(objs.getPages(total));
			objs.getResult().addAll(results);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/disinfect/getPaginate: " + e.getMessage());
		}
		return objs;
	}

	@RequestMapping(value = "/disinfect/save")
	public @ResponseBody JsonDTo save(HttpServletRequest request, HttpServletResponse response, Disinfect obj) {
		try {
			obj.setCreateor(request.getSession().getAttribute("account").toString());
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("personId", obj.getPersonId());
			Person person = personService.get(paramMap);
			obj.setRemark("灭菌人员:"+person.getName());
			disinfectService.save(obj);
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/clean/save," + e.getMessage());
			return JsonDTo.getFail();
		}
	}
}