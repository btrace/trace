package com.study.controller.trace;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.study.common.JsonDTo;
import com.study.model.Person;
import com.study.model.Retrieve;
import com.study.service.PersonService;
import com.study.service.RetrieveService;
import com.study.util.Page;
import com.study.util.StringUtil;

@Controller
public class RetrieveController {
	
	@Autowired
	private PersonService personService;

	@Autowired
	private RetrieveService retrieveService;

	private final static Logger logger = Logger.getLogger(RetrieveController.class);

	@RequestMapping(value = "/retrieve/list")
	public String retrieveList(HttpServletRequest request, HttpServletResponse response) {
		return "retrieve/retrieveList.jsp";
	}

	@RequestMapping(value = "/retrieve/toAdd")
	public String toAdd(HttpServletRequest request, HttpServletResponse response) {
		return "retrieve/retrieveAdd.jsp";
	}

	@RequestMapping(value = "/retrieve/getPaginate")
	public @ResponseBody Page getPaginate(HttpServletRequest request, HttpServletResponse response) {
		Page objs = new Page();
		String pageNo = request.getParameter("pageNo");
		objs.setPageNo(Integer.parseInt(pageNo));
		try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", (objs.getPageNo() - 1) * objs.getPageSize());
			paramMap.put("pageSize", objs.getPageSize());
			
			String pydm = null;
			if (!StringUtil.isBlank(request.getParameter("pydm"))) {
				pydm = "%" + request.getParameter("pydm") + "%";
			}
			paramMap.put("pydm", pydm);
			paramMap.put("beginCreateTime", request.getParameter("beginCreateTime"));
			paramMap.put("endCreateTime", request.getParameter("endCreateTime"));

			List<Retrieve> results = retrieveService.getPaginate(paramMap);
			int total = retrieveService.getTotal(paramMap);
			objs.setTotalPages(objs.getPages(total));
			objs.getResult().addAll(results);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/retrieve/getPaginate: " + e.getMessage());
		}
		return objs;
	}

	@RequestMapping(value = "/retrieve/save")
	public @ResponseBody JsonDTo save(HttpServletRequest request, HttpServletResponse response, Retrieve obj) {
		try {
			obj.setCreateor(request.getSession().getAttribute("account").toString());
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("personId", obj.getPersonId());
			Person person = personService.get(paramMap);
			obj.setRemark("回收人员:"+person.getName());
			retrieveService.save(obj);
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/retrieve/save," + e.getMessage());
			return JsonDTo.getFail();
		}
	}
}