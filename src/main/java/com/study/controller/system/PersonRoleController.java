package com.study.controller.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.study.common.JsonDTo;
import com.study.common.constant.ResultCode;
import com.study.model.PersonRole;
import com.study.service.PersonRoleService;
import com.study.util.Page;
import com.study.util.StringUtil;

@Controller
public class PersonRoleController {

    @Autowired
    private PersonRoleService personRoleService;

    private final static Logger logger = Logger.getLogger(PersonRoleController.class);

    @RequestMapping(value = "/personRole/toAdd")
    public String toAdd(HttpServletRequest request, HttpServletResponse response) {
        return "personRole/personRoleAdd.jsp";
    }

    @RequestMapping(value = "/personRole/toEdit")
    public String toEdit(HttpServletRequest request, HttpServletResponse response) {
    	request.setAttribute("id", request.getParameter("id"));
        return "personRole/personRoleEdit.jsp";
    }

    @RequestMapping(value = "/personRole/isExist")
    public @ResponseBody
    JsonDTo isExistByDepartmentID(HttpServletRequest request, HttpServletResponse response,
    		PersonRole objParam) {
        try {
        	PersonRole obj = personRoleService.isExist(objParam.getPersonRoleName());
            if (StringUtil.isBlank(objParam.getId())) {
                if (obj == null) {
                    //可以直接添加
                    return JsonDTo.getFail();
                } else {
                    //已经存在 不可以添加
                    return JsonDTo.getSuccess();
                }
            } else {
                if (obj == null || obj.getId() == objParam.getId()) {
                    //可以直接添加
                    return JsonDTo.getFail();
                } else {
                    return JsonDTo.getSuccess();
                }
            }
        } catch (Exception e) {
        	e.printStackTrace();
            logger.error("/personRole/isExist: " + e.getMessage());
            return JsonDTo.getFail();
        }
    }

    @RequestMapping(value = "/personRole/list")
    public String list(HttpServletRequest request, HttpServletResponse response) {
        return "personRole/personRoleList.jsp";
    }

    @RequestMapping(value = "/personRole/getPaginate")
    public @ResponseBody
    Page getPaginate(HttpServletRequest request, HttpServletResponse response) {
        Page results = new Page();
        String pageNo = request.getParameter("pageNo");
        results.setPageNo(Integer.parseInt(pageNo));
        try {
            String searchValue = "%" + StringUtil.getStringObj(request.getParameter("searchValue")) + "%";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("page", (results.getPageNo() - 1) * results.getPageSize());
            paramMap.put("pageSize", results.getPageSize());
            paramMap.put("searchValue", searchValue);
            List<PersonRole> newsLists = personRoleService.getPaginate(paramMap);
            int total = personRoleService.getTotal(paramMap);
            results.setTotalPages(results.getPages(total));
            results.getResult().addAll(newsLists);
        } catch (Exception e) {
        	e.printStackTrace();
            logger.error("/personRole/getPaginate: " + e.getMessage());
        }
        return results;
    }

    @RequestMapping(value = "/personRole/removeList")
    public @ResponseBody
    JsonDTo removeList(HttpServletRequest request, HttpServletResponse response) {
        try {
            personRoleService.remove(request.getParameter("ids"));
            return JsonDTo.getSuccess();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return JsonDTo.getFail();
        }
    }

    @RequestMapping(value = "/personRole/save")
    public @ResponseBody
    JsonDTo save(HttpServletRequest request, HttpServletResponse response,
    		PersonRole obj) {
        try {
        	obj.setCreateor(request.getSession().getAttribute("account").toString());// 如果是保存的话，更新人和创建人是一致的
            personRoleService.save(obj);
            return JsonDTo.getSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            return JsonDTo.getFail();
        }
    }

    @RequestMapping(value = "/personRole/update")
    public @ResponseBody
    JsonDTo update(HttpServletRequest request, HttpServletResponse response,
    		PersonRole obj) {
        try {
        	obj.setUpdateor(request.getSession().getAttribute("account").toString());// 如果是保存的话，更新人和创建人是一致的
            personRoleService.update(obj);
            return JsonDTo.getSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            return JsonDTo.getFail();
        }
    }

    @RequestMapping(value = "/personRole/get")
    public @ResponseBody
    JsonDTo get(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter("id");
        try {
            return new JsonDTo(ResultCode.success.value(), ResultCode.success.getDisplay(), personRoleService.get(id));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("/department/get: " + e.getMessage());
            return JsonDTo.getFail();
        }
    }

    @RequestMapping(value = "/personRole/getAll")
    public @ResponseBody
    JsonDTo getAll(HttpServletRequest request, HttpServletResponse response) {
        List<PersonRole> list = null;
        try {
        	list = personRoleService.getAll();
            return new JsonDTo(ResultCode.success.value(), ResultCode.success.getDisplay(), list);
        } catch (Exception e) {
            logger.error("/personRole/getAll: " + e.getMessage());
            return JsonDTo.getFail();
        }
    }
}