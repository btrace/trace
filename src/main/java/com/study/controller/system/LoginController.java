package com.study.controller.system;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.study.common.JsonDTo;
import com.study.common.constant.ResultCode;
import com.study.model.Admin;
import com.study.model.AuthConfig;
import com.study.service.AdminService;
import com.study.util.DateUtil;
import com.study.util.StringUtil;


@Controller
public class LoginController {

	@Resource
	private AdminService adminService;
	
	@RequestMapping(value = "/getSession")
	public @ResponseBody JsonDTo getSession(HttpServletRequest request,HttpServletResponse response) {
		JsonDTo result = new JsonDTo();
		if (request.getSession().getAttribute("account") == null) {
			result.setCode(ResultCode.success.value());
			result.setMsg("session失效");
			return result;
		}
		result.setCode(ResultCode.fail.value());
		result.setMsg("sessionok");
		return result;
	}

	@RequestMapping(value = "/login")
	public String login(HttpServletRequest request, HttpServletResponse response) {
		if(null == request.getSession().getAttribute("account")){
			try {
				response.sendRedirect("/login.jsp");
			} catch (IOException e) {
				e.printStackTrace();
			}
			return  null;
		}
		Map<String,Object> paramMap = new HashMap<String,Object>();
		String account = request.getSession().getAttribute("account").toString();
		paramMap.put("account", account);
		List<AuthConfig> firstAuthConfigs = adminService.getManagerauth(paramMap);
		request.setAttribute("firstAuthConfigs", firstAuthConfigs);

		if(StringUtil.isBlank(request.getParameter("id"))) {
			paramMap.put("id", firstAuthConfigs.get(0).getId());
			List<AuthConfig> selectAuthConfigs = adminService.getSelectManagerauth(paramMap);
			request.setAttribute("selectAuthConfigs", selectAuthConfigs);
		} else {
			paramMap.put("id", request.getParameter("id"));
			List<AuthConfig> selectAuthConfigs = adminService.getSelectManagerauth(paramMap);
			request.setAttribute("selectAuthConfigs", selectAuthConfigs);
			request.setAttribute("id", request.getParameter("id"));
		}
		request.setAttribute("time", DateUtil.dateToString(new Date(),"yyyy年MM月dd日 HH时mm分ss秒"));
		request.setAttribute("week", DateUtil.getWeekOfDate(new Date()));
		
		Admin obj = (Admin)request.getSession().getAttribute("manager");
		request.setAttribute("departmentName", obj.getDepartmentName());
		
		return "index.jsp";
	}
	
	@RequestMapping(value = "/loginout")
	public String loginout(HttpServletRequest request, HttpServletResponse response) {
		try {
			request.getSession().removeAttribute("account");
			response.sendRedirect(request.getContextPath() + "/login.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}