package com.study.controller.person;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.study.common.JsonDTo;
import com.study.common.constant.ResultCode;
import com.study.model.Person;
import com.study.service.PersonService;
import com.study.util.Page;
import com.study.util.StringUtil;

@Controller
public class PersonController {

	@Autowired
	private PersonService personService;

	private final static Logger logger = Logger.getLogger(PersonController.class);

	@RequestMapping(value = "/person/toAdd")
	public String toAdd(HttpServletRequest request, HttpServletResponse response) {
		return "person/personAdd.jsp";
	}
	
	@RequestMapping(value = "/person/toEdit")
	public String toget(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("id", request.getParameter("id"));
		return "person/personEdit.jsp";
	}

	@RequestMapping(value = "/person/list")
	public String list(HttpServletRequest request, HttpServletResponse response) {
		return "person/personList.jsp";
	}

	@RequestMapping(value = "/person/getPaginate")
	public @ResponseBody Page getPaginate(HttpServletRequest request, HttpServletResponse response) {
		Page objs = new Page();
		String pageNo = request.getParameter("pageNo");
		objs.setPageNo(Integer.parseInt(pageNo));
		try {
			String searchValue = "%" + StringUtil.getStringObj(request.getParameter("searchValue")) + "%";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", (objs.getPageNo() - 1) * objs.getPageSize());
			paramMap.put("pageSize", objs.getPageSize());
			paramMap.put("searchValue", searchValue);
			paramMap.put("departmentId", request.getSession().getAttribute("departmentId"));
			List<Person> newsLists = personService.getPaginate(paramMap);
			int total = personService.getTotal(paramMap);
			objs.setTotalPages(objs.getPages(total));
			objs.getResult().addAll(newsLists);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/person/getPaginate: " + e.getMessage());
		}
		return objs;
	}

	@RequestMapping(value = "/person/removeList")
	public @ResponseBody JsonDTo removeList(HttpServletRequest request, HttpServletResponse response) {
		try {
			personService.remove(request.getParameter("ids"));
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return JsonDTo.getFail();
		}
	}
	

    @RequestMapping(value = "/person/isExist")
    public @ResponseBody
    JsonDTo isExist(HttpServletRequest request, HttpServletResponse response,
                                  Person person) {
        try {
        	Person obj = personService.isExist(person.getPersonId());
            if (StringUtil.isBlank(person.getId())) {
                if (obj == null) {
                    //可以直接添加
                    return JsonDTo.getFail();
                } else {
                    //已经存在 不可以添加
                    return JsonDTo.getSuccess();
                }
            } else {
                if (obj == null || obj.getId() == person.getId()) {
                    //可以直接添加
                    return JsonDTo.getFail();
                } else {
                    return JsonDTo.getSuccess();
                }
            }
        } catch (Exception e) {
            logger.error("/department/isExist: " + e.getMessage());
            return JsonDTo.getFail();
        }
    }

	@RequestMapping(value = "/person/save")
	public @ResponseBody JsonDTo save(HttpServletRequest request, HttpServletResponse response, Person obj) {
		try {
			obj.setCreateor(request.getSession().getAttribute("account").toString());
			personService.save(obj);
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(" /person/save," + e.getMessage());
			return JsonDTo.getFail();
		}
	}

	@RequestMapping(value = "/person/update")
	public @ResponseBody JsonDTo update(HttpServletRequest request, HttpServletResponse response, Person obj) {
		try {
			obj.setUpdateor(request.getSession().getAttribute("account").toString());
			personService.update(obj);
			return JsonDTo.getSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			return JsonDTo.getFail();
		}
	}

	@RequestMapping(value = "/person/get")
	public @ResponseBody JsonDTo get(HttpServletRequest request, HttpServletResponse response) {
		try {
			Map<String,Object>  paramMap = new HashMap<String,Object>();
			paramMap.put("id", request.getParameter("id"));
			paramMap.put("personId", request.getParameter("personId"));
			Person obj = personService.get(paramMap);
			return new JsonDTo(ResultCode.success.value(), ResultCode.success.getDisplay(), obj);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/person/get: " + e.getMessage());
			return JsonDTo.getFail();
		}
	}
}