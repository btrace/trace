package com.study.controller.person;

import com.study.common.JsonDTo;
import com.study.common.constant.ResultCode;
import com.study.model.Department;
import com.study.service.DepartmentService;
import com.study.util.Page;
import com.study.util.StringUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    private final static Logger logger = Logger.getLogger(DepartmentController.class);

    @RequestMapping(value = "/department/toAdd")
    public String toAdd(HttpServletRequest request, HttpServletResponse response) {
        return "department/departmentAdd.jsp";
    }

    @RequestMapping(value = "/department/toEdit")
    public String toEdit(HttpServletRequest request, HttpServletResponse response) {
    	request.setAttribute("id", request.getParameter("id"));
        return "department/departmentEdit.jsp";
    }

    @RequestMapping(value = "/department/isExist")
    public @ResponseBody
    JsonDTo isExistByDepartmentID(HttpServletRequest request, HttpServletResponse response,
                                  Department department) {
        try {
            Department obj = departmentService.isExist(department.getDepartmentName());
            if (StringUtil.isBlank(department.getId())) {
                if (obj == null) {
                    //可以直接添加
                    return JsonDTo.getFail();
                } else {
                    //已经存在 不可以添加
                    return JsonDTo.getSuccess();
                }
            } else {
                if (obj == null || obj.getId() == department.getId()) {
                    //可以直接添加
                    return JsonDTo.getFail();
                } else {
                    return JsonDTo.getSuccess();
                }
            }
        } catch (Exception e) {
            logger.error("/department/isExist: " + e.getMessage());
            return JsonDTo.getFail();
        }
    }

    @RequestMapping(value = "/department/list")
    public String list(HttpServletRequest request, HttpServletResponse response) {
        return "department/departmentList.jsp";
    }

    @RequestMapping(value = "/department/getPaginate")
    public @ResponseBody
    Page getPaginate(HttpServletRequest request, HttpServletResponse response) {
        Page departments = new Page();
        String pageNo = request.getParameter("pageNo");
        departments.setPageNo(Integer.parseInt(pageNo));
        try {
            String searchValue = "%" + StringUtil.getStringObj(request.getParameter("searchValue")) + "%";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("page", (departments.getPageNo() - 1) * departments.getPageSize());
            paramMap.put("pageSize", departments.getPageSize());
            paramMap.put("searchValue", searchValue);
            List<Department> newsLists = departmentService.getPaginate(paramMap);
            int total = departmentService.getTotal(paramMap);
            departments.setTotalPages(departments.getPages(total));
            departments.getResult().addAll(newsLists);
        } catch (Exception e) {
            logger.error("/department/getPaginate: " + e.getMessage());
        }
        return departments;
    }

    @RequestMapping(value = "/department/removeList")
    public @ResponseBody
    JsonDTo removeList(HttpServletRequest request, HttpServletResponse response) {
        try {
            departmentService.remove(request.getParameter("ids"));
            return JsonDTo.getSuccess();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return JsonDTo.getFail();
        }
    }

    @RequestMapping(value = "/department/save")
    public @ResponseBody
    JsonDTo save(HttpServletRequest request, HttpServletResponse response,
                 Department department) {
        try {
            department.setCreateor(request.getSession().getAttribute("account").toString());// 如果是保存的话，更新人和创建人是一致的
            departmentService.save(department);
            return JsonDTo.getSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            return JsonDTo.getFail();
        }
    }

    @RequestMapping(value = "/department/update")
    public @ResponseBody
    JsonDTo update(HttpServletRequest request, HttpServletResponse response,
                   Department department) {
        try {
            department.setUpdateor(request.getSession().getAttribute("account").toString());// 如果是保存的话，更新人和创建人是一致的
            departmentService.update(department);
            return JsonDTo.getSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            return JsonDTo.getFail();
        }
    }

    @RequestMapping(value = "/department/get")
    public @ResponseBody
    JsonDTo get(HttpServletRequest request, HttpServletResponse response) {
        String departmentId = request.getParameter("id");
        try {
            return new JsonDTo(ResultCode.success.value(), ResultCode.success.getDisplay(), departmentService.get(departmentId));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("/department/get: " + e.getMessage());
            return JsonDTo.getFail();
        }
    }

    @RequestMapping(value = "/department/getAllDepartmentInfo")
    public @ResponseBody
    JsonDTo getAllDepartmentInfo(HttpServletRequest request, HttpServletResponse response) {
        List<Department> departmentInfoList = null;
        try {
            departmentInfoList = departmentService.getAllDepartmentInfo();
            return new JsonDTo(ResultCode.success.value(), ResultCode.success.getDisplay(), departmentInfoList);
        } catch (Exception e) {
            logger.error("/department/getAllDepartmentInfo: " + e.getMessage());
            return JsonDTo.getFail();
        }
    }
}