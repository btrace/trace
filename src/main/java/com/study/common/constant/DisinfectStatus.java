package com.study.common.constant;

/**
 *
 * @author Administrator
 */
public enum DisinfectStatus {

	//在库/出库/删除/退回/报损/残缺
	in("0"), out("1"), delete("2");

	/** 定义枚举类型自己的属性 **/
	private String status;

	private DisinfectStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return this.status;
	}
}
