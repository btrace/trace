package com.study.common.constant;

public enum ResultCode {
	success("0", "默认"), password_mistake("003", "用户密码错误!"), manager_not_exist("004", "用户不存在!"), fail("-1", "未知异常！");
	private String code;
	private String display;

	ResultCode(String code, String display) {
		this.code = code;
		this.display = display;
	}

	public String value() {
		return this.code;
	}

	public String getDisplay() {
		return this.display;
	}
}
