package com.study.common;

import java.io.Serializable;

import com.study.common.constant.ResultCode;

public class JsonDTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8905604238367203116L;

    private String code;

    private String msg;

    private Object data = null;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public JsonDTo() {
        super();
    }

    public JsonDTo(String code, String msg) {
        super();
        this.code = code;
        this.msg = msg;
    }

    public JsonDTo(String code, String msg, Object data) {
        super();
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    @Override
    public String toString() {
        return "{'code':" + code + ", 'msg':" + msg + ", 'data':" + data + "}";
    }

    public static JsonDTo getSuccess(Object data) {
        return new JsonDTo(ResultCode.success.value(), ResultCode.success.getDisplay(), data);
    }

    public static JsonDTo getSuccess() {
        return new JsonDTo(ResultCode.success.value(), ResultCode.success.getDisplay() );
    }

    public static JsonDTo getFail() {
        return new JsonDTo(ResultCode.fail.value(), ResultCode.fail.getDisplay());
    }

    public static JsonDTo getFail(String msg) {
        return new JsonDTo(ResultCode.fail.value(), msg);
    }
}
