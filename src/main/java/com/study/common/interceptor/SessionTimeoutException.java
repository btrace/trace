package com.study.common.interceptor;

public class SessionTimeoutException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 586346792016144179L;

	public SessionTimeoutException() {
        super();
    }

    public SessionTimeoutException(String message) {
        super(message);
    }
}
