package com.study.service;

import java.util.List;
import java.util.Map;

import com.study.model.NonDisposable;

public interface NonDisposableService {

	int save(NonDisposable obj);

	int update(NonDisposable obj);

	void remove(String ids);

	NonDisposable get(Map<String, Object> paramMap);

	List<NonDisposable> getPaginate(Map<String, Object> paramMap);

	int getTotal(Map<String, Object> paramMap);

	List<NonDisposable> getAllApplyPaginate(Map<String, Object> paramMap);

	int getAllApplyTotal(Map<String, Object> paramMap);

}
