package com.study.service;

import java.util.List;
import java.util.Map;

import com.study.model.Special;

public interface SpecialService {

	int save(Special obj);

	int update(Special obj);

	void remove(String ids);

	Special get(Map<String, Object> paramMap);

	List<Special> getPaginate(Map<String, Object> paramMap);

	int getTotal(Map<String, Object> paramMap);
}
