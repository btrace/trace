package com.study.service;

import java.util.List;
import java.util.Map;

import com.study.model.Admin;
import com.study.model.AuthConfig;

public interface AdminService {

	Admin getLoginInfo(String account);

	List<AuthConfig> getManagerauth(Map<String, Object> paramMap);

	List<Admin> getPaginate(Map<String, Object> paranmMap);

	int getTotal(Map<String, Object> paranmMap);

	void save(Admin manager);

	void update(Admin manager);

	Admin get(Admin manager);

	void remove(String ids);

	void editPassWord(Admin manager);

	Admin isExistByName(Admin manager);

	List<AuthConfig> getSelectManagerauth(Map<String, Object> paramMap);

}
