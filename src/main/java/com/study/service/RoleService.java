package com.study.service;

import java.util.List;
import java.util.Map;

import com.study.model.AuthConfig;
import com.study.model.Role;

public interface RoleService {

	List<Role> getPaginate(Map<String,Object> paramMap);

	int getTotal(String searchValue);

	void save(Role role);

	Role get(String id);

	void update(Role account);

	void remove(String ids);

	List<Role> getAllRole();

	List<AuthConfig> getAuthconfigs();

	List<Role> getByRoleId(Long roleId);

	Role isExistByName(String roleName);

}
