package com.study.service;

import java.util.List;
import java.util.Map;

import com.study.model.Clean;

public interface CleanService {

	int getTotal(Map<String, Object> paramMap);

	List<Clean> getPaginate(Map<String, Object> paramMap);

	void save(Clean obj);

}
