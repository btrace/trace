package com.study.service;

import java.util.List;
import java.util.Map;

import com.study.model.GiveOut;

public interface GiveOutService {

	int getTotal(Map<String, Object> paramMap);

	List<GiveOut> getPaginate(Map<String, Object> paramMap);

	void save(GiveOut obj);

	List<Map<String,Object>> getTracePaginate(Map<String, Object> paramMap);

	int getTraceTotal(Map<String, Object> paramMap);

}
