package com.study.service;

import java.util.List;
import java.util.Map;

import com.study.model.Disinfect;

public interface DisinfectService {

	int getTotal(Map<String, Object> paramMap);

	List<Disinfect> getPaginate(Map<String, Object> paramMap);

	void save(Disinfect obj);

}
