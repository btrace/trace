package com.study.service;

import java.util.List;
import java.util.Map;

import com.study.model.Retrieve;

public interface RetrieveService {

	int getTotal(Map<String, Object> paramMap);

	List<Retrieve> getPaginate(Map<String, Object> paramMap);

	void save(Retrieve obj);

}
