package com.study.service;

import java.util.List;
import java.util.Map;

import com.study.model.Department;

public interface DepartmentService {

	int save(Department department);

	int update(Department department);

	void remove(String ids);
	
	Department get(String id);

	Department isExist(String departmentName);

	List<Department> getPaginate(Map<String, Object> paramMap);

	int getTotal(Map<String, Object> paramMap);

	List<Department> getAllDepartmentInfo();

}
