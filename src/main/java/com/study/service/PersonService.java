package com.study.service;

import java.util.List;
import java.util.Map;

import com.study.model.Person;

public interface PersonService {

	int save(Person doctor);

	int update(Person doctor);

	void remove(String ids);

	Person get(Map<String, Object>  param);

	List<Person> getPaginate(Map<String, Object> paramMap);

	int getTotal(Map<String, Object> paramMap);

	List<Person> getAll();

	Person isExist(String name);
}
