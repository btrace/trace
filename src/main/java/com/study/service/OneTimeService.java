package com.study.service;

import java.util.List;
import java.util.Map;

import com.study.model.OneTime;

public interface OneTimeService {

	int save(OneTime obj);

	int update(OneTime obj);

	void remove(String ids);

	OneTime get(Map<String, Object> paramMap);

	List<OneTime> getPaginate(Map<String, Object> paramMap);

	int getTotal(Map<String, Object> paramMap);

}
