package com.study.service;

import java.util.List;
import java.util.Map;

import com.study.model.PersonRole;

public interface PersonRoleService {

	int save(PersonRole department);

	int update(PersonRole department);

	void remove(String ids);
	
	PersonRole get(String id);

	PersonRole isExist(String departmentName);

	List<PersonRole> getPaginate(Map<String, Object> paramMap);

	int getTotal(Map<String, Object> paramMap);

	List<PersonRole> getAll();

}
