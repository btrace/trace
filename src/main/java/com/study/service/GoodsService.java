package com.study.service;

import com.study.model.Goods;

import java.util.List;
import java.util.Map;

public interface GoodsService {
	// in 入库操作begin
	int inSave(Goods obj);

	int inUpdate(Goods obj);

	Goods inGet(String id);

	List<Goods> getInPaginate(Map<String, Object> paramMap);

	int getInPaginateTotal(Map<String, Object> paramMap);

	void inRemove(String parameter);
	// in 入库操作end

	// out 出库操作begin
	List<Goods> getOutPaginate(Map<String, Object> paramMap);

	int getOutPaginateTotal(Map<String, Object> paramMap);

	int outSave(Goods goods);

	Goods outGet(String parameter);

	void outRemove(String parameter);

	int outUpdate(Goods goods);
	// out 出库操作end

	List<Goods> getStockPaginate(Map<String, Object> paramMap);

	int getStockPaginateTotal(Map<String, Object> paramMap);

	List<Goods> getStockPaginateForOut(Map<String, Object> paramMap);

	int getStockPaginateForOutTotal(Map<String, Object> paramMap);

	Goods stockGetForOut(String parameter);
}
