package com.study.service;

import com.study.model.GoodsType;
import com.study.model.Warehouse;

import java.util.List;
import java.util.Map;

public interface GoodsTypeService {

	int save(GoodsType goods);

	int update(GoodsType goods);

	void remove(String ids);

	GoodsType get(GoodsType obj);

	List<GoodsType> getPaginate(Map<String, Object> paramMap);

	int getTotal(Map<String, Object> paramMap);

	List<GoodsType> getAllGoodsType(GoodsType goods);

	void saveList(List<GoodsType> insertList);

	void saveWarehouseList(List<Warehouse> insertList);

	List<GoodsType> getAllSccssByPydm(GoodsType goods);

	List<Warehouse> getWarehouseByWzuuid(GoodsType goods);
}
