package com.study.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.study.dao.PackDao;
import com.study.model.Pack;
import com.study.service.PackService;

@Service
public class PackServiceImpl implements PackService {

	@Resource
	private PackDao packDao;

	@Override
	public int getTotal(Map<String, Object> paramMap) {
		return packDao.getTotal(paramMap);
	}

	@Override
	public List<Pack> getPaginate(Map<String, Object> paramMap) {
		return packDao.getPaginate(paramMap);
	}

	@Override
	public void save(Pack obj) {
		packDao.save(obj);
	}

}
