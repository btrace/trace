package com.study.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.study.dao.SpecialDao;
import com.study.model.Special;
import com.study.service.SpecialService;

@Service
public class SpecialServiceImpl implements SpecialService {

	@Autowired
	private SpecialDao specialDao;

	@Override
	public List<Special> getPaginate(Map<String, Object> paramMap) {
		return specialDao.getPaginate(paramMap);
	}

	@Override
	public int getTotal(Map<String, Object> paramMap) {
		return specialDao.getTotal(paramMap);
	}

	@Override
	public int save(Special obj) {
		return specialDao.save(obj);
	}

	@Override
	public int update(Special department) {
		return specialDao.update(department);
	}

	@Override
	public void remove(String ids) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("ids", ids.split(","));
		specialDao.remove(paramMap);
	}

	@Override
	public Special get(Map<String, Object> paramMap) {
		return specialDao.get(paramMap);
	}
}
