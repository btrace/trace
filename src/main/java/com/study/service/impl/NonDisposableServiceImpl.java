package com.study.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.study.dao.NonDisposableDao;
import com.study.model.NonDisposable;
import com.study.model.Special;
import com.study.service.NonDisposableService;

@Service
public class NonDisposableServiceImpl implements NonDisposableService {

	@Autowired
	private NonDisposableDao nonDisposableDao;

	@Override
	public List<NonDisposable> getPaginate(Map<String, Object> paramMap) {
		return nonDisposableDao.getPaginate(paramMap);
	}

	@Override
	public int getTotal(Map<String, Object> paramMap) {
		return nonDisposableDao.getTotal(paramMap);
	}

	@Override
	public int save(NonDisposable obj) {
		return nonDisposableDao.save(obj);
	}

	@Override
	public int update(NonDisposable obj) {
		return nonDisposableDao.update(obj);
	}

	@Override
	public void remove(String ids) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("ids", ids.split(","));
		nonDisposableDao.remove(paramMap);
	}

	@Override
	public NonDisposable get(Map<String, Object> paramMap) {
		return nonDisposableDao.get(paramMap);
	}

	@Override
	public List<NonDisposable> getAllApplyPaginate(Map<String, Object> paramMap) {
		return nonDisposableDao.getAllApplyPaginate(paramMap);
	}

	@Override
	public int getAllApplyTotal(Map<String, Object> paramMap) {
		return nonDisposableDao.getAllApplyTotal(paramMap).size();
	}
}
