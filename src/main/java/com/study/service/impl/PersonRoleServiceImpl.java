package com.study.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.study.dao.PersonRoleDao;
import com.study.model.PersonRole;
import com.study.service.PersonRoleService;

@Service
public class PersonRoleServiceImpl implements PersonRoleService {

	@Autowired
	private PersonRoleDao personRoleDao;

	@Override
	public List<PersonRole> getPaginate(Map<String, Object> paramMap) {
		return personRoleDao.getPaginate(paramMap);
	}

	@Override
	public int getTotal(Map<String, Object> paramMap) {
		return personRoleDao.getTotal(paramMap);
	}

	@Override
	public int save(PersonRole department) {
		return personRoleDao.save(department);
	}

	@Override
	public int update(PersonRole department) {
		return personRoleDao.update(department);
	}

	@Override
	public void remove(String ids) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("ids", ids.split(","));
		personRoleDao.remove(paramMap);
	}

	@Override
	public PersonRole isExist(String name) {
		return personRoleDao.isExist(name);
	}

	@Override
	public PersonRole get(String id) {
		return personRoleDao.get(id);
	}

	@Override
	public List<PersonRole> getAll() {
		return personRoleDao.getAll();
	}

}
