package com.study.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.study.dao.OneTimeDao;
import com.study.model.OneTime;
import com.study.service.OneTimeService;

@Service
public class OneTimeServiceImpl implements OneTimeService {

	@Autowired
	private OneTimeDao oneTimeDao;

	@Override
	public List<OneTime> getPaginate(Map<String, Object> paramMap) {
		return oneTimeDao.getPaginate(paramMap);
	}

	@Override
	public int getTotal(Map<String, Object> paramMap) {
		return oneTimeDao.getTotal(paramMap);
	}

	@Override
	public int save(OneTime department) {
		return oneTimeDao.save(department);
	}

	@Override
	public int update(OneTime department) {
		return oneTimeDao.update(department);
	}

	@Override
	public void remove(String ids) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("ids", ids.split(","));
		oneTimeDao.remove(paramMap);
	}

	@Override
	public OneTime get(Map<String, Object> paramMap) {
		return oneTimeDao.get(paramMap);
	}
}
