package com.study.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.study.dao.GiveOutDao;
import com.study.model.GiveOut;
import com.study.service.GiveOutService;

@Service
public class GiveOutServiceImpl implements GiveOutService {

	@Resource
	private GiveOutDao giveOutDao;

	@Override
	public int getTotal(Map<String, Object> paramMap) {
		return giveOutDao.getTotal(paramMap);
	}

	@Override
	public List<GiveOut> getPaginate(Map<String, Object> paramMap) {
		return giveOutDao.getPaginate(paramMap);
	}

	@Override
	public void save(GiveOut obj) {
		giveOutDao.save(obj);
	}

	@Override
	public List<Map<String,Object>> getTracePaginate(Map<String, Object> paramMap) {
		return giveOutDao.getTracePaginate(paramMap);
	}

	@Override
	public int getTraceTotal(Map<String, Object> paramMap) {
		return giveOutDao.getTraceTotal(paramMap);
	}

}
