package com.study.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.study.dao.RetrieveDao;
import com.study.model.Retrieve;
import com.study.service.RetrieveService;

@Service
public class RetrieveServiceImpl implements RetrieveService {

	@Resource
	private RetrieveDao retrieveDao;

	@Override
	public int getTotal(Map<String, Object> paramMap) {
		return retrieveDao.getTotal(paramMap);
	}

	@Override
	public List<Retrieve> getPaginate(Map<String, Object> paramMap) {
		return retrieveDao.getPaginate(paramMap);
	}

	@Override
	public void save(Retrieve obj) {
		retrieveDao.save(obj);
	}

}
