package com.study.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.study.dao.DisinfectDao;
import com.study.model.Disinfect;
import com.study.service.DisinfectService;

@Service
public class DisinfectServiceImpl implements DisinfectService {

	@Resource
	private DisinfectDao disinfectDao;

	@Override
	public int getTotal(Map<String, Object> paramMap) {
		return disinfectDao.getTotal(paramMap);
	}

	@Override
	public List<Disinfect> getPaginate(Map<String, Object> paramMap) {
		return disinfectDao.getPaginate(paramMap);
	}

	@Override
	public void save(Disinfect obj) {
		disinfectDao.save(obj);
	}

}
