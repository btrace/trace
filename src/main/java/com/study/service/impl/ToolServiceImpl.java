package com.study.service.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.study.model.GoodsClass;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.study.dao.ToolDao;
import com.study.model.Database;
import com.study.service.ToolService;

@Service
public class ToolServiceImpl implements ToolService {

	@Autowired
	private ToolDao toolDao;

	@Override
	public List<Database> getDatabaseLists(Map<String, Object> paramMap) {
		return toolDao.getDatabaseLists(paramMap);
	}

	@Override
	public int getTotalDatabaseLists(Map<String, Object> paramMap) {
		return toolDao.getTotalDatabaseLists(paramMap);
	}

	@Override
	public void save(Database obj) {
		toolDao.save(obj);
	}

	@Override
	public List<Database> getDatabaseByMonth(Map<String, Object> paramMap) {
		return toolDao.getDatabaseByMonth(paramMap);
	}

	@Override
	public void del(Map<String, Object> paramMap) {
		toolDao.del(paramMap);
		File file = new File(paramMap.get("path").toString());
		file.deleteOnExit();
		file.delete();
	}

	@Override
	public List<GoodsClass> getAllGoodsClass(Map<String, Object> paramMap) {
		return toolDao.getAllGoodsClass(paramMap);
	}

	@Override
	public Map<String, Object> getWpmcByTid(Map<String, Object> paramMap) {
		return toolDao.getWpmcByTid(paramMap);
	}

}