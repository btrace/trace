package com.study.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.study.dao.DepartmentDao;
import com.study.model.Department;
import com.study.service.DepartmentService;

@Service
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentDao departmentDao;

	@Override
	public List<Department> getPaginate(Map<String, Object> paramMap) {
		return departmentDao.getPaginate(paramMap);
	}

	@Override
	public int getTotal(Map<String, Object> paramMap) {
		return departmentDao.getTotal(paramMap);
	}

	@Override
	public int save(Department department) {
		return departmentDao.save(department);
	}

	@Override
	public int update(Department department) {
		return departmentDao.update(department);
	}

	@Override
	public void remove(String ids) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("ids", ids.split(","));
		departmentDao.remove(paramMap);
	}

	@Override
	public Department isExist(String departmentName) {
		return departmentDao.isExist(departmentName);
	}

	@Override
	public Department get(String id) {
		return departmentDao.get(id);
	}

	@Override
	public List<Department> getAllDepartmentInfo() {
		return departmentDao.getAllDepartmentInfo();
	}

}
