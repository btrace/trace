package com.study.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.study.dao.AdminDao;
import com.study.model.Admin;
import com.study.model.AuthConfig;
import com.study.service.AdminService;

@Service
public class AdminServiceImpl implements AdminService {

	@Resource
	private AdminDao managerDao;

	@Override
	public List<AuthConfig> getManagerauth(Map<String, Object> paramMap) {
		List<AuthConfig> authConfigs = managerDao.getManagerauth(paramMap);
//		for (AuthConfig auth : authConfigs) {
//			paramMap.put("id", auth.getId());
//			List<AuthConfig> childAuthConfigs = managerDao.getChildAuthById(paramMap);
//			if (childAuthConfigs.size() > 0) {
//				auth.setSubAuthConfig(childAuthConfigs);
//			}
//		}
		return authConfigs;
	}
	
	@Override
	public List<AuthConfig> getSelectManagerauth(Map<String, Object> paramMap) {
		List<AuthConfig> childAuthConfigs = managerDao.getChildAuthById(paramMap);
		return childAuthConfigs;
	}

	@Override
	public Admin getLoginInfo(String account) {
		Admin manager = new Admin();
		manager.setAccount(account);
		return managerDao.getLoginInfo(manager);
	}

	@Override
	public List<Admin> getPaginate(Map<String, Object> paranmMap) {
		return managerDao.getPaginate(paranmMap);
	}

	@Override
	public int getTotal(Map<String, Object> paranmMap) {
		return managerDao.getTotal(paranmMap);
	}

	@Override
	public void save(Admin manager) {
		managerDao.save(manager);
		String[] roles = manager.getRoleIds().split(",");
		for (int i = 0; i < roles.length; i++) {
			manager.setRoleIds(roles[i]);
			managerDao.saveManagerRole(manager);
		}
	}

	@Override
	public Admin get(Admin manager) {
		return managerDao.get(manager);
	}

	@Override
	public void update(Admin manager) {
		managerDao.update(manager);
		managerDao.removeManagerRole(manager.getId().toString());
		String[] roles = manager.getRoleIds().split(",");
		for (int i = 0; i < roles.length; i++) {
			manager.setRoleIds(roles[i]);
			managerDao.saveManagerRole(manager);
		}
	}

	@Override
	public void remove(String ids) {
		String[] idsArr = ids.split(",");
		for (int i = 0; i < idsArr.length; i++) {
			managerDao.remove(idsArr[i]);
			managerDao.removeManagerRole(idsArr[i]);
		}
	}
	 @Override
	 public void editPassWord(Admin manager) {
		 managerDao.editPassWord(manager);
	 }
	
	 @Override
	 public Admin isExistByName(Admin manager) {
		 return managerDao.isExistByName(manager);
	 }

}
