package com.study.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.study.dao.PersonDao;
import com.study.model.Person;
import com.study.service.PersonService;

@Service
public class PersonServiceImpl implements PersonService {

	@Autowired
	private PersonDao personDao;

	@Override
	public List<Person> getPaginate(Map<String, Object> paramMap) {
		return personDao.getPaginate(paramMap);
	}

	@Override
	public int getTotal(Map<String, Object> paramMap) {
		return personDao.getTotal(paramMap);
	}

	@Override
	public int save(Person doctor) {
		return personDao.save(doctor);
	}

	@Override
	public int update(Person doctor) {
		return personDao.update(doctor);
	}

	@Override
	public void remove(String ids) {
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("ids", ids.split(","));
		personDao.remove(param);
	}

	@Override
	public Person get(Map<String,Object> param) {
		return personDao.get(param);
	}

	@Override
	public Person isExist(String name) {
		return personDao.isExist(name);
	}

	@Override
	public List<Person> getAll() {
		return personDao.getAll();
	}
}
