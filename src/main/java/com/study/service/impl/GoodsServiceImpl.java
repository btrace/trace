package com.study.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.study.dao.GoodsDao;
import com.study.model.Goods;
import com.study.service.GoodsService;

@Service
public class GoodsServiceImpl implements GoodsService {

	@Autowired
	private GoodsDao goodsDao;

	// 入库 操作 begin
	@Override
	public List<Goods> getInPaginate(Map<String, Object> paramMap) {
		return goodsDao.getInPaginate(paramMap);
	}

	@Override
	public int getInPaginateTotal(Map<String, Object> paramMap) {
		return goodsDao.getInPaginateTotal(paramMap);
	}

	@Override
	public int inSave(Goods obj) {
		return goodsDao.inSave(obj);
	}

	@Override
	public int inUpdate(Goods obj) {
		return goodsDao.inUpdate(obj);
	}

	@Override
	public void inRemove(String ids) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("ids", ids.split(","));
		goodsDao.inRemove(paramMap);
	}

	@Override
	public Goods inGet(String id) {
		return goodsDao.inGet(id);
	}
	// 入库 操作 end
	
	// 出库 操作 begin
	@Override
	public List<Goods> getOutPaginate(Map<String, Object> paramMap) {
		return goodsDao.getOutPaginate(paramMap);
	}
	
	@Override
	public int getOutPaginateTotal(Map<String, Object> paramMap) {
		return goodsDao.getOutPaginateTotal(paramMap);
	}
	
	@Override
	public int outSave(Goods obj) {
		return goodsDao.outSave(obj);
	}
	
	@Override
	public int outUpdate(Goods obj) {
		return goodsDao.outUpdate(obj);
	}
	
	@Override
	public void outRemove(String ids) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("ids", ids.split(","));
		goodsDao.outRemove(paramMap);
	}
	
	@Override
	public Goods outGet(String id) {
		return goodsDao.outGet(id);
	}
	// 出库 操作 end

	// 库存查询 begin
	@Override
	public List<Goods> getStockPaginate(Map<String, Object> paramMap) {
		return goodsDao.getStockPaginate(paramMap);
	}

	@Override
	public int getStockPaginateTotal(Map<String, Object> paramMap) {
		return goodsDao.getStockPaginateTotal(paramMap);
	}
	// 库存查询 end

	@Override
	public List<Goods> getStockPaginateForOut(Map<String, Object> paramMap) {
		return goodsDao.getStockPaginateForOut(paramMap);
	}

	@Override
	public int getStockPaginateForOutTotal(Map<String, Object> paramMap) {
		return goodsDao.getStockPaginateForOutTotal(paramMap);
	}

	@Override
	public Goods stockGetForOut(String id) {
		return goodsDao.stockGetForOut(id);
	}

}
