package com.study.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.study.model.Warehouse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.study.dao.GoodsTypeDao;
import com.study.model.GoodsType;
import com.study.service.GoodsTypeService;

@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {

    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Override
    public List<GoodsType> getPaginate(Map<String, Object> paramMap) {
        return goodsTypeDao.getPaginate(paramMap);
    }

    @Override
    public int getTotal(Map<String, Object> paramMap) {
        return goodsTypeDao.getTotal(paramMap);
    }

    @Override
    public int save(GoodsType goods) {
        try {
            goodsTypeDao.save(goods);
        } catch (Exception e) {
            goodsTypeDao.update(goods);
        }
        return 0;
    }

    @Override
    public int update(GoodsType goods) {
        return goodsTypeDao.update(goods);
    }

    @Override
    public void remove(String ids) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("ids", paramMap);
        goodsTypeDao.remove(paramMap);
    }

    @Override
    public GoodsType get(GoodsType goods) {
        return goodsTypeDao.get(goods);
    }

    @Override
    public List<GoodsType> getAllGoodsType(GoodsType goods) {
        return goodsTypeDao.getAllGoodsType(goods);
    }

    @Override
    public void saveList(List<GoodsType> insertList) {
        for (GoodsType obj : insertList) {
            save(obj);
        }
    }

    @Override
    public void saveWarehouseList(List<Warehouse> insertList) {
        for (Warehouse obj : insertList) {
            saveWarehouse(obj);
        }
    }

    public void saveWarehouse(Warehouse obj) {
        goodsTypeDao.saveWarehouse(obj);
    }

	@Override
	public List<GoodsType> getAllSccssByPydm(GoodsType goods) {
		return  goodsTypeDao.getAllSccssByPydm(goods);
	}

	@Override
	public List<Warehouse> getWarehouseByWzuuid(GoodsType goods) {
		return  goodsTypeDao.getWarehouseByWzuuid(goods);
	}

}
