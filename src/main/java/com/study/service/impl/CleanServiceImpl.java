package com.study.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.study.dao.CleanDao;
import com.study.model.Clean;
import com.study.service.CleanService;

@Service
public class CleanServiceImpl implements CleanService {

	@Resource
	private CleanDao cleanDao;

	@Override
	public int getTotal(Map<String, Object> paramMap) {
		return cleanDao.getTotal(paramMap);
	}

	@Override
	public List<Clean> getPaginate(Map<String, Object> paramMap) {
		return cleanDao.getPaginate(paramMap);
	}

	@Override
	public void save(Clean obj) {
		cleanDao.save(obj);
	}

}
