package com.study.util;

import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * 初始化
 */
public class InitConfig extends PropertyPlaceholderConfigurer {

	private static final Properties pro = new Properties();

	protected void processProperties(ConfigurableListableBeanFactory beanFactory, java.util.Properties props)
			throws BeansException {
		super.processProperties(beanFactory, props);

		for (Object key : props.keySet()) {
			String keyStr = key.toString();
			String value = props.getProperty(keyStr);
			pro.put(keyStr, value);
		}
	}

	public static String getProperty(String key) {
		return pro.getProperty(key);
	}

}
