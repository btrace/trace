//package com.study.util;
//
//import java.io.UnsupportedEncodingException;
//import java.util.ArrayList;
//import java.util.List;
//
//import com.sun.jna.Library;
//import com.sun.jna.Native;
//
//public class PrintBarcodeUtil {
//
//	private static final String LOAD_LIBRARY = "TSCLIB";
//
//	private static final String print_model = "Gprinter GP-9025T";
//
//	/**
//	 * @class:TscLibDll
//	 * @descript:创建TSCLIB.dll编程接口 动态链接库TSCLIB.dll，支持大部分佳博打印机，和其他品牌的TSC系列的打印机
//	 */
//	public interface TscLibDll extends Library {
//		
//		TscLibDll INSTANCE = (TscLibDll) Native.loadLibrary("TSCLIB", TscLibDll.class);
//
//		int about();
//
//		int openport(String pirnterName);
//
//		int closeport();
//
//		int sendcommand(String printerCommand);
//
//		int setup(String width, String height, String speed, String density, String sensor, String vertical,
//				String offset);
//
//		int downloadpcx(String filename, String image_name);
//
//		int barcode(String x, String y, String type, String height, String readable, String rotation, String narrow,
//				String wide, String code);
//
//		int printerfont(String x, String y, String fonttype, String rotation, String xmul, String ymul, String text);
//
//		int clearbuffer();
//
//		int printlabel(String set, String copy);
//
//		int formfeed();
//
//		int nobackfeed();
//
//		int windowsfont(int x, int y, int fontheight, int rotation, int fontstyle, int fontunderline, String szFaceName,
//				String content);
//	}
//
//	public static void printBarcode(String message, String text) {
//		// 加载驱动
//		System.loadLibrary(LOAD_LIBRARY);
//		// 解决中文乱码
//		System.setProperty("jna.encoding", "GBK");
//		
//		// TscLibDll.INSTANCE.about();
//		TscLibDll.INSTANCE.openport(print_model);
//		// TscLibDll.INSTANCE.downloadpcx("C:\\UL.PCX", "UL.PCX");
//		// TscLibDll.INSTANCE.sendcommand("REM ***** This is a test by JAVA. *****");
//		TscLibDll.INSTANCE.setup("100", "60", "5", "8", "0", "0", "0");
//		TscLibDll.INSTANCE.clearbuffer();
//		TscLibDll.INSTANCE.printerfont("150", "270", "TSS24.BF2", "0", "1", "1", text);
//		TscLibDll.INSTANCE.barcode("150", "90", "128", "150", "1", "0", "2", "2", message);
//		// TscLibDll.INSTANCE.windowsfont(400, 200, 48, 0, 3, 1, "arial", "DEG 0");
//		TscLibDll.INSTANCE.printlabel("1", "1");
//		TscLibDll.INSTANCE.closeport();
//	}
//
//	// 测试
//	public static void main(String[] args) throws UnsupportedEncodingException {
//		List<String> list = new ArrayList<String>();
//		list.add("2008abcd1");
//		if (list != null && list.size() > 0) {
//			for (String message : list) {
//				printBarcode(message, "广州纽曼科技");
//			}
//		}
//		System.out.println("打印成功");
//	}
//}
