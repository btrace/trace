package com.study.util;

import java.util.ArrayList;
import java.util.List;

public class Page {
	private int pageNo;
	private int pageSize = 10;
	private List<Object> result = new ArrayList<Object>();
	private int totalPages = -1;

	public int getPages(int totals) {
		return Long.valueOf((totals + pageSize - 1) / pageSize).intValue();
	}

	// 构造函数
	public Page() {
		super();
	}

	public Page(int pageSize) {
		setPageSize(pageSize);
	}

	// 查询参数函数
	/**
	 * 获得当前页的页号,序号从1开始,默认为1.
	 */
	public int getPageNo() {
		return pageNo;
	}

	/**
	 * 设置当前页的页号,序号从1开始,低于1时自动调整为1.
	 */
	public void setPageNo(int pageNo) {
		if (StringUtil.isBlank(pageNo + "")) {
			this.pageNo = 1;
		}
		this.pageNo = pageNo;
	}

	/**
	 * 获得每页的记录数量,默认为10.
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * 设置每页的记录数量.
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 取得页内的记录列表.
	 */
	public List<Object> getResult() {
		return result;
	}

	public void setResult(List result) {
		this.result = result;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
}
