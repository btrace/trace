package com.study.model;

import javax.xml.bind.annotation.XmlRootElement;

public class ManagerAuth {
	private Long roleid;
	private String account;
	private String password;
	private String authnames;
	private String selectKeys;
	private String dynatrees;

	public Long getRoleid() {
		return roleid;
	}

	public void setRoleid(Long roleid) {
		this.roleid = roleid;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public String getAuthnames() {
		return authnames;
	}

	public void setAuthnames(String authnames) {
		this.authnames = authnames;
	}

	public String getSelectKeys() {
		return selectKeys;
	}

	public void setSelectKeys(String selectKeys) {
		this.selectKeys = selectKeys;
	}

	public String getDynatrees() {
		return dynatrees;
	}

	public void setDynatrees(String dynatrees) {
		this.dynatrees = dynatrees;
	}

}
