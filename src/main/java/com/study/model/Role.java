package com.study.model;


public class Role {
	private Long id;
	private String roleName;
	private String roleDes;

	private String roleid;
	private String authid;
	private String authnames;
	private String selectKeys;
	private String dynatrees;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRoleDes() {
		return roleDes;
	}
	public void setRoleDes(String roleDes) {
		this.roleDes = roleDes;
	}
	public String getRoleid() {
		return roleid;
	}
	public void setRoleid(String roleid) {
		this.roleid = roleid;
	}
	public String getAuthid() {
		return authid;
	}
	public void setAuthid(String authid) {
		this.authid = authid;
	}
	public String getAuthnames() {
		return authnames;
	}
	public void setAuthnames(String authnames) {
		this.authnames = authnames;
	}
	public String getSelectKeys() {
		return selectKeys;
	}
	public void setSelectKeys(String selectKeys) {
		this.selectKeys = selectKeys;
	}
	public String getDynatrees() {
		return dynatrees;
	}
	public void setDynatrees(String dynatrees) {
		this.dynatrees = dynatrees;
	}
}