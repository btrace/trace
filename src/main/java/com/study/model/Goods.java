package com.study.model;

import java.io.Serializable;

public class Goods implements Serializable {

	private Long id;
	private String inId;// 入库表ID
	private String wpglid;// 物品管理ID
	private String sccs;// 物品管理ID
	private String wzuuid;// 物资编码UUID（物品编号）
	private String ckid;// 仓库ID
	private String wzzt;// 物资状态 在库/出库/删除/退回/报损/残缺
	private String wpyxq;// 有效期/按月
	private String scph;// 生产批号
	private String scrq;// 生产日期
	private String wzsl;// 物资数量
	private String rksj;// 入库时间
	private String cksj;// 出库时间
	private String disinfectTime;// 操作员UUID
	private String czyuuid;// 操作员UUID
	private String createTime;
	private String updateTime;
	private String createor;
	private String updateor;

	private String pydm;
	private String wpmc;
	private String wpdw;
	private String warehouseName;
	private String wpgg;
	private String zxgg;
	private String mrdj;
	private String wpfl;
	private String whoApply;

	public String getWhoApply() {
		return whoApply;
	}

	public void setWhoApply(String whoApply) {
		this.whoApply = whoApply;
	}

	public String getInId() {
		return inId;
	}

	public void setInId(String inId) {
		this.inId = inId;
	}

	public String getWpfl() {
		return wpfl;
	}

	public void setWpfl(String wpfl) {
		this.wpfl = wpfl;
	}

	public String getWpdw() {
		return wpdw;
	}

	public void setWpdw(String wpdw) {
		this.wpdw = wpdw;
	}

	public String getPydm() {
		return pydm;
	}

	public void setPydm(String pydm) {
		this.pydm = pydm;
	}

	public String getWpmc() {
		return wpmc;
	}

	public void setWpmc(String wpmc) {
		this.wpmc = wpmc;
	}

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public String getWpgg() {
		return wpgg;
	}

	public void setWpgg(String wpgg) {
		this.wpgg = wpgg;
	}

	public String getZxgg() {
		return zxgg;
	}

	public void setZxgg(String zxgg) {
		this.zxgg = zxgg;
	}

	public String getMrdj() {
		return mrdj;
	}

	public void setMrdj(String mrdj) {
		this.mrdj = mrdj;
	}

	public String getSccs() {
		return sccs;
	}

	public void setSccs(String sccs) {
		this.sccs = sccs;
	}

	public String getDisinfectTime() {
		return disinfectTime;
	}

	public void setDisinfectTime(String disinfectTime) {
		this.disinfectTime = disinfectTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWpglid() {
		return wpglid;
	}

	public void setWpglid(String wpglid) {
		this.wpglid = wpglid;
	}

	public String getWzuuid() {
		return wzuuid;
	}

	public void setWzuuid(String wzuuid) {
		this.wzuuid = wzuuid;
	}

	public String getCkid() {
		return ckid;
	}

	public void setCkid(String ckid) {
		this.ckid = ckid;
	}

	public String getWzzt() {
		return wzzt;
	}

	public void setWzzt(String wzzt) {
		this.wzzt = wzzt;
	}

	public String getWpyxq() {
		return wpyxq;
	}

	public void setWpyxq(String wpyxq) {
		this.wpyxq = wpyxq;
	}

	public String getScph() {
		return scph;
	}

	public void setScph(String scph) {
		this.scph = scph;
	}

	public String getScrq() {
		return scrq;
	}

	public void setScrq(String scrq) {
		this.scrq = scrq;
	}

	public String getWzsl() {
		return wzsl;
	}

	public void setWzsl(String wzsl) {
		this.wzsl = wzsl;
	}

	public String getRksj() {
		return rksj;
	}

	public void setRksj(String rksj) {
		this.rksj = rksj;
	}

	public String getCksj() {
		return cksj;
	}

	public void setCksj(String cksj) {
		this.cksj = cksj;
	}

	public String getCzyuuid() {
		return czyuuid;
	}

	public void setCzyuuid(String czyuuid) {
		this.czyuuid = czyuuid;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreateor() {
		return createor;
	}

	public void setCreateor(String createor) {
		this.createor = createor;
	}

	public String getUpdateor() {
		return updateor;
	}

	public void setUpdateor(String updateor) {
		this.updateor = updateor;
	}
}