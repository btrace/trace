package com.study.model;

import java.io.Serializable;

public class OneTime implements Serializable {

	private Long id;
	private String pydm;
	private String wzuuid;
	private String applyPersonId;
	private String sccs;
	private String scph;
	private String wzsl;
	private String wpmc;
	private String wpgg;
	private String wpdw;
	private String zxgg;
	private String applyTime;
	private String tid;
	private String personName;
	private String departmentName;
	private String account;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getWpmc() {
		return wpmc;
	}

	public void setWpmc(String wpmc) {
		this.wpmc = wpmc;
	}

	public String getWpgg() {
		return wpgg;
	}

	public void setWpgg(String wpgg) {
		this.wpgg = wpgg;
	}

	public String getWpdw() {
		return wpdw;
	}

	public void setWpdw(String wpdw) {
		this.wpdw = wpdw;
	}

	public String getZxgg() {
		return zxgg;
	}

	public void setZxgg(String zxgg) {
		this.zxgg = zxgg;
	}

	public String getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(String applyTime) {
		this.applyTime = applyTime;
	}

	private String createTime;

	private String updateTime;

	private String createor;

	private String updateor;

	public String getWzuuid() {
		return wzuuid;
	}

	public void setWzuuid(String wzuuid) {
		this.wzuuid = wzuuid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPydm() {
		return pydm;
	}

	public void setPydm(String pydm) {
		this.pydm = pydm;
	}

	public String getApplyPersonId() {
		return applyPersonId;
	}

	public void setApplyPersonId(String applyPersonId) {
		this.applyPersonId = applyPersonId;
	}

	public String getSccs() {
		return sccs;
	}

	public void setSccs(String sccs) {
		this.sccs = sccs;
	}

	public String getScph() {
		return scph;
	}

	public void setScph(String scph) {
		this.scph = scph;
	}

	public String getWzsl() {
		return wzsl;
	}

	public void setWzsl(String wzsl) {
		this.wzsl = wzsl;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreateor() {
		return createor;
	}

	public void setCreateor(String createor) {
		this.createor = createor;
	}

	public String getUpdateor() {
		return updateor;
	}

	public void setUpdateor(String updateor) {
		this.updateor = updateor;
	}

}
