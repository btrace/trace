package com.study.model;

import java.io.Serializable;

public class Department implements Serializable {
	private Long id;

	private String departmentName;

	private String departmentDirector;

	private String telephoneNum;

	private String address;

	private String createTime;

	private String updateTime;

	private String createor;

	private String updateor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getDepartmentDirector() {
		return departmentDirector;
	}

	public void setDepartmentDirector(String departmentDirector) {
		this.departmentDirector = departmentDirector;
	}

	public String getTelephoneNum() {
		return telephoneNum;
	}

	public void setTelephoneNum(String telephoneNum) {
		this.telephoneNum = telephoneNum;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public String getCreateor() {
		return createor;
	}

	public void setCreateor(String createor) {
		this.createor = createor;
	}

	public String getUpdateor() {
		return updateor;
	}

	public void setUpdateor(String updateor) {
		this.updateor = updateor;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

}
