package com.study.model;

import java.io.Serializable;

public class Person implements Serializable {

	private Long id;

	private String personId;

	private String name;

	private String sex;

	private String telephoneNum;

	private String departmentId;
	private String departmentName;

	private String personRoleIds;
	private String personRoleNames;

	private String remark;

	private String createTime;

	private String updateTime;

	private String createor;

	private String updateor;

	public String getPersonRoleIds() {
		return personRoleIds;
	}

	public void setPersonRoleIds(String personRoleIds) {
		this.personRoleIds = personRoleIds;
	}

	public String getPersonRoleNames() {
		return personRoleNames;
	}

	public void setPersonRoleNames(String personRoleNames) {
		this.personRoleNames = personRoleNames;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getTelephoneNum() {
		return telephoneNum;
	}

	public void setTelephoneNum(String telephoneNum) {
		this.telephoneNum = telephoneNum;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreateor() {
		return createor;
	}

	public void setCreateor(String createor) {
		this.createor = createor;
	}

	public String getUpdateor() {
		return updateor;
	}

	public void setUpdateor(String updateor) {
		this.updateor = updateor;
	}

}
