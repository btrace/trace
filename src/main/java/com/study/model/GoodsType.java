package com.study.model;

import java.io.Serializable;

public class GoodsType implements Serializable {

	private Long id;
	private String wzuuid;
	private String pydm;
	private String wpmc;
	private String wpgg;
	private String wpdw;
	private String zxgg;
	private String sccs;
	private String mrdj;
	private String wpfl;
	private String createTime;
	private String updateTime;
	private String createor;
	private String updateor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWzuuid() {
		return wzuuid;
	}

	public void setWzuuid(String wzuuid) {
		this.wzuuid = wzuuid;
	}

	public String getPydm() {
		return pydm;
	}

	public void setPydm(String pydm) {
		this.pydm = pydm;
	}

	public String getWpmc() {
		return wpmc;
	}

	public void setWpmc(String wpmc) {
		this.wpmc = wpmc;
	}

	public String getWpgg() {
		return wpgg;
	}

	public void setWpgg(String wpgg) {
		this.wpgg = wpgg;
	}

	public String getWpdw() {
		return wpdw;
	}

	public void setWpdw(String wpdw) {
		this.wpdw = wpdw;
	}

	public String getZxgg() {
		return zxgg;
	}

	public void setZxgg(String zxgg) {
		this.zxgg = zxgg;
	}

	public String getSccs() {
		return sccs;
	}

	public void setSccs(String sccs) {
		this.sccs = sccs;
	}

	public String getMrdj() {
		return mrdj;
	}

	public void setMrdj(String mrdj) {
		this.mrdj = mrdj;
	}

	public String getWpfl() {
		return wpfl;
	}

	public void setWpfl(String wpfl) {
		this.wpfl = wpfl;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreateor() {
		return createor;
	}

	public void setCreateor(String createor) {
		this.createor = createor;
	}

	public String getUpdateor() {
		return updateor;
	}

	public void setUpdateor(String updateor) {
		this.updateor = updateor;
	}
}