package com.study.model;

import java.io.Serializable;

public class Warehouse implements Serializable {

    private Long id;
    private String wzuuids;
    private String warehouseName;
    private String createTime;
    private String updateTime;
    private String createor;
    private String updateor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWzuuids() {
        return wzuuids;
    }

    public void setWzuuids(String wzuuids) {
        this.wzuuids = wzuuids;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreateor() {
        return createor;
    }

    public void setCreateor(String createor) {
        this.createor = createor;
    }

    public String getUpdateor() {
        return updateor;
    }

    public void setUpdateor(String updateor) {
        this.updateor = updateor;
    }
}