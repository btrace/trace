package com.study.model;

import java.util.ArrayList;
import java.util.List;

public class AuthConfig implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 主键.
	 */
	private Long id;
	/**
	 * 名称.
	 */
	private String name;
	/**
	 * 设定url.
	 */
	private String url;
	/**
	 * 父ID，根的父ID为0..
	 */
	private Long pid;

	private List<AuthConfig> subAuthConfig = new ArrayList<AuthConfig>();

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getPid() {
		return this.pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public List<AuthConfig> getSubAuthConfig() {
		return subAuthConfig;
	}

	public void setSubAuthConfig(List<AuthConfig> subAuthConfig) {
		this.subAuthConfig = subAuthConfig;
	}

}