package com.study.dao;

import java.util.List;
import java.util.Map;

import com.study.model.AuthConfig;
import com.study.model.Role;

public interface RoleDao {

	public List<Role> getPaginate(Map<String,Object> paramMap);

	public List<Role> getTotal(String searchValue);

	public void save(Role role);

	public void update(Role role);

	public Role get(String id);

	public void saveRoleAuth(Role role);
	
	public List<AuthConfig> getAuthconfigs();
	
	public void remove(String sql);

	public List<Role> getAllRole();

	public String getIdByRoleName(String roleName);

	public void removeRoleAuth(Role role);

	public List<Role> getByRoleId(Long roleId);

	public Role isExistByName(String roleName);

}
