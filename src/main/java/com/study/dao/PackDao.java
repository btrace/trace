package com.study.dao;

import java.util.List;
import java.util.Map;

import com.study.model.Pack;

public interface PackDao {

	int getTotal(Map<String, Object> paramMap);

	List<Pack> getPaginate(Map<String, Object> paramMap);

	void save(Pack obj);

}
