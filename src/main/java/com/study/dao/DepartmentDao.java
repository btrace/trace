package com.study.dao;

import java.util.List;
import java.util.Map;

import com.study.model.Department;

public interface DepartmentDao {

    public List<Department> getPaginate(Map<String, Object> paramMap);

    public int getTotal(Map<String, Object> paramMap);

    public int save(Department obj);

    public int update(Department obj);

    public void remove(Map<String, Object> paramMap);

    public Department isExist(String departmentName);

    public Department get(String id);

    public List<Department> getAllDepartmentInfo();

}
