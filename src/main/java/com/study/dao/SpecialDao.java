package com.study.dao;

import java.util.List;
import java.util.Map;

import com.study.model.Special;

public interface SpecialDao {

    public List<Special> getPaginate(Map<String, Object> paramMap);

    public int getTotal(Map<String, Object> paramMap);

    public int save(Special obj);

    public int update(Special obj);

    public void remove(Map<String, Object> paramMap);

    public Special get(Map<String, Object> paramMap);

}
