package com.study.dao;

import java.util.List;
import java.util.Map;

import com.study.model.Admin;
import com.study.model.AuthConfig;

public interface AdminDao {

	List<AuthConfig> getManagerauth(Map<String, Object> paramMap);

	List<AuthConfig> getChildAuthById(Map<String, Object> paramMap);

	Admin getLoginInfo(Admin manager);

	List<Admin> getPaginate(Map<String, Object> paranmMap);

	int getTotal(Map<String, Object> paranmMap);

	void save(Admin manager);

	void update(Admin manager);

	Admin get(Admin manager);

	void saveManagerRole(Admin manager);

	void remove(String id);

	void removeManagerRole(String id);

	void editPassWord(Admin manager);

	Admin isExistByName(Admin manager);

}