package com.study.dao;

import java.util.List;
import java.util.Map;

import com.study.model.OneTime;

public interface OneTimeDao {

    public List<OneTime> getPaginate(Map<String, Object> paramMap);

    public int getTotal(Map<String, Object> paramMap);

    public int save(OneTime obj);

    public int update(OneTime obj);

    public void remove(Map<String, Object> paramMap);

    public OneTime get(Map<String, Object> paramMap);

}
