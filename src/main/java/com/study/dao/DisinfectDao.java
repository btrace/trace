package com.study.dao;

import java.util.List;
import java.util.Map;

import com.study.model.Disinfect;

public interface DisinfectDao {

	int getTotal(Map<String, Object> paramMap);

	List<Disinfect> getPaginate(Map<String, Object> paramMap);

	void save(Disinfect obj);

}
