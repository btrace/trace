package com.study.dao;

import java.util.List;
import java.util.Map;

import com.study.model.Retrieve;

public interface RetrieveDao {

	int getTotal(Map<String, Object> paramMap);

	List<Retrieve> getPaginate(Map<String, Object> paramMap);

	void save(Retrieve obj);

}
