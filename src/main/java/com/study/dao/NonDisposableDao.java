package com.study.dao;

import java.util.List;
import java.util.Map;

import com.study.model.NonDisposable;

public interface NonDisposableDao {

    public List<NonDisposable> getPaginate(Map<String, Object> paramMap);

    public int getTotal(Map<String, Object> paramMap);

    public int save(NonDisposable obj);

    public int update(NonDisposable obj);

    public void remove(Map<String, Object> paramMap);

    public NonDisposable get(Map<String, Object> paramMap);

	public List<NonDisposable> getAllApplyPaginate(Map<String, Object> paramMap);

	public List<NonDisposable> getAllApplyTotal(Map<String, Object> paramMap);

}
