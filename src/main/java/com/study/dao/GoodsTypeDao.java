package com.study.dao;

import com.study.model.GoodsType;
import com.study.model.Warehouse;

import java.util.List;
import java.util.Map;

public interface GoodsTypeDao {

    List<GoodsType> getPaginate(Map<String, Object> paramMap);

    int getTotal(Map<String, Object> paramMap);

    int save(GoodsType obj);

    int update(GoodsType obj);

    void remove(Map<String, Object> ids);

    GoodsType get(GoodsType goods);

    List<GoodsType> getAllGoodsType(GoodsType goods);

    void saveWarehouse(Warehouse obj);

	List<GoodsType> getAllSccssByPydm(GoodsType goods);

	List<Warehouse> getWarehouseByWzuuid(GoodsType goods);
}
