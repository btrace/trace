package com.study.dao;

import java.util.List;
import java.util.Map;

import com.study.model.Database;
import com.study.model.GoodsClass;

public interface ToolDao {
	
	List<Database> getDatabaseLists(Map<String, Object> paramMap);

	int getTotalDatabaseLists(Map<String, Object> paramMap);

	void save(Database obj);

	List<Database> getDatabaseByMonth(Map<String, Object> paramMap);

	void del(Map<String, Object> paramMap);

    List<GoodsClass> getAllGoodsClass(Map<String,Object> paramMap);

	Map<String, Object> getWpmcByTid(Map<String, Object> paramMap);
}
