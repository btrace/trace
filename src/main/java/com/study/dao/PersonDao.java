package com.study.dao;

import java.util.List;
import java.util.Map;

import com.study.model.Person;
public interface PersonDao {
	
	public List<Person> getPaginate(Map<String, Object> paramMap);

	public int getTotal(Map<String, Object> paramMap);
	
	public int save(Person obj);

	public int update(Person obj);

	public void remove(Map<String, Object> param);

	public Person get(Map<String, Object> param);

	public List<Person> getAll();

	public Person isExist(String name);
}
