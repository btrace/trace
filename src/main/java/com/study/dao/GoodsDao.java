package com.study.dao;

import com.study.model.Goods;

import java.util.List;
import java.util.Map;

public interface GoodsDao {
	// 入库操作 begin
	List<Goods> getInPaginate(Map<String, Object> paramMap);

	int getInPaginateTotal(Map<String, Object> paramMap);

	int inSave(Goods obj);

	int inUpdate(Goods obj);

	void inRemove(Map<String, Object> paramMap);

	Goods inGet(String id);
	// 入库操作 end

	// out 出库操作begin
	List<Goods> getOutPaginate(Map<String, Object> paramMap);

	int getOutPaginateTotal(Map<String, Object> paramMap);

	int outSave(Goods goods);

	Goods outGet(String parameter);

	void outRemove(Map<String, Object> paramMap);

	int outUpdate(Goods goods);
	// out 出库操作end
	
	//库存 查询 begin
	List<Goods> getStockPaginate(Map<String, Object> paramMap);

	int getStockPaginateTotal(Map<String, Object> paramMap);
	//库存 查询 end

	List<Goods> getStockPaginateForOut(Map<String, Object> paramMap);

	int getStockPaginateForOutTotal(Map<String, Object> paramMap);

	Goods stockGetForOut(String id);
}
