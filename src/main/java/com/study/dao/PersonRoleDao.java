package com.study.dao;

import java.util.List;
import java.util.Map;

import com.study.model.PersonRole;

public interface PersonRoleDao {

    public List<PersonRole> getPaginate(Map<String, Object> paramMap);

    public int getTotal(Map<String, Object> paramMap);

    public int save(PersonRole obj);

    public int update(PersonRole obj);

    public void remove(Map<String, Object> paramMap);

    public PersonRole isExist(String departmentName);

    public PersonRole get(String id);

    public List<PersonRole> getAll();

}
