<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
<title>供应室追溯管理系统</title>
<head>
<head>
<%@include file="/WEB-INF/public/common_header.jstf"%>
</head>
<link href="/static/css/style.css" rel="stylesheet" type="text/css"
	id="skinSheet">
<!-- <script src="/static/js/jquery-3.3.1.min.js"></script> -->
<script src="/static/js/cloud.js"></script>
<!-- <script src="/static/layer/layer.js"></script> -->
</head>
<script type="text/javascript">
	$(function() {
		$("#account").focus();

		$('.loginbox').css({
			'position' : 'absolute',
			'left' : ($(window).width() - 692) / 2
		});
		$(window).resize(function() {
			$('.loginbox').css({
				'position' : 'absolute',
				'left' : ($(window).width() - 692) / 2
			});
		})
	});

	function login() {
		if ($("#account").val() == '') {
			layer.msg("用户名不能为空", {
				offset : [ '50%', '50%' ],
				shift : 6
			});
			return;
		}

		if ($("#password").val() == '') {
			layer.msg("密码不能为空", {
				offset : [ '50%', '50%' ],
				shift : 6
			});
			return;
		}
		
		if ($("#checkCode").val() == '') {
			layer.msg("验证码不能为空", {
				offset : [ '50%', '50%' ],
				shift : 6
			});
			return;
		}
		var obj = new Object();
		obj.account = $("#account").val();
		obj.password = $("#password").val();
		obj.checkCode = $("#checkCode").val();
		$.ajax({
			url : "/admin/getLoginInfo",
			type : 'post',
			dataType : "json",
			data : obj,
			success : function(data) {
				if (data.code != "0") {
					layer.msg(data.msg, {
						offset : [ '50%', '50%' ],
						shift : 6
					});
					return;
				} else {
					window.location.href = "/login";
				}
			}
		});
	}

	function checkCodeAgain() {
		$("#checkCodeId").attr("src",
				"/checkCode?" + Math.floor(Math.random() * 100));
	}
</script>

<body
	style="background-color: #1c77ac; background-image: url(/static/images/webimage/light.png); background-repeat: no-repeat; background-position: center top; overflow: hidden;">
	<div id="mainBody">
		<div id="cloud1" class="cloud"></div>
		<div id="cloud2" class="cloud"></div>
	</div>
	<div class="logintop">
		<span style="font-size: 25px;">供应室追溯管理系统</span>
	</div>

	<div class="loginbody">
		<span class="systemlogo"></span>
		<div class="loginbox">
			<ul>
				<li><input id="account" type="text" class="loginuser"
					placeholder="请输入用户名" value="" /></li>
				<li><input id="password" type="password" class="loginpwd"
					placeholder="请输入密码" value="" /></li>
				<li><input id="checkCode" type="text" class="code_input"
					placeholder="请输入验证码"><img
					style="width: 80px; height: 35px;margin-left: 50px;margin-bottom: 5px;" id="checkCodeId"
					name="checkCodeId" src="/checkCode" onclick="checkCodeAgain();">
				</li>
				<li><input type="button" class="loginbtn" onclick="login();"
					value="登录" />
			</ul>
		</div>
	</div>
	<div class="loginbm">版权所有 2018,纽腾医疗</div>
</body>
</html>
</html>