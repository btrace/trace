<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
          content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"></meta>
    <meta name="renderer" content="webkit|ie-comp|ie-stand"></meta>
    <meta name="description" content=""></meta>
    <meta name="author" content=""></meta>
    <title>修改密码</title>
    <script type="text/javascript">
        function onback() {
            var url = "/admin/list?t=" + new Date().getTime();
            $.get(url, function (data) {
                $('.contentpanel').html(data);
            });
        }

        function onsave() {
            if ($("#password").val() == '') {
                layer.msg("密码必填!", {offset: ['50%', '50%'], shift: 6});
                return false;
            }
            if ($("#confirmpassword").val() == '') {
                layer.msg("确认密码必填!", {offset: ['50%', '50%'], shift: 6});
                return false;
            }
            if ($("#confirmpassword").val() != $("#password").val()) {
                layer.msg("两次密码输入必须一致!", {offset: ['50%', '50%'], shift: 6});
                return false;
            }
            var obj = new Object();
            obj.account = $("#account").val();
            obj.password = $("#password").val();
            $.ajax({
                url: '/admin/editPassWord?' + Math.floor(Math.random() * 100),
                type: "POST",
                data: obj,
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.code == "0") {
                        layer.msg("保存成功!", {offset: ['50%', '50%'], shift: 6});
                        if ($("#account").val() == 'admin') {
                            window.location.href = "/login.jsp";
                        } else {
                            onback();
                        }
                    }
                }
            });
        }
    </script>
</head>
<body>
<div class="row">
    <div class="col-md-12">
        <ul class="page-breadcrumb breadcrumb">
            <li><i class="fa fa-home"></i> <a href="javascript:;">首页 </a> <i
                    class="fa fa-angle-right"></i></li>
            <li><a href="javascript:;">系统管理 </a> <i
                    class="fa fa-angle-right"></i></li>
            <li><a href="javascript:;">账号信息</a></li>
        </ul>
    </div>
</div>
<div class="panel panel-default panel-list-border">
    <div class="panel-heading">
        <h4 class="modal-title">重置登陆密码</h4>
    </div>
</div>
<div class="panel-body">
    <div class="form-group">
        <label class="col-sm-2 control-label">登陆账号<span
                class="star-red">*</span></label>
        <div class="col-sm-5">
            <input type="text" class="form-control" readonly="readonly" id="account" name="account" value="${account }"
                   maxlength="16">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">密码<span
                class="star-red">*</span></label>
        <div class="col-sm-5">
            <input type="password" class="form-control" id="password" name="password"
                   placeholder="请输入登陆密码,最大长度是16" maxlength="16">
        </div>
        <div class="col-sm-5">
            <span class="help-block-text">密码与确认密码必须一致！</span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">确认密码<span
                class="star-red">*</span></label>
        <div class="col-sm-5">
            <input type="password" class="form-control" id="confirmpassword" name="confirmpassword"
                   placeholder="请输入确认密码,最大长度是16" maxlength="16">
        </div>
    </div>
    <div class="box-footer">
        <button type="button" class="btn btn-primary" id="save"
                onclick="onsave();">提交
        </button>
        <button type="button" class="btn btn-primary" id="back"
                onclick="onback();">返回
        </button>
    </div>
</div>
</body>
</html>