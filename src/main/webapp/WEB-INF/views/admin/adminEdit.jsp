<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
          content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"></meta>
    <meta name="renderer" content="webkit|ie-comp|ie-stand"></meta>
    <meta name="description" content=""></meta>
    <meta name="author" content=""></meta>
    <title>编辑账号</title>
    <script type="text/javascript">
        $(document).ready(function () {
            var obj = new Object();
            obj.id = $("#id").val();
            $.ajax({
                url: "/admin/get?t=" + new Date().getTime(),
                cache: false,
                type: 'post',
                data: obj,
                dataType: "json",
                success: function (data) {
                    if (data.code == '0') {
                        $("#account").val(data.data.account);
                        $("#remark").val(data.data.remark);
                        var roleIds = data.data.roleIds;

                        var departmentId = data.data.departmentId;
                        $.ajax({
                            url: "/department/getAllDepartmentInfo?" + "&t=" + new Date().getTime(),
                            type: "post",
                            success: function (data) {
                                var options = '';
                                for (var i = 0; i < data.data.length; i++) {
                                    if (data.data[i].id == departmentId) {
                                        options += "<option value=\"" + data.data[i].id + "\" selected =\"selected\">" + data.data[i].departmentName + "</option>";
                                    } else {
                                        options += "<option value=\"" + data.data[i].id + "\">" + data.data[i].departmentName + "</option>";
                                    }
                                }
                                $("#departmentInfo").append(options);
                            }
                        });
                        $.ajax({
                            url: "/role/getAllRole?t=" + new Date().getTime(),
                            cache: false,
                            type: 'post',
                            dataType: "json",
                            success: function (data) {
                                var datatr = "";
                                var len = data.data.length;
                                if (len > 0) {
                                    for (var i = 0; i < len; i++) {
                                        var temp = data.data[i];
                                        var ids = roleIds.split(",");
                                        for (var j = 0; j < ids.length; j++) {
                                            if (ids[j] == temp.id) {
                                            	flag = true;
                                            }
                                        }
                                        if(flag){
                                            datatr += "<input type=\"checkbox\" name=\"roleId\" checked=\"checked\" value=\"" + temp.id + "\" />" + temp.roleName;
                                        } else {
                                            datatr += "<input type=\"checkbox\" name=\"roleId\" value=\"" + temp.id + "\" />" + temp.roleName;
                                        }
                                        
                                    }
                                }
                                $("#roleDivId").html(datatr);
                            }
                        });
                    }
                }
            });
        });

        function onback() {
            var url = "/admin/list?t=" + new Date().getTime();
            $.get(url, function (data) {
                $('.contentpanel').html(data);
            });
        }

        function onsave() {
            if ($("#account").val() == '') {
                layer.msg("登陆账号必填!", {offset: ['50%', '50%'], shift: 6});
                return false;
            }

            var chk_value = [];
            $('input[name="roleId"]:checked').each(function () {
                chk_value.push($(this).val());
            });
            if (chk_value.toString() == '') {
                layer.msg("登陆账号必须分配角色!", {offset: ['50%', '50%'], shift: 6});
                return false;
            }

            if (isExistByName()) {
                var obj = new Object();
                obj.id = $("#id").val();
                obj.account = $("#account").val();
                obj.roleIds = chk_value.toString();
                obj.remark = $("#remark").val();
                obj.departmentId = $("#departmentInfo").val();
                $.ajax({
                    url: '/admin/update?' + Math.floor(Math.random() * 100),
                    type: "POST",
                    data: obj,
                    dataType: "json",
                    async: false,
                    success: function (result) {
                        if (result.code == "0") {
                            layer.msg("更新成功!", {offset: ['50%', '50%'], shift: 6});
                            onback();
                        }
                    }
                });
            }
        }

        function isExistByName() {
            var flag = true;
            var obj = new Object();
            obj.id = $("#id").val();
            obj.account = $("#account").val();
            $.ajax({
                url: "/admin/isExistByName",
                type: 'post',
                data: obj,
                async: false,
                dataType: "json",
                success: function (data) {
                    if (data.code == '0') {
                        flag = false;
                        layer.msg("该登陆账号名称已经存在,请重新输入！！", {offset: ['50%', '50%'], shift: 6});
                    }
                }
            });
            return flag;
        }
    </script>
</head>
<body>
<input type="hidden" value="${id }" id="id" name="id">
<div class="row">
    <div class="col-md-12">
        <ul class="page-breadcrumb breadcrumb">
            <li><i class="fa fa-home"></i> <a href="javascript:;">首页 </a> <i
                    class="fa fa-angle-right"></i></li>
            <li><a href="javascript:;">系统管理 </a> <i
                    class="fa fa-angle-right"></i></li>
            <li><a href="javascript:;">账号信息</a></li>
        </ul>
    </div>
</div>
<div class="panel panel-default panel-list-border">
    <div class="panel-heading">
        <h4 class="modal-title">编辑账号</h4>
    </div>
</div>
<div class="panel-body">
    <div class="form-group">
        <label class="col-sm-2 control-label">登陆账号<span
                class="star-red">*</span></label>
        <div class="col-sm-5">
            <input type="text" class="form-control" id="account" name="account"
                   placeholder="请输入登陆账号,最大长度是16" maxlength="16">
        </div>
        <div class="col-sm-5">
            <span class="help-block-text">账号名称不能与其他账号名称重复，必须是唯一的！</span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">账号角色<span
                class="star-red">*</span></label>
        <div id="roleDivId" style="margin-left: 185px;"></div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">科室信息<span
                class="star-red">*</span></label>
        <div class="col-lg-2">
            <select id="departmentInfo" class="form-control">
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">备注</label>
        <div class="col-sm-5">
            <textarea class="form-control" rows="3" id="remark" name="remark"></textarea>
            <span class="help-block-text"></span>
        </div>
    </div>
    <div class="box-footer">
        <button type="button" class="btn btn-primary" id="save"
                onclick="onsave();">提交
        </button>
        <button type="button" class="btn btn-primary" id="back"
                onclick="onback();">返回
        </button>
    </div>
</div>
</body>
</html>