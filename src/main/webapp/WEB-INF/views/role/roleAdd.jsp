<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"></meta>
<meta name="renderer" content="webkit|ie-comp|ie-stand"></meta>
<title>角色信息</title>
<script type="text/javascript">
$(document).ready(function(){
	$.ajax({
	    url:  "/role/allauthconfig?t=" + new Date().getTime(),
    	contentType:"application/x-www-form-urlencoded",
        type:"POST",
        dataType:"json",
        cache:false,
     	 success:  function(data){
     		$("#mytree").dynatree({
				  checkbox: true,
				  selectMode: 3,
				  children: data,
	              onSelect: function(select, node) {
					var selKeys = $.map(node.tree.getSelectedNodes(), function(node){
						return node.data.key;
					});
					
					var selParentKeys = $.map(node.tree.getSelectedNodes(), function(node){
						return node.parent.data.key;
					});
					
					if(selKeys.length > 0){
						var keys = selKeys.join(",") + ",1," + selParentKeys.join(",");
						var strArr=keys.split(",");//把字符串分割成一个数组  
			            strArr.sort();//排序  
			            var result= new Array();//创建出一个结果数组  
			            var tempStr="";  
			            for(var i in strArr){  
			                 if(strArr[i] != tempStr){
			                     result.push(strArr[i]);  
			                     tempStr=strArr[i];  
			                 } else {  
			                     continue;
			                 }
			            }
						$("#selectKeys").val(result.join(","));
					} else {
						layer.msg("必须分配权限菜单!", {offset: ['50%', '50%'],shift: 6});
					}
				},
				onDblClick: function(node, event) {
					node.toggleSelect();
				},
				onKeydown: function(node, event) {
					if( event.which == 32 ) {
						node.toggleSelect();
						return false;
					}
				}, 
				cookieId: "dynatree-Cb3",
				idPrefix: "dynatree-Cb3-"
			});
    	}
	});
	
});

function onback(){
	var url = "/role/list?t=" + new Date().getTime();
	$.get(url, function(data) {
        $('.contentpanel').html(data);
    });
}
function onsave(){
	if($("#roleName").val()==''){
		layer.msg("角色名称必填!", {offset: ['50%', '50%'],shift: 6});
		return false;
	}
	if($("#roleDes").val()==''){
		layer.msg("角色描述必填!", {offset: ['50%', '50%'],shift: 6});
		return false;
	}
	if($("#selectKeys").val()==''){
		layer.msg("角色名必须分配菜单权限!", {offset: ['50%', '50%'],shift: 6});
		return false;
	}
	if(isExistByName()){
		var obj = new Object();
		obj.id = $("#id").val();
		obj.roleName = $("#roleName").val();
		obj.roleDes = $("#roleDes").val();
		obj.selectKeys = $("#selectKeys").val();
		$.ajax({
			url : '/role/save?' + Math.floor(Math.random() * 100),
			type : "POST",
			data : obj,
			dataType : "json",
			async: false,
			success : function(result) {
				if (result.code == "0") {
					layer.msg("保存成功!", {offset: ['50%', '50%'],shift: 6});
					onback();
				}
			},
		});
	}
}

function isExistByName(){
	var flag = true;
	var obj = new Object();
	obj.id = $("#id").val();
	obj.roleName = $("#roleName").val();
	$.ajax({
     	url:"/role/isExistByName",
    	type: 'post',
    	data : obj,
    	async: false,
    	dataType:"json",
        success:function(data) {
            if(data.code == '0') {
            	flag = false;
            	layer.msg("该角色名称已经存在,请重新输入！！", {offset: ['50%', '50%'],shift: 6});
            }
         }
	});
	return flag;
}
</script>
</head>
<body>
	<input type="hidden" value="${id }" id="id" name="id">
	<input type="hidden" id="selectKeys" name="selectKeys" value="" />
	<input type="hidden" id="oldselectKeys" name="oldselectKeys" value="" />
	<div class="row">
		<div class="col-md-12">
			<ul class="page-breadcrumb breadcrumb">
				<li><i class="fa fa-home"></i> <a href="javascript:;">首页 </a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">系统管理 </a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">角色信息</a></li>
			</ul>
		</div>
	</div>
	<div class="panel panel-default panel-list-border">
		<div class="panel-heading">
			<c:choose>
				<c:when test="${id =='-1'}">
					<h4 class="modal-title">添加角色</h4>
				</c:when>
			    <c:otherwise>
					<h4 class="modal-title">编辑角色</h4>
			    </c:otherwise>
		    </c:choose>
		</div>
	</div>
	<div class="panel-body">
		<div class="form-group">
			<label class="col-sm-2 control-label">角色名称<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入角色名称,最大长度是32" maxlength="32" id="roleName" name="roleName">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text">角色名称不能与其他角色名称重复，必须是唯一的！</span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">角色描述<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入角色描述,最大长度是64" maxlength="64" id="roleDes" name="roleDes">
			</div>
		</div>
		<div class="box-body">
			<div class="form-group">
				<div id="mytree" style="margin-left: 15px;"></div>
			</div>
		</div>
		<div class="box-footer">
			<button type="button" class="btn btn-primary" id="save" onclick="onsave();">提交</button>
			<button type="button" class="btn btn-primary" id="back" onclick="onback();">返回</button>
		</div>
	</div>
</body>
</html>