<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
          content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"></meta>
    <meta name="renderer" content="webkit|ie-comp|ie-stand"></meta>
    <meta name="description" content=""></meta>
    <meta name="author" content=""></meta>
    <title>管理后台</title>
    <head>
        <%@include file="/WEB-INF/public/common_header.jstf" %>
        <style type="text/css">
			.topleft1{height:88px;background:url(/static/images/webimage/topleft.jpg) no-repeat;float:left; width:300px;}
			.topleft1 img{margin-top:12px;margin-left:10px;}
			.topright1 ul{padding-top:15px; float:right; padding-right:12px;}
			.topright1 ul li{float:left; padding-left:9px; padding-right:9px; background:url(/static/images/webimage/line.gif) no-repeat right;}
			.topright1 ul li:last-child{background:none;}
			.topright1 ul li a{font-size:13px; color:#e9f2f7;}
			.topright1 ul li a:hover{color:#fff;}
			.topright1 ul li span{margin-top:2px;float:left;padding-right:3px;}
			.topright1{height:88px;background:url(/static/images/webimage/topright.jpg) no-repeat right;float:right;}
			.nav1 {float:left;}
			.nav1 li {float:left;width:87px;height:88px; text-align:center;}
			.nav1 li a{display:block;width:87px;height:88px;-moz-transition: none; transition: background-color 0.3s linear; -moz-transition: background-color 0.3s linear; -webkit-transition: background-color 0.3s linear; -o-transition: background-color 0.3s linear; }
			.nav1 li a.selected{background:url(/static/images/webimage/navbg.png) no-repeat;}
			.nav1 li a:hover{display:block;background:#000;color:#fff;background: none repeat scroll 0% 0% rgb(43, 127, 181);}
			.nav1 li img{margin-top:10px;}
			.nav1 li a{display:block;}
			.nav1 a h2{font-size:14px;color:#d6e8f1;margin-top:1px;}
			.nav1 a:hover h2{color:#fff;}
        </style>
    </head>
    <script type="text/javascript">
        $(function () {
            $(".mainwrapper a").each(function (i,e) {
            	if(i==1){
            		$(this).parent().addClass("active");
            		var url = this.href;
            		$.get(url, function(data) {
                    	$('.contentpanel').html(data);
					});
            	}
                if ($(this).attr("href") != '') {
                    $(this).click(function (e) {
                        $("a").each(function () {
                            $(this).parent().removeClass("active");
                        });
                        var url = this.href;
                        if (url != '') {
                            $(this).parent().addClass("active");
                            $.ajax({
                 				url : '/getSession?' + Math.floor(Math.random() * 100),
                 				type : "POST",
                 				dataType : "json",
                 				async : true,
                 				success : function(result) {
                 					if (result.code == "0") {
                 						window.location.href='/login.jsp';
                 					} else {
        			                 	$.get(url, function(data) {
        			                    	$('.contentpanel').html(data);
                 						});
                 					}
                 				}
                 			});
                        }
                        e.preventDefault();
                    });
                }
            });
        });

        function toeditPass() {
            var url = '/admin/toeditPass?t=' + new Date().getTime();
            $.get(url, function (data) {
                $('.contentpanel').html(data);
            });
        }

        function changeSecondMenu(id) {
            window.location.href = '/login?id=' + id + '&t=' + new Date().getTime();
        }

        function loginout() {
            window.location.href = '/loginout?t=' + new Date().getTime();
        }
    </script>
</head>
<body style="background:url(/static/images/webimage/topbg.gif) repeat-x;">
	<div class="topleft1">
    	<img src="/static/images/webimage/logo3.png"/>
    </div>
    <ul class="nav1">
    	<c:forEach var="authConfig" items="${firstAuthConfigs}"  varStatus="status">
    		<c:if test="${ status.index % 5 ==0}">
	    		<li><a href="/login?id=${authConfig.id}"
	    		<c:if test="${empty id }">class="selected"</c:if>
	    		<c:if test="${authConfig.id==id}">class="selected"</c:if>><img src="/static/images/webimage/icon01.png" /><h2>${authConfig.name}</h2></a></li>
	    	</c:if>
	    	<c:if test="${ status.index % 5 ==1}">
		    	<li><a href="/login?id=${authConfig.id}"<c:if test="${authConfig.id==id}">class="selected"</c:if>><img src="/static/images/webimage/icon09.png" /><h2>${authConfig.name}</h2></a></li>
		    </c:if>
		    <c:if test="${ status.index % 5 ==2}">
		    	<li><a href="/login?id=${authConfig.id}"<c:if test="${authConfig.id==id}">class="selected"</c:if>><img src="/static/images/webimage/icon08.png" /><h2>${authConfig.name}</h2></a></li>
		    </c:if>
		    <c:if test="${ status.index % 5 ==3}">
		    	<li><a href="/login?id=${authConfig.id}"<c:if test="${authConfig.id==id}">class="selected"</c:if>><img src="/static/images/webimage/icon07.png"/><h2>${authConfig.name}</h2></a></li>
		    </c:if>
		    <c:if test="${ status.index % 5 ==4}">
			    <li><a href="/login?id=${authConfig.id}"<c:if test="${authConfig.id==id}">class="selected"</c:if>><img src="/static/images/webimage/icon06.png"/><h2>${authConfig.name}</h2></a></li>
		    </c:if>
	    </c:forEach>
    </ul>
    <div class="topright1">    
	    <ul>
	    	<li><a href="#">${time}&nbsp;&nbsp;&nbsp;${week}</a></li>
	    	<li><a href="#">${account }欢迎登陆！&nbsp;&nbsp;&nbsp;管理员&nbsp;&nbsp;&nbsp;所属科室:${departmentName}</font></a></li>
		    <li><a href="#" onclick="loginout()">退出</a></li>
	    </ul>
    </div>
	<section>
	    <div class="mainwrapper">
	        <div class="leftpanel" style="border-right: 1px solid #e7e7e7;">
	            <div class="media profile-left">
	                <div class="media-body">
	                    <h4 class="media-heading"></h4>
	                    <small class="text-muted"><a onclick="toeditPass();" href="javascript:void(0)">修改密码</a></small>
	                </div>
	            </div>
	            <ul class="nav nav-stacked">
	                <c:forEach var="subAuthConfig" items="${selectAuthConfigs}">
	                    <li><a href="<%=request.getContextPath()%>${subAuthConfig.url}">${subAuthConfig.name}</a></li>
	                </c:forEach>
	            </ul>
	        </div>
	        <div class="mainpanel">
	            <div class="contentpanel">
	            </div>
	        </div>
	    </div>
	</section>
</body>
</html>