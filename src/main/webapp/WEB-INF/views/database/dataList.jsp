<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
          content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"></meta>
    <meta name="renderer" content="webkit|ie-comp|ie-stand"></meta>
    <meta name="description" content=""></meta>
    <meta name="author" content=""></meta>
    <title>数据库备份列表</title>
    <script type="text/javascript">
        var page = new Page();
        $(document).ready(function () {
            getPageData(1);
            $("ul.pagination a:not(.number)").each(function (index) {
                $(this).click(function () {
                    switch (index) {
                        case 0://首页
                            page.gotoFirst();
                            break;
                        case 1://上一页
                            page.gotoPre();
                            break;
                        case 2://下一页
                            page.gotoNext();
                            break;
                        case 3://尾页
                            page.gotoLast();
                            break;
                    }
                    getPageData(page.pageNo);
                });
            });
        });

        function getPageData(pageNo) {
            $("#datalist tbody tr").remove();
            var obj = new Object();
            obj.pageNo = pageNo;
            $.ajax({
                url: '/database/getLists',
                type: 'post',
                dataType: "json",
                data: obj,
                success: dataFill
            });
        }

        function dataFill(data) {
            var datatr = "";
            var resultObject = data;

            $("ul.pagination [id='number']").remove();
            page.pageNo = resultObject.pageNo;//从data中获取当前页码
            page.totalPage = resultObject.totalPages;//从data中获取总页数
            var pageRegion = page.getPageRegion();
            var pageLink = "";
            for (var i = pageRegion[0]; i <= pageRegion[1]; i++) {
                pageLink += " <li id='number' class='number "
                    + (i == page.pageNo ? 'active' : '') + "' title='" + i
                    + "'><a href='#'>" + i + "</a></li>";
            }
            $("ul.pagination").children().eq(1).after($(pageLink));
            $("ul.pagination [id='number']").click(function () {
                page.gotoIndex(parseInt($(this).attr("title")));
                getPageData(page.pageNo);
            });
            $(".pagination").show();
            //设置翻页相关信息结束

            $("#datalist tbody tr").remove();
            if (resultObject.totalPages > 0) {
                var countNum = (page.pageNo - 1) * page.pageSize;
                for (var i = 0; i < resultObject.result.length; i++) {
                    var data = resultObject.result[i];
                    datatr += "<tr id=\"" + data.id + "\">"
                    datatr += "<td>" + data.name + "</td>";
                    datatr += "<td>";
                    datatr += "<button class=\"btn btn-info btn-sm\" onclick=\"down('" + data.path + "')\"><i class=\"fa fa-plus\">下载</i></button>";
                    datatr += "</td>";
                    datatr += "<td>" + data.createTime + "</td>";
                    datatr += "<td>";
                    datatr += "<button class=\"btn btn-danger btn-sm\" onclick=\"del(" + data.id + ",'" + data.path + "')\"><i class=\"fa fa-trash-o\">删除</i></button>";
                    datatr += "</td>";
                    datatr += "</tr>";
                }
            } else {
                datatr = "<tr><td colspan=\"96\"  align='center'>很抱歉，没有找到相关的信息！</td><tr>";
            }
            $("#datalist tbody").append(datatr);
        }

        function down(path) {
            window.location.href = "/database/download?path=" + path;
        }

        function save() {
            $.ajax({
                url: '/database/save?' + Math.floor(Math.random() * 100),
                type: "POST",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.code == "0") {
                        layer.msg(result.msg, {offset: ['50%', '50%'], shift: 6});
                        getPageData(1);
                    }
                }
            });
        }

        function del(id, path) {
            if (confirm("确定删除吗？")) {
                var obj = new Object();
                obj.id = id;
                obj.path = path;
                $.ajax({
                    url: '/database/del?t=' + Math.floor(Math.random() * 100),
                    type: "POST",
                    data: obj,
                    dataType: "json",
                    async: false,
                    success: function (result) {
                        if (result.code == "0") {
                            layer.msg(result.msg, {offset: ['50%', '50%'], shift: 6});
                            getPageData(1);
                        }
                    }
                });
            }
        }

        function onback() {
            var url = "/database/list?t=" + new Date().getTime();
            $.get(url, function (data) {
                $('.contentpanel').html(data);
            });
        }
    </script>
</head>
<body>
<div class="row">
    <div class="col-md-12">
        <ul class="page-breadcrumb breadcrumb">
            <li><i class="fa fa-home"></i><a href="javascript:;">首页 </a> <i
                    class="fa fa-angle-right"></i></li>
            <li><a href="javascript:;">系统管理 </a> <i
                    class="fa fa-angle-right"></i></li>
            <li><a href="javascript:;">数据库备份</a></li>
        </ul>
    </div>
</div>
<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">
			   <span> <a class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="save();"
                         data-toggle="modal"><i class="fa fa-plus"></i>备份</a>
				</span>
        </h3>
    </div>
</div>
<div class="panel-body">
    <table id="datalist"
           class="table table-responsive table-bordered table-striped table-hover text-center-contd">
        <thead>
        <tr>
            <th>数据库备份名称</th>
            <th>数据库路径(点击可以下载)</th>
            <th>创建时间</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        </tr>
        </tbody>
    </table>
    <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-right">
            <li><a href="#" title="首页">&laquo; 首页</a></li>
            <li><a href="#" title="上一页">&laquo; 上一页</a></li>
            <li><a href="#" title="下一页">下一页 &raquo;</a></li>
            <li><a href="#" title="尾页">尾页 &raquo;</a></li>
        </ul>
    </div>
</div>
</body>
</html>