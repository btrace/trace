<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
          content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"></meta>
    <meta name="renderer" content="webkit|ie-comp|ie-stand"></meta>
    <meta name="description" content=""></meta>
    <meta name="author" content=""></meta>
    <title>excel导入数据</title>
    <script type="text/javascript">
        function addGoods() {
            var filePath = $("#addGoods").val();
            if (filePath == "") {
                layer.msg("请选择文件", {offset: ['50%', '50%'], shift: 6});
                return false;
            }
            var strExtension = filePath.substr(filePath.lastIndexOf('.') + 1);
            if (strExtension != 'xls' && strExtension != 'xlsx') {
                layer.msg("文件必须是excel文件", {offset: ['50%', '50%'], shift: 6});
                return false;
            }
            $("#addGoodsForm").ajaxSubmit({
                type: 'post',
                url: 'goodsType/addGoodsType',
                success: function (data) {
                    if (data.code == "0") {
                        layer.msg(data.msg, {offset: ['50%', '50%'], shift: 6});
                    } else {
                        var message = "";
                        for (var i = 0; i < data.data.length; i++) {
                            message = message + data.data[i] + "<br>";
                        }
                        $("#error").html(message);
                    }
                },
                error: function () {
                    layer.msg("上传失败，请检查网络后重试", {offset: ['50%', '50%'], shift: 6});
                }
            });
        }

        function addWarehouse() {
            var filePath = $("#addWarehouse").val();
            if (filePath == "") {
                layer.msg("请选择文件", {offset: ['50%', '50%'], shift: 6});
                return false;
            }
            var strExtension = filePath.substr(filePath.lastIndexOf('.') + 1);
            if (strExtension != 'xls' && strExtension != 'xlsx') {
                layer.msg("文件必须是excel文件", {offset: ['50%', '50%'], shift: 6});
                return false;
            }
            $("#addWarehouseForm").ajaxSubmit({
                type: 'post',
                url: 'goodsType/addWarehouse',
                success: function (data) {
                    if (data.code == "0") {
                        layer.msg(data.msg, {offset: ['50%', '50%'], shift: 6});
                    } else {
                        var message = "";
                        for (var i = 0; i < data.data.length; i++) {
                            message = message + data.data[i] + "<br>";
                        }
                        $("#error").html(message);
                    }
                },
                error: function () {
                    layer.msg("上传失败，请检查网络后重试", {offset: ['50%', '50%'], shift: 6});
                }
            });
        }

    </script>
</head>
<body>
<div class="row">
    <div class="col-md-12">
        <ul class="page-breadcrumb breadcrumb">
            <li><i class="fa fa-home"></i> <a href="javascript:;">首页 </a> <i
                    class="fa fa-angle-right"></i></li>
            <li><a href="javascript:;">系统管理 </a> <i
                    class="fa fa-angle-right"></i></li>
            <li><a href="javascript:;">数据导入</a></li>
        </ul>
    </div>
</div>
<div class="panel panel-default panel-list-border">
    <div class="panel-heading">
        <h4 class="modal-title">数据导入</h4>
    </div>
</div>
<div class="panel-body">
    <div id="error"></div>
    <table>
        <tr>
            <td>
                物资基本数据导入<form id="addGoodsForm" enctype="multipart/form-data" method="post">
                    <input type="file" name="addGoods" id="addGoods">
                </form>
            </td>
            <td>
                <button type="button" class="btn btn-primary" id="save"
                        onclick="addGoods();">提交
                </button>
            </td>
        </tr>
       <tr style="height: 20px;"></tr>
        <tr>
            <td>
                仓库基础数据导入<form id="addWarehouseForm" enctype="multipart/form-data" method="post">
                    <input type="file" name="addWarehouse" id="addWarehouse">
                </form>
            </td>
            <td>
                <button type="button" class="btn btn-primary" id="save"
                        onclick="addWarehouse();">提交
                </button>
            </td>
        </tr>
    </table>
</div>
</body>
</html>