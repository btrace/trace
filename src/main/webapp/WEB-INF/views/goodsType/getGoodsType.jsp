<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"></meta>
<meta name="renderer" content="webkit|ie-comp|ie-stand"></meta>
<title>物品类别</title>
<script type="text/javascript">
	$(document).ready(function(){
		var id = $("#id").val();
		if(id != '-1'){
			$.ajax({
	          url:"<%=request.getContextPath()%>/goodsType/get.do?id="+id+"&t=" + new Date().getTime(),
				type : "post",
				error : function() {
					ajaxAccessErrorProcess();
				},
				success : function(data) {
					var resultObject = data;
					$("#goodsId").val(resultObject.goodsId);
					$("#goodsName").val(resultObject.goodsName);
					$("input[name=goodsType][value="+resultObject.goodsType+"]").attr("checked",true);
					$("#manufacturer").val(resultObject.manufacturer);
					}
				});
		}
	});

	function onback(){
		var url = "<%=request.getContextPath()%>/goodsType/list.do?t=" + new Date().getTime();
		$.get(url, function(data) {
	        $('.contentpanel').html(data);
	    });
	}
	
	function onsave(){
		if($("#goodsId").val()==''){
			alert("物品ID必填!");
			return false;
		}
		if($("#goodsName").val()==''){
			alert("物品名称必填!");
			return false;
		}
        if($("#manufacturer").val()==''){
            alert("生产厂家必填!");
            return false;
        }
		if(isExistByGoodsId()){
			var obj = new Object();
			obj.id = $("#id").val();
			obj.goodsId = $("#goodsId").val();
			obj.goodsName = $("#goodsName").val();
			obj.goodsType = $("input[name='goodsType']:checked").val();
			obj.manufacturer = $("#manufacturer").val();
			obj.modifier = "<%=session.getAttribute("account")%>";
			$.ajax({
				url : '<%=request.getContextPath()%>/goodsType/saveorupdate.do?' + Math.floor(Math.random() * 100),
				type : "POST",
				data : obj,
				dataType : "json",
				async: false,
				success : function(result) {
					if (result.code == "0") {
						alert("保存成功!");
                        onback();
					}else{
						alert("保存失败!");
					}
				}
			});
		}
	}

	function isExistByGoodsId(){
		var flag = true;
		var obj = new Object();
		obj.id = $("#id").val();
		obj.goodsId = $("#goodsId").val();
		$.ajax({
	     	url:"<%=request.getContextPath()%>/goodsType/isExistByGoodsId.do",
	    	type: 'POST',
	    	data : obj,
	    	async: false,
	    	dataType:"json",
	        success:function(data) {
	            if(data.code == '0') {
	            	flag = false;
	            	alert("该物品类别ID已经存在,请重新输入！！")
	            }
	         }
		});
		return flag;
	}
</script>
</head>
<body>
	<input type="hidden" value="${id }" id="id" name="id">
	<div class="row">
		<div class="col-md-12">
			<ul class="page-breadcrumb breadcrumb">
				<li><i class="fa fa-home"></i> <a href="javascript:;">首页 </a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">物品管理</a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">一次性物品列表</a></li>
			</ul>
		</div>
	</div>
	<div class="panel panel-default panel-list-border">
		<div class="panel-heading">
			<c:choose>
				<c:when test="${id =='-1'}">
					<h4 class="modal-title">添加一次性物品信息</h4>
				</c:when>
			    <c:otherwise>
					<h4 class="modal-title">编辑一次性物品信息</h4>
			    </c:otherwise>
		    </c:choose>
		</div>
	</div>
	<div class="panel-body">
		<div class="form-group">
			<label class="col-sm-2 control-label">物品种类ID<span
					class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入物品ID" maxlength="11" id="goodsId" name="goodsId">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text">物品种类ID必须是唯一的！</span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">物品种类名称<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入物品名称,最大长度是32" maxlength="32" id="goodsName" name="goodsName">
			</div>
		</div>
		<div class="form-group">
            <label class="col-sm-2 control-label">物品种类类型<span class="star-red">*</span></label>
            <div class="col-sm-5">
                <input type="radio" id="goodsType" name="goodsType" value="0" checked="checked"/>一次性物品
                <input type="radio" id="goodsType" name="goodsType" value="1"/>非一次性物品
            </div>
            <div class="col-sm-5">
            </div>
        </div>
		<div class="form-group">
			<label class="col-sm-2 control-label">生产厂家<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入生产厂家" maxlength="32" id="manufacturer" name="manufacturer">
			</div>
		</div>
		</div>
		<div class="box-footer">
			<button type="button" class="btn btn-primary" id="save" onclick="onsave();">提交</button>
			<button type="button" class="btn btn-primary" id="back" onclick="onback();">返回</button>
		</div>
</body>
</html>