<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"></meta>
<meta name="renderer" content="webkit|ie-comp|ie-stand"></meta>
<meta name="description" content=""></meta>
<meta name="author" content=""></meta>
<title>一次性物品列表</title>
<script type="text/javascript">
var page = new Page();
$(document).ready(function(){
	getPageData(1);
	$("ul.pagination a:not(.number)").each(function(index){
	    $(this).click(function(){
	        switch(index){
	            case 0://首页
	                page.gotoFirst();
	                break;
	            case 1://上一页
	                page.gotoPre();
	                break;
	            case 2://下一页
	                page.gotoNext();
	                break;
	            case 3://尾页
	                page.gotoLast();
	                break;
	        }
	        getPageData(page.pageNo);
	    });
	});
});
   
function getPageData(pageNo){
     $("#datalist tbody tr").remove();
     var obj = new Object();
     obj.pageNo = pageNo;
     //obj.goodsType = "1";
     obj.searchValue=$("#searchValue").val();
     $.ajax({
       url: '<%=request.getContextPath()%>/goodsType/getPaginate.do',
       type: 'post',
       dataType:"json",
       data : obj,
       success : dataFill
	});
}
   
function dataFill(data) {
	var datatr = "";
	var resultObject = data;
	
	$("ul.pagination [id='number']").remove();
	page.pageNo = resultObject.pageNo;//从data中获取当前页码
	page.totalPage = resultObject.totalPages;//从data中获取总页数
	var pageRegion = page.getPageRegion();
	var pageLink = "";
	for (var i = pageRegion[0]; i <= pageRegion[1]; i++) {
		pageLink += " <li id='number' class='number "
				+ (i == page.pageNo ? 'active' : '') + "' title='" + i
				+ "'><a href='#'>" + i + "</a></li>";
	}
	$("ul.pagination").children().eq(1).after($(pageLink));
	$("ul.pagination [id='number']").click(function() {
		page.gotoIndex(parseInt($(this).attr("title")));
		getPageData(page.pageNo);
	});
	$(".pagination").show();
	//设置翻页相关信息结束
	
	$("#datalist tbody tr").remove();
	if (resultObject.totalPages > 0) {
		var countNum = (page.pageNo - 1) * page.pageSize;
		for (var i = 0; i < resultObject.result.length; i++) {
			var data = resultObject.result[i];
			datatr += "<tr id=\""+data.id+"\">"
			datatr += "<td><input type=\"checkbox\" value=\""+data.id+"\" name=\"selectList\" /></td>";
			var goodsType = data.goodsType=='0' ? '一次性物品':'非一次性物品';
			datatr += "<td>" + data.goodsId+ "</td>";
			datatr += "<td>" + data.goodsName + "</td>";
			datatr += "<td>" + goodsType + "</td>";
			datatr += "<td>" + data.manufacturer + "</td>";
			datatr += "<td>";
			datatr += "<button class='btn btn-info btn-sm' onclick='edit("+data.id+")'>编辑</i></button>";
			datatr += "<button class='btn btn-danger btn-sm' onclick='del("+data.id+")'>删除</i></button>";
			datatr += "</td>";
			datatr += "</tr>";
		}
	} else {
		datatr = "<tr><td colspan=\"96\"  align='center'>很抱歉，没有找到相关的信息！</td><tr>";
	}
	$("#datalist tbody").append(datatr);
}

function edit(id){
	 var url = '<%=request.getContextPath()%>/goodsType/toGetGoodsType.do?id='+id+'&t=' + new Date().getTime();
     $.get(url, function(data) {
     	 $('.contentpanel').html(data);
     });
}

function selectEvent(checked){
	if(checked){
		$("input[name='selectList']").attr("checked",true); 
	} else {
		$("input[name='selectList']").attr("checked",false); 
	}
}

function add(){
	var url = '<%=request.getContextPath()%>/goodsType/toGetGoodsType.do?id=-1&t=' + new Date().getTime();
    $.get(url, function(data) {
 	   $('.contentpanel').html(data);
    });
}

function delList(){
	var selectList =[];    
   	$('input[name="selectList"]:checked').each(function(){    
   		selectList.push($(this).val());    
   	});
   	if(selectList.length==0){
   		alert("请选择要删除的物品信息!");
   		return false;
   	}
   	del(selectList.toString());
}
function del(ids){
	if(confirm("确定删除吗？")){
		var obj = new Object();
		obj.ids = ids;
		$.ajax({
			url : '<%=request.getContextPath()%>/goodsType/removeList.do?' + Math.floor(Math.random() * 100),
			type : "POST",
			data : obj,
			dataType : "json",
			async: false,
			success : function(result) {
				if (result.code == "0") {
					alert("删除成功!");
					onback();
				}
			},
		});
	}
}

function onback(){
	var url = "<%=request.getContextPath()%>/goodsType/list.do?t=" + new Date().getTime();
	$.get(url, function(data) {
        $('.contentpanel').html(data);
    });
}
</script>
</head>
<body>
	<div class="row">
		<div class="col-md-12">
			<ul class="page-breadcrumb breadcrumb">
				<li><i class="fa fa-home"></i><a href="javascript:;">首页 </a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">物品管理 </a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">物品种类</a></li>
			</ul>
		</div>
	</div>
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">
			   <span> <a class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="add();"
					data-toggle="modal"><i class="fa fa-plus"></i>添加</a>
				</span>  
				<span> <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="delList();"
					data-toggle="modal"><i class="fa fa-trash-o"></i>删除</a>
				</span>

				<span style="float: right;"> <input type="text"
					id="searchValue" name="searchValue" placeholder="请输入物品名称/id">
					<button type="button" class="btn  btn-primary" onclick="getPageData(1);">
						<i class="fa fa-search"></i>查询
					</button>
				</span>
			</h3>
		</div>
	</div>
	<div class="panel-body">
		<table id="datalist"
			class="table table-responsive table-bordered table-striped table-hover text-center-contd">
			<thead>
				<tr>
				    <th><input type="checkbox" onclick="selectEvent(this.checked);"/></th>
					<th>物品种类ID</th>
					<th>物品种类名称</th>
					<th>物品种类类型</th>
					<th>生产厂家</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<tr>
				</tr>
			</tbody>
		</table>
		<div class="box-footer clearfix">
			<ul class="pagination pagination-sm no-margin pull-right">
				<li><a href="#" title="首页">&laquo; 首页</a></li>
				<li><a href="#" title="上一页">&laquo; 上一页</a></li>
				<li><a href="#" title="下一页">下一页 &raquo;</a></li>
				<li><a href="#" title="尾页">尾页 &raquo;</a></li>
			</ul>
		</div>
	</div>
</body>
</html>