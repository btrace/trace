<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"></meta>
<meta name="renderer" content="webkit|ie-comp|ie-stand"></meta>
<meta name="description" content=""></meta>
<meta name="author" content=""></meta>
<title>清洗登记</title>
<script type="text/javascript">
        var page = new Page();
        $(document).ready(function () {
            getPageData(1);
            $("ul.pagination a:not(.number)").each(function (index) {
                $(this).click(function () {
                    switch (index) {
                        case 0://首页
                            page.gotoFirst();
                            break;
                        case 1://上一页
                            page.gotoPre();
                            break;
                        case 2://下一页
                            page.gotoNext();
                            break;
                        case 3://尾页
                            page.gotoLast();
                            break;
                    }
                    getPageData(page.pageNo);
                });
            });
        });

        function getPageData(pageNo) {
            $("#datalist tbody tr").remove();
            var obj = new Object();
            obj.pageNo = pageNo;
            obj.pydm = $("#pydm").val();
            obj.beginCreateTime = $("#beginCreateTime").val();
            obj.endCreateTime = $("#endCreateTime").val();
            $.ajax({
                url: '/clean/getPaginate',
                type: 'post',
                dataType: "json",
                data: obj,
                success: dataFill
            });
        }

        function dataFill(data) {
            var datatr = "";
            var resultObject = data;

            $("ul.pagination [id='number']").remove();
            page.pageNo = resultObject.pageNo;//从data中获取当前页码
            page.totalPage = resultObject.totalPages;//从data中获取总页数
            var pageRegion = page.getPageRegion();
            var pageLink = "";
            for (var i = pageRegion[0]; i <= pageRegion[1]; i++) {
                pageLink += " <li id='number' class='number "
                    + (i == page.pageNo ? 'active' : '') + "' title='" + i
                    + "'><a href='#'>" + i + "</a></li>";
            }
            $("ul.pagination").children().eq(1).after($(pageLink));
            $("ul.pagination [id='number']").click(function () {
                page.gotoIndex(parseInt($(this).attr("title")));
                getPageData(page.pageNo);
            });
            $(".pagination").show();
            //设置翻页相关信息结束

            $("#datalist tbody tr").remove();
            if (resultObject.totalPages > 0) {
                var countNum = (page.pageNo - 1) * page.pageSize;
                for (var i = 0; i < resultObject.result.length; i++) {
                    var data = resultObject.result[i];
                    datatr += "<tr id=\"" + data.id + "\">"
                    datatr += "<td><input type=\"checkbox\" value=\"" + data.id + "\" name=\"selectList\" /></td>";
                    datatr += "<td>" + ++countNum + "</td>";
                    datatr += "<td>" + data.pydm + "</td>";
                    datatr += "<td>" + data.wzuuid + "</td>";
                    datatr += "<td>" + data.wpmc + "</td>";
                    datatr += "<td>" + data.tid + "</td>";
                    datatr += "<td>" + data.remark + "</td>";
                    datatr += "<td>" + data.createTime + "</td>";
                    datatr += "</tr>";
                }
            } else {
                datatr = "<tr><td colspan=\"96\"  align='center'>很抱歉，没有找到相关的信息！</td><tr>";
            }
            $("#datalist tbody").append(datatr);
        }

        function add() {
            var url = '/clean/toAdd?id=-1&t=' + new Date().getTime();
            $.get(url, function (data) {
                $('.contentpanel').html(data);
            });
        }

        function onback() {
            var url = "/clean/list?t=" + new Date().getTime();
            $.get(url, function (data) {
                $('.contentpanel').html(data);
            });
        }
        
        function selectEvent(checked){
        	if(checked){
        		$("input[name='selectList']").attr("checked",true); 
        	} else {
        		$("input[name='selectList']").attr("checked",false); 
        	}
        }
    </script>
</head>
<body>
	<div class="row">
		<div class="col-md-12">
			<ul class="page-breadcrumb breadcrumb">
				<li><i class="fa fa-home"></i><a href="javascript:;">首页 </a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">消毒灭菌 </a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">清洗登记</a></li>
			</ul>
		</div>
	</div>
	<div class="panel panel-info">
		<div class="panel-heading" style="height: 70px;">
			<h3 class="panel-title">
				<span><button class="btn btn-primary btn-sm" onclick="add();"
					data-toggle="modal"><i class="fa fa-plus"></i>添加</button>
				</span>
				<span style="float: right;"><input type="text" id="pydm"
					placeholder="请输入拼音代码">
					入库时间<input id="beginCreateTime"
					class="Wdate"
					onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" />~<input
					id="endCreateTime" class="Wdate"
					onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" />
					<button type="button" class="btn  btn-primary"
						onclick="getPageData(1);">
						<i class="fa fa-search"></i>查询
					</button>
				</span>
			</h3>
		</div>
	</div>
	<div class="panel-body">
		<table id="datalist"
			class="table table-responsive table-bordered table-striped table-hover text-center-contd">
			<thead>
				<tr>
					<th><input type="checkbox"
						onclick="selectEvent(this.checked);" /></th>
					<th>编码</th>
					<th>拼音代码</th>
					<th>物品编码</th>
					<th>物品名称</th>
					<th>追溯ID</th>
					<th>消毒灭菌描述</th>
					<th>入库时间</th>
				</tr>
			</thead>
			<tbody>
				<tr>
				</tr>
			</tbody>
		</table>
		<div class="box-footer clearfix">
			<ul class="pagination pagination-sm no-margin pull-right">
				<li><a href="#" title="首页">&laquo; 首页</a></li>
				<li><a href="#" title="上一页">&laquo; 上一页</a></li>
				<li><a href="#" title="下一页">下一页 &raquo;</a></li>
				<li><a href="#" title="尾页">尾页 &raquo;</a></li>
			</ul>
		</div>
	</div>
</body>
</html>