<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"></meta>
<meta name="renderer" content="webkit|ie-comp|ie-stand"></meta>
<meta name="description" content=""></meta>
<meta name="author" content=""></meta>
<title>一次性物品申请登记</title>
<script type="text/javascript">
var page = new Page();
$(document).ready(function(){
	getPageData(1);
	$("ul.pagination a:not(.number)").each(function(index){
	    $(this).click(function(){
	        switch(index){
	            case 0://首页
	                page.gotoFirst();
	                break;
	            case 1://上一页
	                page.gotoPre();
	                break;
	            case 2://下一页
	                page.gotoNext();
	                break;
	            case 3://尾页
	                page.gotoLast();
	                break;
	        }
	        getPageData(page.pageNo);
	    });
	});
});
   
function getPageData(pageNo){
     $("#datalist tbody tr").remove();
     var obj = new Object();
     obj.pageNo = pageNo;
     obj.pydm = $("#pydm").val();
     obj.beginCreateTime = $("#beginCreateTime").val();
     obj.endCreateTime = $("#endCreateTime").val();
     $.ajax({
       url: '/oneTime/getPaginate',
       type: 'post',
       dataType:"json",
       data : obj,
       error:function() {
           ajaxAccessErrorProcess();
       },
       success : dataFill
	});
}
   
function dataFill(data) {
	var datatr = "";
	var resultObject = data;
	
	$("ul.pagination [id='number']").remove();
	page.pageNo = resultObject.pageNo;//从data中获取当前页码
	page.totalPage = resultObject.totalPages;//从data中获取总页数
	var pageRegion = page.getPageRegion();
	var pageLink = "";
	for (var i = pageRegion[0]; i <= pageRegion[1]; i++) {
		pageLink += " <li id='number' class='number "
				+ (i == page.pageNo ? 'active' : '') + "' title='" + i
				+ "'><a href='#'>" + i + "</a></li>";
	}
	$("ul.pagination").children().eq(1).after($(pageLink));
	$("ul.pagination [id='number']").click(function() {
		page.gotoIndex(parseInt($(this).attr("title")));
		getPageData(page.pageNo);
	});
	$(".pagination").show();
	//设置翻页相关信息结束
	
	$("#datalist tbody tr").remove();
	if (resultObject.totalPages > 0) {
		var countNum = (page.pageNo - 1) * page.pageSize;
		for (var i = 0; i < resultObject.result.length; i++) {
			var data = resultObject.result[i];
			datatr += "<tr id=\""+data.id+"\">"
			datatr += "<td><input type=\"checkbox\" value=\""+data.id+"\" name=\"selectList\" /></td>";
			datatr += "<td>" + ++countNum + "</td>";
			datatr += "<td>" + data.pydm + "</td>";
			datatr += "<td>" + data.wzuuid + "</td>";
			datatr += "<td>" + data.wzuuid + "</td>";
			datatr += "<td>" + data.wpmc + "</td>";
			datatr += "<td>" + data.wpgg + "</td>";
			datatr += "<td>" + data.wpdw + "</td>";
			datatr += "<td>" + data.zxgg + "</td>";
			datatr += "<td>" + data.wzsl + "</td>";
			datatr += "<td>" + data.applyTime + "</td>";
			datatr += "<td>" + data.tid + "</td>";
			datatr += "<td>";
			datatr += "<button class='btn btn-info btn-sm mr15' onclick='edit("+data.id+")'>编辑</i></button>";
			datatr += "<button class='btn btn-danger btn-sm' onclick='del("+data.id+")'>删除</i></button>";
			datatr += "</td>";
			datatr += "</tr>";
		}
	} else {
		datatr = "<tr><td colspan=\"96\"  align='center'>很抱歉，没有找到相关的信息！</td><tr>";
	}
	$("#datalist tbody").append(datatr);
}

function edit(id){
	 var url = '/oneTime/toEdit?id='+id+'&t=' + new Date().getTime();
     $.get(url, function(data) {
     	 $('.contentpanel').html(data);
     });
}

function add(){
	var url = '/oneTime/toAdd?t=' + new Date().getTime();
    $.get(url, function(data) {
 	   $('.contentpanel').html(data);
    });
}

function delList(){
	var selectList =[];    
   	$('input[name="selectList"]:checked').each(function(){    
   		selectList.push($(this).val());    
   	});
   	if(selectList.length==0){
   		layer.msg("请选择要删除的信息!", {offset: ['50%', '50%'],shift: 6});
   		return false;
   	}
   	del(selectList.toString());
}

function del(ids){
	if(confirm("确定删除吗？")){
	var obj = new Object();
	obj.ids = ids;
	$.ajax({
		url : '/oneTime/removeList?' + Math.floor(Math.random() * 100),
		type : "POST",
		data : obj,
		dataType : "json",
		async: false,
		success : function(result) {
			if (result.code == "0") {
				layer.msg("删除成功!", {offset: ['50%', '50%'],shift: 6});
				onback();
			}
		},
	});
	}
}

function selectEvent(checked){
	if(checked){
		$("input[name='selectList']").attr("checked",true); 
	} else {
		$("input[name='selectList']").attr("checked",false); 
	}
}

function onback(){
	var url = "/oneTime/list?t="+ new Date().getTime();
	$.get(url, function(data) {
		$('.contentpanel').html(data);
	});
}
</script>
</head>
<body>
	<div class="row">
		<div class="col-md-12">
			<ul class="page-breadcrumb breadcrumb">
				<li><i class="fa fa-home"></i><a href="javascript:;">首页 </a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">物品申领 </a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">一次性物品申请登记</a></li>
			</ul>
		</div>
	</div>
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span> <button class="btn btn-primary btn-sm"
					onclick="add();" data-toggle="modal"><i class="fa fa-plus"></i>添加</button>
				</span> 
				<!-- <span> <button class="btn btn-danger btn-sm" href="javascript:void(0)"
					onclick="delList();" data-toggle="modal"><i
						class="fa fa-trash-o"></i>删除</button>
				</span> --> 
				
				<span style="float: right;"> <input type="text"
					id="pydm" placeholder="请输入拼音代码">
					生产时间<input id="beginCreateTime" class="Wdate"
					onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" />~<input
					id="endCreateTime" class="Wdate"
					onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" />
					<button type="button" class="btn  btn-primary"
						onclick="getPageData(1);">
						<i class="fa fa-search"></i>查询
					</button>
				</span>
			</h3>
		</div>
	</div>
	<div class="panel-body">
		<table id="datalist"
			class="table table-responsive table-bordered table-striped table-hover text-center-contd">
			<thead>
				<tr>
					<th><input type="checkbox" onclick="selectEvent(this.checked);" /></th>
					<th>编号</th>
					<th>拼音代码</th>
					<th>物品编码</th>
					<th>状态</th>
					<th>物品名称</th>
					<th>物品规格</th>
					<th>物品单位</th>
					<th>装箱规格</th>
					<th>数量</th>
					<th>申领时间</th>
					<th>追溯ID</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<tr>
				</tr>
			</tbody>
		</table>
		<div class="box-footer clearfix">
			<ul class="pagination pagination-sm no-margin pull-right">
				<li><a href="#" title="首页">&laquo; 首页</a></li>
				<li><a href="#" title="上一页">&laquo; 上一页</a></li>
				<li><a href="#" title="下一页">下一页 &raquo;</a></li>
				<li><a href="#" title="尾页">尾页 &raquo;</a></li>
			</ul>
		</div>
	</div>
</body>
</html>