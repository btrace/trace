<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"></meta>
<meta name="renderer" content="webkit|ie-comp|ie-stand"></meta>
<title>添加科室信息</title>
<script type="text/javascript">
	function onback() {
		var url = "/personRole/list?t=" + new Date().getTime();
		$.get(url, function(data) {
			$('.contentpanel').html(data);
		});
	}

	function onsave() {
		if ($("#personRoleName").val() == '') {
			layer.msg("人员角色名称必填!", {
				offset : [ '50%', '50%' ],
				shift : 6
			});
			return false;
		}
		if (isExist()) {
			var obj = new Object();
			obj.personRoleName = $("#personRoleName").val();
			$.ajax({
				url : '/personRole/save?'
						+ Math.floor(Math.random() * 100),
				type : "POST",
				data : obj,
				dataType : "json",
				async : false,
				success : function(result) {
					if (result.code == "0") {
						layer.msg("保存成功!", {offset : [ '50%', '50%' ],shift : 6 });
						onback();
					}
				},
			});
		}
	}

	function isExist() {
		var flag = true;
		var obj = new Object();
		obj.personRoleName = $("#personRoleName").val();
		$.ajax({
			url : "/personRole/isExist",
			type : 'post',
			data : obj,
			async : false,
			dataType : "json",
			success : function(data) {
				if (data.code == '0') {
					flag = false;
					layer.msg("该人员角色名称已经存在,请重新输入！！", {offset : [ '50%', '50%' ],shift : 6 });
				}
			}
		});
		return flag;
	}
</script>
</head>
<body>
	<div class="row">
		<div class="col-md-12">
			<ul class="page-breadcrumb breadcrumb">
				<li><i class="fa fa-home"></i> <a href="javascript:;">首页 </a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">人员管理</a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">人员角色信息列表</a></li>
			</ul>
		</div>
	</div>
	<div class="panel panel-default panel-list-border">
		<div class="panel-heading">
			<h4 class="modal-title">添加人员角色信息</h4>
		</div>
	</div>
	<div class="panel-body">
		<div class="form-group">
			<label class="col-sm-2 control-label">人员角色名称<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control"
					placeholder="请输入人员角色姓名,最大长度是32" maxlength="32" id="personRoleName">
			</div>
		</div>
	</div>
	<div class="box-footer">
		<button type="button" class="btn btn-primary" id="save"
			onclick="onsave();">提交</button>
		<button type="button" class="btn btn-primary" id="back"
			onclick="onback();">返回</button>
	</div>
	</div>
</body>
</html>