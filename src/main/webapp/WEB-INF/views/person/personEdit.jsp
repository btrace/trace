<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
          content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"></meta>
    <meta name="renderer" content="webkit|ie-comp|ie-stand"></meta>
    <title>编辑信息</title>
    <script type="text/javascript">
        $(document).ready(function () {
            var id = $("#id").val();
            $.ajax({
                url: "/person/get?id=" + id + "&t=" + new Date().getTime(),
                type: "post",
                success: function (data) {
                    $("#personId").val(data.data.personId);
                    $("#name").val(data.data.name);
                    $("input[name=sex][value=" + data.data.sex + "]").attr("checked", true);
                    $("#remark").val(data.data.remark);
                    $("#telephoneNum").val(data.data.telephoneNum);

                    var departmentId = data.data.departmentId;
                    $.ajax({
                        url: "/department/getAllDepartmentInfo?" + "&t=" + new Date().getTime(),
                        type: "post",
                        success: function (data) {
                            var options = '';
                            for (var i = 0; i < data.data.length; i++) {
                                if (data.data[i].id == departmentId) {
                                    options += "<option value=\"" + data.data[i].id + "\" selected =\"selected\">" + data.data[i].departmentName + "</option>";
                                } else {
                                    options += "<option value=\"" + data.data[i].id + "\">" + data.data[i].departmentName + "</option>";
                                }
                            }
                            $("#departmentInfo").append(options);
                        }
                    });
                    
                    var personRoleIds = data.data.personRoleIds;
                    $.ajax({
                        url: "/personRole/getAll?" + "&t=" + new Date().getTime(),
                        type: "post",
                        success: function (data) {
                        	var datatr = "";
                            var len = data.data.length;
                            if (len > 0) {
                                for (var i = 0; i < len; i++) {
                                	var flag = false;
                                    var temp = data.data[i];
                                    var ids = personRoleIds.split(",");
                                    for (var j = 0; j < ids.length; j++) {
                                        if (ids[j] == temp.id) {
                                        	flag = true;
                                        } 
                                    }
                                    
                                    if(flag){
                                        datatr += "<input type=\"checkbox\" name=\"personRoleIds\" checked=\"checked\" value=\"" + temp.id + "\" />" + temp.personRoleName;
                                    } else {
                                        datatr += "<input type=\"checkbox\" name=\"personRoleIds\" value=\"" + temp.id + "\" />" + temp.personRoleName;
                                    }
                                    
                                }
                            }
                            $("#personRoleIds").html(datatr);
                        }
                    });
                }
            });
        });

        function onback() {
            var url = "/person/list?t=" + new Date().getTime();
            $.get(url, function (data) {
                $('.contentpanel').html(data);
            });
        }


        function onsave() {
            if ($("#personId").val() == '') {
                layer.msg("人员ID必填!", {offset: ['50%', '50%'], shift: 6});
                return false;
            }
            if ($("#name").val() == '') {
                layer.msg("姓名必填!", {offset: ['50%', '50%'], shift: 6});
                return false;
            }
            var phoneReg = /^1[34578]\d{9}$/;
            var phone = $("#telephoneNum").val();
            if (phone.length != 11 || !phoneReg.test(phone)) {
                layer.msg("请输入有效的手机号码！", {offset: ['50%', '50%'], shift: 6});
                return false;
            }
            if ($("#departmentInfo").val() == '') {
                layer.msg("人员所属科室信息必填!", {offset: ['50%', '50%'], shift: 6});
                return false;
            }
            var chk_value = [];
            $('input[name="personRoleIds"]:checked').each(function () {
                chk_value.push($(this).val());
            });
            if (chk_value.toString() == '') {
                layer.msg("人员必须分配人员角色!", {offset: ['50%', '50%'], shift: 6});
                return false;
            }
            if (isExist()) {
                var obj = new Object();
                obj.id = $("#id").val();
                obj.personId = $("#personId").val();
                obj.name = $("#name").val();
                obj.sex = $("input[name='sex']:checked").val();
                obj.telephoneNum = phone;
                obj.departmentId = $("#departmentInfo").val();
                obj.personRoleIds = chk_value.toString();
                obj.remark = $("#remark").val();
                $.ajax({
                    url: '/person/update?' + Math.floor(Math.random() * 100),
                    type: "POST",
                    data: obj,
                    dataType: "json",
                    async: true,
                    success: function (result) {
                        layer.msg("保存成功!", {offset: ['50%', '50%'], shift: 6});
                        onback();
                    },
                });
            }
        }

        function isExist() {
            var flag = true;
            var obj = new Object();
            obj.id = $("#id").val();
            obj.personId = $("#personId").val();
            $.ajax({
                url: "/person/isExist",
                type: 'post',
                data: obj,
                async: false,
                dataType: "json",
                success: function (data) {
                    if (data.code == '0') {
                        flag = false;
                        layer.msg("该人员ID已经存在,请重新输入！！", {offset: ['50%', '50%'], shift: 6});
                    }
                }
            });
            return flag;
        }
    </script>
</head>
<body>
<input type="hidden" value="${id }" id="id" name="id">
<div class="row">
    <div class="col-md-12">
        <ul class="page-breadcrumb breadcrumb">
            <li><i class="fa fa-home"></i> <a href="javascript:;">首页 </a> <i
                    class="fa fa-angle-right"></i></li>
            <li><a href="javascript:;">人员管理</a> <i
                    class="fa fa-angle-right"></i></li>
            <li><a href="javascript:;">人员信息列表</a></li>
        </ul>
    </div>
</div>
<div class="panel panel-default panel-list-border">
    <div class="panel-heading">
        <h4 class="modal-title">编辑人员信息</h4>
    </div>
</div>
<div class="panel-body">
    <div class="form-group">
        <label class="col-sm-2 control-label">人员ID<span
                class="star-red">*</span></label>
        <div class="col-sm-5">
            <input type="text" class="form-control"
                   placeholder="请输入人员ID,最大长度是11" maxlength="11" id="personId">
        </div>
        <div class="col-sm-5">
            <span class="help-block-text">人员ID不能重复，必须是唯一的！</span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">人员姓名<span
                class="star-red">*</span></label>
        <div class="col-sm-5">
            <input type="text" class="form-control"
                   placeholder="请输入姓名,最大长度是32" maxlength="32" id="name">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">性别<span
                class="star-red">*</span></label>
        <div class="col-sm-5">
            <input type="radio" name="sex" value="0"
                   checked="checked"/>男 <input type="radio" name="sex" value="1"/>女
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">联系电话<span
                class="star-red">*</span></label>
        <div class="col-sm-5">
            <input type="text" class="form-control"
                   placeholder="请输入手机号,最大长度是11" maxlength="11" id="telephoneNum">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">科室信息<span
                class="star-red">*</span></label>
        <div class="col-lg-2">
            <select id="departmentInfo" class="form-control">
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">人员角色信息<span
                class="star-red">*</span></label>
        <div class="col-lg-5" id="personRoleIds">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">描述</label>
        <div class="col-sm-5">
            <input type="text" class="form-control" maxlength="256" id=remark
                   name="remark">
        </div>
    </div>
</div>
</div>
<div class="box-footer">
    <button type="button" class="btn btn-primary" id="save"
            onclick="onsave();">提交
    </button>
    <button type="button" class="btn btn-primary" id="back"
            onclick="onback();">返回
    </button>
</div>
</body>
</html>