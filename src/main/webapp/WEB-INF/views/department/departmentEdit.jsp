<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"></meta>
<meta name="renderer" content="webkit|ie-comp|ie-stand"></meta>

<script type="text/javascript">
$(document).ready(function(){
	var id = $("#id").val();
	if(id != '-1'){
		$.ajax({
            url:"/department/get?id="+id+"&t=" + new Date().getTime(),
			type : "post",
			success : function(data) {
				$("#departmentID").val(data.data.departmentID);
				$("#departmentName").val(data.data.departmentName);
				$("#departmentDirector").val(data.data.departmentDirector);
				$("#telephoneNum").val(data.data.telephoneNum);
				$("#address").val(data.data.address);
			}
		});
	}
});

function onback(){
	var url = "/department/list?t=" + new Date().getTime();
	$.get(url, function(data) {
        $('.contentpanel').html(data);
    });
}
function onsave(){
	if($("#departmentName").val()==''){
		layer.msg("科室名称必填!", {offset: ['50%', '50%'],shift: 6});
		return false;
	}
	if($("#departmentDirector").val()==''){
		layer.msg("科室主任姓名必填!", {offset: ['50%', '50%'],shift: 6});
		return false;
	}
	var myreg = /^1[34578]\d{9}$/;
    var phone = $("#telephoneNum").val();
	if(phone.length !=11){
		layer.msg("请输入有效的手机号码！", {offset: ['50%', '50%'],shift: 6});
		return false;
    }else if(!myreg.test(phone)){
    	layer.msg("请输入有效的手机号码！", {offset: ['50%', '50%'],shift: 6});
    	return false;
    }
	if($("#address").val()==''){
		layer.msg("科室地址信息必填!", {offset: ['50%', '50%'],shift: 6});
		return false;
	}

	if (isExist()) {
		var obj = new Object();
		obj.id = $("#id").val();
		obj.departmentName = $("#departmentName").val();
		obj.departmentDirector = $("#departmentDirector").val();
		obj.telephoneNum = phone;
		obj.address = $("#address").val();
		$.ajax({
			url : '/department/update?' + Math.floor(Math.random() * 100),
			type : "POST",
			data : obj,
			dataType : "json",
			async: false,
			success : function(result) {
				if (result.code == "0") {
					layer.msg("保存成功!", {offset: ['50%', '50%'],shift: 6});
					onback();
				}
			},
		});
	}
}

function isExist() {
	var flag = true;
	var obj = new Object();
	obj.id = $("#id").val();
	obj.departmentName = $("#departmentName").val();
	$.ajax({
		url : "/department/isExist",
		type : 'post',
		data : obj,
		async : false,
		dataType : "json",
		success : function(data) {
			if (data.code == '0') {
				flag = false;
				layer.msg("该科室名称已经存在,请重新输入！！", {offset : [ '50%', '50%' ],shift : 6 });
			}
		}
	});
	return flag;
}

</script>
</head>
<body>
	<input type="hidden" value="${id }" id="id" name="id">
	<div class="row">
		<div class="col-md-12">
			<ul class="page-breadcrumb breadcrumb">
				<li><i class="fa fa-home"></i> <a href="javascript:;">首页 </a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">人员管理</a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">科室信息列表</a></li>
			</ul>
		</div>
	</div>
	<div class="panel panel-default panel-list-border">
		<div class="panel-heading">
			<h4 class="modal-title">编辑科室信息</h4>
		</div>
	</div>
	<div class="panel-body">
		<div class="form-group">
			<label class="col-sm-2 control-label">科室名称<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入科室姓名,最大长度是32" maxlength="32" id="departmentName" name="departmentName">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text">科室ID不能重复，必须是唯一的！</span>
			</div>
		</div>     
		<div class="form-group">
			<label class="col-sm-2 control-label">科室主任<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入科室主任姓名,最大长度是32" maxlength="32" id="departmentDirector" name="departmentDirector">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">联系电话<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入医生/护士手机号,最大长度是11" maxlength="11" id="telephoneNum" name="telephoneNum">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">科室地址<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入科室地址信息" maxlength="128" id="address" name="address">
			</div>
		</div>
		</div>
		<div class="box-footer">
			<button type="button" class="btn btn-primary" id="save" onclick="onsave();">提交</button>
			<button type="button" class="btn btn-primary" id="back" onclick="onback();">返回</button>
		</div>
	</div>
</body>
</html>