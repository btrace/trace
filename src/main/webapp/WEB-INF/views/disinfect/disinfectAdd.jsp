<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"></meta>
<meta name="renderer" content="webkit|ie-comp|ie-stand"></meta>
<title>添加灭菌记录</title>
<script type="text/javascript">
	function onback() {
		var url = "/disinfect/list?t=" + new Date().getTime();
		$.get(url, function(data) {
			$('.contentpanel').html(data);
		});
	}

	function onsave() {
		if ($("#personId").val() == '') {
			layer.msg("操作人员工号必填!", {
				offset : [ '50%', '50%' ],
				shift : 6
			});
			return false;
		}
		
		if ($("#tid").val() == '') {
			layer.msg("追溯ID必填!", {
				offset : [ '50%', '50%' ],
				shift : 6
			});
			return false;
		}

		var obj = new Object();
		obj.personId = $("#personId").val();
		obj.tid = $("#tid").val();
		obj.wzuuid = $("#wzuuid").val();
		$.ajax({
			url : '/disinfect/save?' + Math.floor(Math.random() * 100),
			type : "POST",
			data : obj,
			dataType : "json",
			async : true,
			success : function(result) {
				layer.msg("保存成功!", {
					offset : [ '50%', '50%' ],
					shift : 6
				});
				onback();
			},
		});
	}

	function getWpmc(value) {
		var obj = new Object();
		obj.tid = value;
		obj.wzuuid = $("#wzuuid").val();
		$.ajax({
			url : "/tool/getWpmcByTid",
			type : 'post',
			data : obj,
			async : false,
			dataType : "json",
			success : function(data) {
				if (data.code == '0') {
					$("#wpmc").val(data.data.wpmc);
					$("#wzuuid").val(data.data.wzuuid);
				}
			}
		});
	}
</script>
</head>
<body>
	<input type="hidden" id="wzuuid">
	<div class="row">
		<div class="col-md-12">
			<ul class="page-breadcrumb breadcrumb">
				<li><i class="fa fa-home"></i><a href="javascript:;">首页 </a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">消毒灭菌 </a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">灭菌登记</a></li>
			</ul>
		</div>
	</div>
	<div class="panel panel-default panel-list-border">
		<div class="panel-heading">
			<h4 class="modal-title">添加灭菌登记记录</h4>
		</div>
	</div>
	<div class="panel-body">
		<div class="form-group">
			<label class="col-sm-2 control-label">操作人员工号<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入操作人员工号"
					id="personId">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">追溯ID<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入追溯ID"
					id="tid" onblur="getWpmc(this.value)">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">物品名称</label>
			<div class="col-sm-5">
				<input type="text" class="form-control" id="wpmc">
			</div>
		</div>
	</div>
	<div class="box-footer">
		<button type="button" class="btn btn-primary" id="save"
			onclick="onsave();">提交</button>
		<button type="button" class="btn btn-primary" id="back"
			onclick="onback();">返回</button>
	</div>
</body>
</html>