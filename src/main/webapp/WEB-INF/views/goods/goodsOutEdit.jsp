<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"></meta>
<meta name="renderer" content="webkit|ie-comp|ie-stand"></meta>
<title>物品使用</title>
<script type="text/javascript">
	$(document).ready(function(){
		$.ajax({
            url: "/goods/outGet?id="+$("#id").val()+"&t=" + new Date().getTime(),
            type: "post",
            success: function (data) {
                if (data.code == '0') {
                	$("#pydm").val(data.data.pydm).attr("disabled",true);
                	$("#sccs").val(data.data.sccs).attr("disabled",true);
                	$("#wpmc").val(data.data.wpmc).attr("disabled",true);
 					$("#wpfl").val(data.data.wpfl).attr("disabled",true);
            		$("#wzuuid").val(data.data.wzuuid).attr("disabled",true);
            		$("#sccs").val(data.data.sccs).attr("disabled",true);
            		$("#wzsl").val(data.data.wzsl);
            		$("#disinfectTime").val(data.data.disinfectTime);
            		$("#scph").val(data.data.scph).attr("disabled",true);
            		$("#scrq").val(data.data.scrq).attr("disabled",true);
            		$("#wpyxq").val(data.data.wpyxq).attr("disabled",true);
            		$("#ckid").val(data.data.warehouseName).attr("disabled",true);
            		$("#czyuuid").val(data.data.czyuuid).attr("disabled",true);
                }
            }
        });
	});
	
	function onsave(){
		if($("#wzsl").val()==''){
			layer.msg("物品数量不能为空!", {offset: ['50%', '50%'], shift: 6});
			return false;
		}
		var reg = /^[1-9]\d*$/;
		if(!reg.test($("#wzsl").val())){
			layer.msg("物品数量格式不正确!", {offset: ['50%', '50%'], shift: 6});
			return false;
		}
		if($("#disinfectTime").val()==''){
			layer.msg("灭菌时间不能为空!", {offset: ['50%', '50%'], shift: 6});
			return false;
		}
		var obj = new Object();
		obj.id = $("#id").val();
		obj.wzsl = $("#wzsl").val();
		obj.disinfectTime = $("#disinfectTime").val();
		$.ajax({
			url : '/goods/outUpdate?' + Math.floor(Math.random() * 100),
			type : "POST",
			data : obj,
			dataType : "json",
			async: false,
			success : function(result) {
				if (result.code == "0") {
					layer.msg("更新成功!", {offset: ['50%', '50%'], shift: 6});
					onback();
				}
			}
		});
	}
	
	function onback() {
        var url = '/goods/outlist?t=' + new Date().getTime();
        $.get(url, function (data) {
            $('.contentpanel').html(data);
        });
    }
</script>
</head>
<body>
	<input type="hidden" value="${id }" id="id">
	<div class="row">
		<div class="col-md-12">
			<ul class="page-breadcrumb breadcrumb">
				<li><i class="fa fa-home"></i> <a href="javascript:;">首页 </a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">物资管理</a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">物品出库统计</a></li>
			</ul>
		</div>
	</div>
	
	<div class="panel panel-default panel-list-border">
		<div class="panel-heading">
			<h4 class="modal-title">编辑物品出库登记</h4>
		</div>
	</div>
	
	<div class="panel-body">
		<div class="form-group">
			<label class="col-sm-2 control-label">拼音编码<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control"  maxlength="64" id="pydm">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">生产厂商<span
					class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control"  maxlength="64" id="sccs">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">登记人员<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control"  maxlength="64" id="czyuuid">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">物品名称<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" maxlength="32" id="wpmc" >
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">数量<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入物品数量,最大长度是32" maxlength="32" id="wzsl">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">生产批号<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入物品ID,最大长度是32" maxlength="32" id="scph">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">生产日期<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" id="scrq" />
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">有效期<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入物品有效期,最大长度是32" maxlength="32" id="wpyxq">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label">灭菌时间<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" id="disinfectTime" class="Wdate"
                               onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})"/>
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">仓储位置<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" maxlength="64" id="ckid" >
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>	
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">物品分类<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" maxlength="64" id="wpfl" >
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>		
		</div>
		<div class="box-footer">
			<button type="button" class="btn btn-primary" id="save" onclick="onsave();">提交</button>
			<button type="button" class="btn btn-primary" id="back" onclick="onback();">返回</button>
		</div>
	</div>
</body>
</html>