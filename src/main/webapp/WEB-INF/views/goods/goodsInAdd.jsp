<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"></meta>
<meta name="renderer" content="webkit|ie-comp|ie-stand"></meta>
<title>物品使用</title>
<script type="text/javascript">
	$(document).ready(function(){
        $.ajax({
            url: "/goodsType/getAllGoodsType?" + "t=" + new Date().getTime(),
            type: "post",
            success: function (data) {
                if (data.code == '0') {
                    var options = '';
                    options += "<option value=''>请选择</option>";
                    for (var i = 0; i < data.data.length; i++) {
                        options += "<option value=\"" + data.data[i].pydm + "\">" + data.data[i].pydm + "</option>";
                    }
                    $("#pydm").html(options);
                }
            }
        });
	});
	
	function changePydm(value){
		var obj = new Object();
		obj.pydm = value;
		$.ajax({
			url : 'goodsType/getAllSccssByPydm?' + Math.floor(Math.random() * 100),
			type : "POST",
			data : obj,
			dataType : "json",
			async: false,
			success : function(data) {
				if (data.code == '0') {
                    var options = '';
                    options += "<option value=''>请选择</option>";
                    for (var i = 0; i < data.data.length; i++) {
                        options += "<option value=\"" + data.data[i].sccs + "\">" + data.data[i].sccs + "</option>";
                    }
                    $("#sccs").html(options);
                }
			}
		});
	}
	
	function getGoodsType(){
		var obj = new Object();
		obj.sccs = $("#sccs").val();
		obj.pydm = $("#pydm").val();
		$.ajax({
			url : 'goodsType/get?' + Math.floor(Math.random() * 100),
			type : "POST",
			data : obj,
			dataType : "json",
			async: false,
			success : function(data) {
				if (data.code == '0') {
					$("#wpmc").val(data.data.wpmc);
					$("#wpfl").val(data.data.wpfl);
					var obj = new Object();
					obj.wzuuid = data.data.wzuuid;
					$("#wzuuid").val(data.data.wzuuid);
					$.ajax({
						url : 'goodsType/getWarehouseByWzuuid?' + Math.floor(Math.random() * 100),
						type : "POST",
						data : obj,
						dataType : "json",
						async: false,
						success : function(data) {
							if (data.code == '0') {
			                    var options = '';
			                    for (var i = 0; i < data.data.length; i++) {
			                        options += "<option value=\"" + data.data[i].id + "\">" + data.data[i].warehouseName + "</option>";
			                    }
			                    $("#ckid").html(options);
			                }
						}
					});
				}
			}
		});
	}
	
	function onsave(){
		if($("#sccs").val()==''){
			layer.msg("生产厂商不能为空!", {offset: ['50%', '50%'], shift: 6});
			return false;
		}
		if($("#wzsl").val()==''){
			layer.msg("物品入库数量不能为空!", {offset: ['50%', '50%'], shift: 6});
			return false;
		}
		var reg = /^[1-9]\d*$/;
		if(!reg.test($("#wzsl").val())){
			layer.msg("物品入库数量格式不正确!", {offset: ['50%', '50%'], shift: 6});
			return false;
		}
		if($("#scph").val()==''){
			layer.msg("生产批号不能为空!", {offset: ['50%', '50%'], shift: 6});
			return false;
		}
		if($("#scrq").val()==''){
			layer.msg("生产日期不能为空!", {offset: ['50%', '50%'], shift: 6});
			return false;
		}
		if($("#wpyxq").val()==''){
			layer.msg("有效期不能为空!", {offset: ['50%', '50%'], shift: 6});
			return false;
		}
		if($("#disinfectTime").val()==''){
			layer.msg("灭菌时间不能为空!", {offset: ['50%', '50%'], shift: 6});
			return false;
		}
		if($("#ckid").val()==''){
			layer.msg("仓库不能为空!", {offset: ['50%', '50%'], shift: 6});
			return false;
		}
		var obj = new Object();
		obj.wzuuid = $("#wzuuid").val();
		obj.sccs = $("#sccs").val();
		obj.wzsl = $("#wzsl").val();
		obj.scph = $("#scph").val();
		obj.scrq = $("#scrq").val();
		obj.wpyxq = $("#wpyxq").val();
		obj.disinfectTime = $("#disinfectTime").val();
		obj.ckid = $("#ckid").val();
		obj.czyuuid = $("#account").val();
		$.ajax({
			url : '/goods/inSave?' + Math.floor(Math.random() * 100),
			type : "POST",
			data : obj,
			dataType : "json",
			async: false,
			success : function(result) {
				if (result.code == "0") {
					layer.msg("保存成功!", {offset: ['50%', '50%'], shift: 6});
					onback();
				}
			}
		});
	}
	
	function onback(){
		 var url = '/goods/inlist?t=' + new Date().getTime();
	     $.get(url, function(data) {
	     	 $('.contentpanel').html(data);
	     });
	}
</script>
</head>
<body>
	<input type="hidden" id="wzuuid">
	<div class="row">
		<div class="col-md-12">
			<ul class="page-breadcrumb breadcrumb">
				<li><i class="fa fa-home"></i> <a href="javascript:;">首页 </a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">物资管理</a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">物品入库登记</a></li>
			</ul>
		</div>
	</div>
	
	<div class="panel panel-default panel-list-border">
		<div class="panel-heading">
			<h4 class="modal-title">增加物品入库登记</h4>
		</div>
	</div>
	
	<div class="panel-body">
		<div class="form-group">
			<label class="col-sm-2 control-label">拼音编码<span
				class="star-red">*</span></label>
			<div class="col-lg-5" >
				<select id="pydm" class="form-control" onchange="changePydm(this.value)">
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">生产厂商<span
					class="star-red">*</span></label>
			<div class="col-lg-5" >
				<select id="sccs" class="form-control" onchange="getGoodsType()">
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">登记人员<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" disabled maxlength="64" id="account" value="${account}">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">物品名称<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" maxlength="32" id="wpmc" disabled>
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">入库数量<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入物品入库数量,最大长度是32" maxlength="32" id="wzsl">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">生产批号<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入物品ID,最大长度是32" maxlength="32" id="scph">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">生产日期<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" id="scrq" class="Wdate"
                               onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})"/>
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">有效期<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入物品有效期,最大长度是32" maxlength="32" id="wpyxq">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label">灭菌时间<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" id="disinfectTime" class="Wdate"
                               onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})"/>
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">仓储位置<span
				class="star-red">*</span></label>
			<div class="col-lg-5" >
				<select id="ckid" class="form-control">
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">物品分类<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" maxlength="64" id="wpfl" disabled>
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>		
		</div>
		<div class="box-footer">
			<button type="button" class="btn btn-primary" id="save" onclick="onsave();">提交</button>
			<button type="button" class="btn btn-primary" id="back" onclick="onback();">返回</button>
		</div>
	</div>
</body>
</html>