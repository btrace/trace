<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"></meta>
<meta name="renderer" content="webkit|ie-comp|ie-stand"></meta>

<script type="text/javascript">
$(document).ready(function(){
	var id = $("#id").val();
	$.ajax({
		url:"/nonDisposable/get?id="+id+"&t=" + new Date().getTime(),
		type : "post",
		success : function(data) {
			$("#pydm").val(data.data.pydm).attr("disabled",true);
           	$("#sccs").val(data.data.sccs).attr("disabled",true);
           	$("#wpmc").val(data.data.wpmc).attr("disabled",true);
       		$("#sccs").val(data.data.sccs).attr("disabled",true);
       		$("#personName").val(data.data.personName);
       		$("#wzsl").val(data.data.wzsl);
       		$("#scph").val(data.data.scph).attr("disabled",true);
       		$("#departmentName").val(data.data.departmentName).attr("disabled",true);
       		$("#account").val(data.data.account).attr("disabled",true);
		}
	});
});


function getDepartment(value){
	var obj = new Object();
	obj.personId = value;
	$.ajax({
		url : 'person/get?' + Math.floor(Math.random() * 100),
		type : "POST",
		data : obj,
		dataType : "json",
		async: false,
		success : function(data) {
			if (data.code == '0') {
				$("#departmentName").val(data.data.departmentName);
			}
		}
	});
}

function onback() {
	var url = "/nonDisposableRevert/list?t=" + new Date().getTime();
	$.get(url, function(data) {
		$('.contentpanel').html(data);
	});
}

function onsave() {
	if($("#personName").val()==''){
		layer.msg("申请/借用人工号不能为空!", {offset: ['50%', '50%'], shift: 6});
		return false;
	}
	if($("#wzsl").val()==''){
		layer.msg("物品数量不能为空!", {offset: ['50%', '50%'], shift: 6});
		return false;
	}
	var reg = /^[1-9]\d*$/;
	if(!reg.test($("#wzsl").val())){
		layer.msg("物品数量格式不正确!", {offset: ['50%', '50%'], shift: 6});
		return false;
	}

	var obj = new Object();
	obj.id = $("#id").val();
	if($("#applyPersonId").val()!=''){
		obj.applyPersonId = $("#applyPersonId").val();
	}
	obj.wzsl = $("#wzsl").val();
	//obj.status = $("#status").val();
	$.ajax({
		url : '/nonDisposable/update?'
				+ Math.floor(Math.random() * 100),
		type : "POST",
		data : obj,
		dataType : "json",
		async : false,
		success : function(result) {
			if (result.code == "0") {
				layer.msg("更新成功!", {offset : [ '50%', '50%' ],shift : 6 });
				onback();
			}
		},
	});
}
</script>
</head>
<body>
	<input type="hidden" value="${id }" id="id" name="id">
	<div class="row">
		<div class="col-md-12">
			<ul class="page-breadcrumb breadcrumb">
				<li><i class="fa fa-home"></i><a href="javascript:;">首页 </a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">物品申领 </a> <i
					class="fa fa-angle-right"></i></li>
				<li><a href="javascript:;">非一次性物品归还登记</a></li>
			</ul>
		</div>
	</div>
	<div class="panel panel-default panel-list-border">
		<div class="panel-heading">
			<h4 class="modal-title">编辑非一次性物品归还登记</h4>
		</div>
	</div>
	<div class="panel-body">
		<div class="form-group">
			<label class="col-sm-2 control-label">拼音编码<span
				class="star-red">*</span></label>
			<div class="col-lg-5" >
				<input id="pydm" class="form-control" placeholder="请输入物品ID,最大长度是32" maxlength="32">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">生产厂商<span
					class="star-red">*</span></label>
			<div class="col-lg-5" >
				<input id="sccs" class="form-control" placeholder="请输入物品ID,最大长度是32" maxlength="32">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">登记人员<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" disabled maxlength="64" id="account">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">原申请/借用人名称<span
				class="star-red"></span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" maxlength="64" id="personName">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">申请/借用人工号<span
				class="star-red"></span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" maxlength="64" id="applyPersonId" onblur="getDepartment(this.value)">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">科室信息<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" disabled maxlength="64" id="departmentName">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text">请先填写申请/借用人工号</span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">物品名称<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" maxlength="32" id="wpmc" disabled>
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">物品数量<span
				class="star-red">*</span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入物品数量,最大长度是32" maxlength="32" id="wzsl">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">生产批号<span
				class="star-red"></span></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" placeholder="请输入物品ID,最大长度是32" maxlength="32" id="scph">
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div>
		<!-- <div class="form-group">
			<label class="col-sm-2 control-label">物品状态<span
				class="star-red"></span></label>
			<div class="col-sm-5">
				<select id="status" class="form-control">
					<option value="1">借用</option>
					<option value="2">归还</option>
				</select>
			</div>
			<div class="col-sm-5">
				<span class="help-block-text"></span>
			</div>
		</div> -->
	</div>
	<div class="box-footer">
		<button type="button" class="btn btn-primary" id="save"
			onclick="onsave();">提交</button>
		<button type="button" class="btn btn-primary" id="back"
			onclick="onback();">返回</button>
	</div>
</body>
</html>